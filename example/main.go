package main

import (
	// "fmt"
	"bitbucket.org/jtejido/inverted"
	. "bitbucket.org/jtejido/inverted/document"
	. "bitbucket.org/jtejido/inverted/document/field"
)

func main() {

	doc1 := NewDocument()
	doc1.AddField(NewField("test one", "test one", false, true, true))

	doc2 := NewDocument()
	doc2.AddField(NewField("test two", "test two", false, true, true))

	doc3 := NewDocument()
	doc3.AddField(NewField("test three", "test three", false, true, true))

	doc4 := NewDocument()
	doc4.AddField(NewField("test four", "test four", false, true, true))

	doc5 := NewDocument()
	doc5.AddField(NewField("test five", "test five", false, true, true))

	doc6 := NewDocument()
	doc6.AddField(NewField("test six", "test six", false, true, true))

	index := goffer.Create("test")
	index.AddDocument(doc1)
	index.AddDocument(doc2)
	index.AddDocument(doc3)
	index.AddDocument(doc4)
	index.AddDocument(doc5)
	index.AddDocument(doc6)
	//index.Optimize()
	index.Find("six")

}
