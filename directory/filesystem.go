package directory

import (
    . "bitbucket.org/jtejido/inverted/file"
    "fmt"
    "os"
    "path/filepath"
)

type DirectoryFileSystem struct {
    Path        string
    FileHandler map[string]*FileSystem
}

func NewDirectoryFileSystem(path string) *DirectoryFileSystem {
    os.MkdirAll(path, os.ModePerm)
    return &DirectoryFileSystem{
        Path:        path,
        FileHandler: make(map[string]*FileSystem),
    }
}

// reader
func (dfs DirectoryFileSystem) GetFileObject(filename string, shareHandler bool) (*FileSystem, error) {
    fullFilename := dfs.Path + "/" + filename
    if shareHandler == false {
        v, err := NewFileSystem(fullFilename, false)
        if err != nil {
            return nil, err
        }
        return v, nil
    }

    v, ok := dfs.FileHandler[filename]
    if ok {
        v.Seek(0, 0)
        return dfs.FileHandler[filename], nil
    }
    v, err := NewFileSystem(fullFilename, false)
    if err != nil {
        return nil, err
    }

    dfs.FileHandler[filename] = v
    return dfs.FileHandler[filename], nil
}

func (dfs DirectoryFileSystem) FileList() []string {
    var result = make([]string, 0)
    err := filepath.Walk(dfs.Path, func(path string, f os.FileInfo, err error) error {
        if !f.IsDir() {
            result = append(result, f.Name())
        }

        return nil
    })

    if err != nil {
        fmt.Errorf("No Files found.")
    }

    return result
}

// writer
func (dfs DirectoryFileSystem) CreateFile(filename string) (*FileSystem, error) {

    _, ok := dfs.FileHandler[filename]
    if ok {
        dfs.FileHandler[filename].FileHandle.Close()
    }

    delete(dfs.FileHandler, filename)

    v, err := NewFileSystem(dfs.Path+"/"+filename, true)
    if err != nil {
        return nil, err
    }
    dfs.FileHandler[filename] = v

    return dfs.FileHandler[filename], nil
}

func (dfs DirectoryFileSystem) DeleteFile(filename string) error {
    _, ok := dfs.FileHandler[filename]
    if ok {
        dfs.FileHandler[filename].FileHandle.Close()
    }
    delete(dfs.FileHandler, filename)
    fmt.Println(dfs.Path + "/" + filename)
    err := os.Remove(dfs.Path + "/" + filename)

    if err != nil {
        return err
    }

    return nil

}
