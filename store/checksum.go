package store

import (
	"bitbucket.org/jtejido/inverted/util"
)

type ChecksumIndexInput interface {
	IndexInput
	Checksum() int64
}

type ChecksumIndexInputImplSPI interface {
	util.DataReader
	FilePointer() int64
}

type ChecksumIndexInputImpl struct {
	*IndexInputImpl
	spi ChecksumIndexInputImplSPI
}

func NewChecksumIndexInput(desc string, spi ChecksumIndexInputImplSPI) *ChecksumIndexInputImpl {
	return &ChecksumIndexInputImpl{NewIndexInputImpl(desc, spi), spi}
}

func (in *ChecksumIndexInputImpl) Seek(pos int64, whence int) error {
	skip := pos - in.spi.FilePointer()
	assert2(skip >= 0, "ChecksumIndexInput cannot seek backwards")
	return in.SkipBytes(skip)
}
