package store

import (
	"errors"
	"fmt"
	"bitbucket.org/jtejido/inverted/util"
	"io"
	"time"
)

const LOCK_POOL_INTERVAL = 1000
const LOCK_OBTAIN_WAIT_FOREVER = -1

type Lock interface {
	io.Closer
	Obtain() (ok bool, err error)
	ObtainWithin(lockWaitTimeout int64) (ok bool, err error)
	IsLocked() bool
}

type LockImpl struct {
	self Lock
	failureReason error
}

func NewLockImpl(self Lock) *LockImpl {
	return &LockImpl{self: self}
}

func (lock *LockImpl) ObtainWithin(lockWaitTimeout int64) (locked bool, err error) {
	lock.failureReason = nil
	locked, err = lock.self.Obtain()
	if err != nil {
		return
	}
	assert2(lockWaitTimeout >= 0 || lockWaitTimeout == LOCK_OBTAIN_WAIT_FOREVER,
		"lockWaitTimeout should be LOCK_OBTAIN_WAIT_FOREVER or a non-negative number (got %v)",
		lockWaitTimeout)

	maxSleepCount := lockWaitTimeout / LOCK_POOL_INTERVAL
	for sleepCount := int64(0); !locked; locked, err = lock.self.Obtain() {
		if lockWaitTimeout != LOCK_OBTAIN_WAIT_FOREVER && sleepCount >= maxSleepCount {
			reason := fmt.Sprintf("Lock obtain time out: %v", lock)
			if lock.failureReason != nil {
				reason = fmt.Sprintf("%v: %v", reason, lock.failureReason)
			}
			err = errors.New(reason)
			return
		}
		sleepCount++
		time.Sleep(LOCK_POOL_INTERVAL * time.Millisecond)
	}
	return
}

// Utility to execute code with exclusive access.
func WithLock(lock Lock, lockWaitTimeout int64, body func() interface{}) interface{} {
	panic("not implemeted yet")
}

type LockFactory interface {
	Make(name string) Lock
	Clear(name string) error
	SetLockPrefix(prefix string)
	LockPrefix() string
}

type LockFactoryImpl struct {
	lockPrefix string
}

func (f *LockFactoryImpl) SetLockPrefix(prefix string) {
	f.lockPrefix = prefix
}

func (f *LockFactoryImpl) LockPrefix() string {
	return f.lockPrefix
}

type FSLockFactory struct {
	*LockFactoryImpl
	lockDir string
}

func newFSLockFactory() *FSLockFactory {
	ans := &FSLockFactory{}
	ans.LockFactoryImpl = &LockFactoryImpl{}
	return ans
}

func (f *FSLockFactory) setLockDir(lockDir string) {
	if f.lockDir != "" {
		panic("You can set the lock directory for this factory only once.")
	}
	f.lockDir = lockDir
}

func (f *FSLockFactory) getLockDir() string {
	return f.lockDir
}

func (f *FSLockFactory) Clear(name string) error {
	panic("invalid")
}

func (f *FSLockFactory) Make(name string) Lock {
	panic("invalid")
}

func (f *FSLockFactory) String() string {
	return fmt.Sprintf("FSLockFactory@%v", f.lockDir)
}

type Directory interface {
	io.Closer
	// Files related methods
	ListAll() (paths []string, err error)
	// Returns true iff a file with the given name exists.
	// @deprecated This method will be removed in 5.0
	FileExists(name string) bool
	// Removes an existing file in the directory.
	DeleteFile(name string) error
	// Returns the length of a file in the directory. This method
	// follows the following contract:
	// 	- Must return error if the file doesn't exists.
	// 	- Returns a value >=0 if the file exists, which specifies its
	// length.
	FileLength(name string) (n int64, err error)
	CreateOutput(name string) (out IndexOutput, err error)
	Sync(names []string) error
	OpenInput(name string) (in IndexInput, err error)
	OpenOutput(name string) (out IndexOutput, err error)
	OpenChecksumInput(name string) (ChecksumIndexInput, error)
	MakeLock(name string) Lock
	ClearLock(name string) error
	SetLockFactory(lockFactory LockFactory)
	LockFactory() LockFactory
	LockID() string
	Copy(to Directory, src, dest string) error
	EnsureOpen()
}

type DirectoryImplSPI interface {
	OpenInput(string) (IndexInput, error)
	LockFactory() LockFactory
}

type DirectoryImpl struct {
	spi DirectoryImplSPI
}

func NewDirectoryImpl(spi DirectoryImplSPI) *DirectoryImpl {
	return &DirectoryImpl{spi}
}

func (d *DirectoryImpl) OpenChecksumInput(name string) (ChecksumIndexInput, error) {
	in, err := d.spi.OpenInput(name)
	if err != nil {
		return nil, err
	}
	return newBufferedChecksumIndexInput(in), nil
}

func (d *DirectoryImpl) LockID() string {
	return fmt.Sprintf("%v", d)
}

func (d *DirectoryImpl) String() string {
	return fmt.Sprintf("@hex lockFactory=%v", d.spi.LockFactory)
}

func (d *DirectoryImpl) Copy(to Directory, src, dest string) (err error) {
	var os IndexOutput
	var is IndexInput
	var success = false
	defer func() {
		if success {
			err = util.Close(os, is)
		} else {
			util.CloseWhileSuppressingError(os, is)
		}
		defer func() {
			recover()
		}()
		to.DeleteFile(dest)
	}()

	os, err = to.CreateOutput(dest)
	if err != nil {
		return err
	}
	is, err = d.spi.OpenInput(src)
	if err != nil {
		return err
	}
	err = os.CopyBytes(is, is.Length())
	if err != nil {
		return err
	}
	success = true
	return nil
}
