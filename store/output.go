package store

import (
	"bitbucket.org/jtejido/inverted/util"
	"io"
)

/*
Wrap IndexOutput to allow coding in flow style without worrying about
error check. If error happens in the middle, following calls are just
ignored.
*/
type IndexOutputStream struct {
	err error
	out IndexOutput
}

func Stream(out IndexOutput) *IndexOutputStream {
	return &IndexOutputStream{out: out}
}

func (ios *IndexOutputStream) WriteString(s string) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteString(s)
	}
	return ios
}

func (ios *IndexOutputStream) WriteInt(i int32) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteInt(i)
	}
	return ios
}

func (ios *IndexOutputStream) WriteIntAt(i int32, offset int64) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteIntAt(i, offset)
	}
	return ios
}

func (ios *IndexOutputStream) WriteVInt(i int32) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteVInt(i)
	}
	return ios
}

func (ios *IndexOutputStream) WriteLong(l int64) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteLong(l)
	}
	return ios
}

func (ios *IndexOutputStream) WriteLongAt(l int64, offset int64) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteLongAt(l, offset)
	}
	return ios
}

func (ios *IndexOutputStream) WriteByte(b byte) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteByte(b)
	}
	return ios
}

func (ios *IndexOutputStream) WriteByteAt(b byte, offset int64) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteByteAt(b, offset)
	}
	return ios
}

func (ios *IndexOutputStream) WriteBytes(b []byte) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteBytes(b)
	}
	return ios
}

func (ios *IndexOutputStream) WriteStringStringMap(m map[string]string) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteStringStringMap(m)
	}
	return ios
}

func (ios *IndexOutputStream) WriteStringSet(m map[string]bool) *IndexOutputStream {
	if ios.err == nil {
		ios.err = ios.out.WriteStringSet(m)
	}
	return ios
}

func (ios *IndexOutputStream) Close() error {
	return ios.err
}

type IndexOutputService interface {
	io.Closer
	FilePointer() int64
	Seek(pos int64) error
	Length() int64
}

type IndexOutput interface {
	io.Closer
	util.DataOutput
	FilePointer() int64
	Checksum() int64
}

type IndexOutputImpl struct {
	*util.DataOutputImpl
}

func NewIndexOutput(part util.DataWriter) *IndexOutputImpl {
	return &IndexOutputImpl{util.NewDataOutput(part)}
}
