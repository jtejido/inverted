package store

import (
	"errors"
	"fmt"
	"bitbucket.org/jtejido/inverted/util"
	"io"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"sync"
)

type NoSuchDirectoryError struct {
	msg string
}

func newNoSuchDirectoryError(msg string) *NoSuchDirectoryError {
	return &NoSuchDirectoryError{msg}
}

func (err *NoSuchDirectoryError) Error() string {
	return err.msg
}

type FSDirectorySPI interface {
	OpenInput(string) (IndexInput, error)
}

type FSDirectory struct {
	*DirectoryImpl
	*BaseDirectory
	FSDirectorySPI
	sync.Locker
	path           string
	staleFiles     map[string]bool
	staleFilesLock *sync.RWMutex
	chunkSize      int
}

func newFSDirectory(spi FSDirectorySPI, path string) (d *FSDirectory, err error) {
	d = &FSDirectory{
		Locker:         &sync.Mutex{},
		path:           path,
		staleFiles:     make(map[string]bool),
		staleFilesLock: &sync.RWMutex{},
		chunkSize:      math.MaxInt32,
	}
	d.DirectoryImpl = NewDirectoryImpl(d)
	d.BaseDirectory = NewBaseDirectory(d)
	d.FSDirectorySPI = spi

	if fi, err := os.Stat(path); err == nil && !fi.IsDir() {
		return d, newNoSuchDirectoryError(fmt.Sprintf("file '%v' exists but is not a directory", path))
	}

	// TODO default to native lock factory
	d.SetLockFactory(NewSimpleFSLockFactory(path))
	return d, nil
}

func OpenFSDirectory(path string) (Directory, error) {
	// TODO support native implementations
	dir, err := NewSimpleFSDirectory(path)
	if err != nil {
		return nil, err
	}
	return dir, nil
}

func (d *FSDirectory) SetLockFactory(lockFactory LockFactory) {
	d.BaseDirectory.SetLockFactory(lockFactory)

	if lf, ok := lockFactory.(*SimpleFSLockFactory); ok {
		if lf.lockDir == "" {
			lf.lockDir = d.path
			lf.lockPrefix = ""
		} else if lf.lockDir == d.path {
			lf.lockPrefix = ""
		}
	}
}

func FSDirectoryListAll(path string) (paths []string, err error) {
	f, err := os.Open(path)
	if os.IsNotExist(err) {
		return nil, newNoSuchDirectoryError(fmt.Sprintf("directory '%v' does not exist", path))
	} else if err != nil {
		return nil, err
	}
	defer f.Close()
	fi, err := f.Stat()
	if !fi.IsDir() {
		return nil, newNoSuchDirectoryError(fmt.Sprintf("file '%v' exists but is not a directory", path))
	}

	return f.Readdirnames(0)
}

func (d *FSDirectory) ListAll() (paths []string, err error) {
	d.EnsureOpen()
	return FSDirectoryListAll(d.path)
}

func (d *FSDirectory) FileExists(name string) bool {
	d.EnsureOpen()
	_, err := os.Stat(filepath.Join(d.path, name))
	return err == nil || os.IsExist(err)
}

func (d *FSDirectory) FileLength(name string) (n int64, err error) {
	d.EnsureOpen()
	fi, err := os.Stat(filepath.Join(d.path, name))
	if err != nil {
		return 0, err
	}
	return fi.Size(), nil
}

func (d *FSDirectory) DeleteFile(name string) (err error) {
	d.EnsureOpen()
	if err = os.Remove(filepath.Join(d.path, name)); err == nil {
		d.staleFilesLock.Lock()
		defer d.staleFilesLock.Unlock()
		delete(d.staleFiles, name)
	}
	return
}

func (d *FSDirectory) CreateOutput(name string) (out IndexOutput, err error) {
	d.EnsureOpen()
	err = d.ensureCanCreateAndWrite(name)
	if err != nil {
		return nil, err
	}

	return newFSIndexOutput(d, name, os.O_CREATE|os.O_EXCL|os.O_RDWR)
}

func (d *FSDirectory) OpenOutput(name string) (out IndexOutput, err error) {
	d.EnsureOpen()

	err = d.ensureCanOpenAndWriteFile(name)
	if err != nil {
		return nil, err
	}

	return newFSIndexOutput(d, name, os.O_RDWR)
}

func (d *FSDirectory) ensureCanCreateAndWrite(name string) error {
	err := os.MkdirAll(d.path, os.ModePerm)
	if err != nil {
		return errors.New(fmt.Sprintf("Cannot create directory %v: %v", d.path, err))
	}

	filename := filepath.Join(d.path, name)
	_, err = os.Stat(filename)
	if err == nil || os.IsExist(err) {
		err = os.Remove(filename)
		if err != nil {
			return errors.New(fmt.Sprintf("Cannot overwrite %v/%v: %v", d.path, name, err))
		}
	}
	return nil
}

func (d *FSDirectory) ensureCanOpenAndWriteFile(name string) error {

	filename := filepath.Join(d.path, name)
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return errors.New(fmt.Sprintf("Cannot find file %v/%v: %v", d.path, name, err))
	}
	return nil
}

func (d *FSDirectory) onIndexOutputClosed(name string) {
	d.staleFilesLock.Lock()
	defer d.staleFilesLock.Unlock()
	d.staleFiles[name] = true
}

func (d *FSDirectory) Sync(names []string) (err error) {
	d.EnsureOpen()

	toSync := make(map[string]bool)
	d.staleFilesLock.RLock()
	for _, name := range names {
		if _, ok := d.staleFiles[name]; ok {
			continue
		}
		toSync[name] = true
	}
	d.staleFilesLock.RUnlock()

	for name, _ := range toSync {
		err = d.fsync(name)
		if err != nil {
			return err
		}
	}

	if len(toSync) > 0 {
		err = util.Fsync(d.path, true)
		if err != nil {
			return err
		}
	}

	for name, _ := range toSync {
		delete(d.staleFiles, name)
	}
	return
}

func (d *FSDirectory) LockID() string {
	d.EnsureOpen()
	var digest int
	for _, ch := range d.path {
		digest = 31*digest + int(ch)
	}
	return fmt.Sprintf("lucene-%v", strconv.FormatUint(uint64(digest), 10))
}

func (d *FSDirectory) Close() error {
	d.Lock()
	defer d.Unlock()
	d.IsOpen = false
	return nil
}

func (d *FSDirectory) fsync(name string) error {
	return nil
}

func (d *FSDirectory) String() string {
	return fmt.Sprintf("FSDirectory@%v", d.DirectoryImpl.String())
}

// interface that implements two io interfaces
type OutputWriterAtCloser interface {
	io.WriteCloser  // for both Write() and Close()
	io.WriterAt 	// for WriteAt()
}

type FilteredWriteCloser struct {
	OutputWriterAtCloser
	f func(p []byte, off int64, isWriteAt bool) (int, error)
}

func filter(w OutputWriterAtCloser, f func(p []byte, off int64, isWriteAt bool) (int, error)) *FilteredWriteCloser {
	return &FilteredWriteCloser{w, f}
}

func (w *FilteredWriteCloser) Write(p []byte) (int, error) {
	return w.f(p, 0, false)
}

func (w *FilteredWriteCloser) WriteAt(p []byte, off int64) (int, error) {
	return w.f(p, off, true)
}

type FSIndexOutput struct {
	*OutputStreamIndexOutput
	*FSDirectory
	name string
}

func newFSIndexOutput(parent *FSDirectory, name string, flag int) (*FSIndexOutput, error) {
	file, err := os.OpenFile(filepath.Join(parent.path, name), flag, 0644)

	if err != nil {
		return nil, err
	}
	return &FSIndexOutput{
		newOutputStreamIndexOutput(filter(file, func(p []byte, off int64, isWriteAt bool) (int, error) {
			chunk := CHUNK_SIZE
			var n int
			var err error
			offset, length := 0, len(p)
			for length > 0 {
				if length < chunk {
					chunk = length
				}
				if isWriteAt == false {
					n, err = file.Write(p[offset : offset+chunk])
				} else {
					n, err = file.WriteAt(p[offset : offset+chunk], off)
				}
				
				if err != nil {
					return offset, err
				}
				length -= n
				offset += n
			}
			return offset, nil
		}), CHUNK_SIZE),
		parent,
		name,
	}, nil
}

func (out *FSIndexOutput) Close() (err error) {
	defer func() {
		err = out.OutputStreamIndexOutput.Close()
	}()
	out.onIndexOutputClosed(out.name)
	return nil
}

