package goffer

import (
    "bitbucket.org/jtejido/inverted/index"
)

type TermStreamsPriorityQueue struct {
    termStreams      []index.TermsStream
    termsStreamQueue *index.TermsPriorityQueue
    lastTerm         *index.Term
}

func newTermStreamsPriorityQueue(streams []index.TermsStream) *TermStreamsPriorityQueue {
    ans := &TermStreamsPriorityQueue{termStreams: streams}
    ans.ResetTermsStream()
    return ans
}

/**
 * Reset terms stream.
 */
func (tspq *TermStreamsPriorityQueue) ResetTermsStream() {
    tspq.termsStreamQueue = index.NewTermsPriorityQueue()

    for _, termStream := range tspq.termStreams {
        termStream.ResetTermsStream()

        // Skip "empty" containers
        if termStream.CurrentTerm() != nil {
            tspq.termsStreamQueue.Put(termStream)
        }
    }

    tspq.NextTerm()
}

/**
 * Skip terms stream up to specified term preffix.
 *
 * Prefix contains fully specified field info and portion of searched term
 */
func (tspq *TermStreamsPriorityQueue) SkipTo(prefix *index.Term) {
    termStreams := make([]index.TermsStream, 0)

    for {

        termStream := tspq.termsStreamQueue.Pop()

        if termStream == nil {
            break
        }

        termStreams = append(termStream, termStreams)

    }

    for _, termStream := range termStreams {
        termStream.SkipTo(prefix)

        if termStream.CurrentTerm() != nil {
            tspq.termsStreamQueue.Put(termStream)
        }
    }

    tspq.NextTerm()
}

/**
 * Scans term streams and returns next term
 */
func (tspq *TermStreamsPriorityQueue) NextTerm() *index.Term {

    for {
        termStream := tspq.termsStreamQueue.Pop()

        if termStream == nil {
            break
        }

        if tspq.termsStreamQueue.Top() == nil || tspq.termsStreamQueue.Top().CurrentTerm().Key() != termStream.CurrentTerm().Key() {
            // We got new term
            tspq.lastTerm = termStream.CurrentTerm()

            if termStream.NextTerm() != nil {
                // Put segment back into the priority queue
                tspq.termsStreamQueue.Put(termStream)
            }

            return tspq.lastTerm
        }

        if termStream.NextTerm() != nil {
            // Put segment back into the priority queue
            tspq.termsStreamQueue.Put(termStream)
        }
    }

    // End of stream
    tspq.lastTerm = nil

    return nil
}

/**
 * Returns term in current position
 */
func (tspq *TermStreamsPriorityQueue) CurrentTerm() *index.Term {
    return tspq.lastTerm
}

/**
 * Close terms stream
 *
 * Should be used for resources clean up if stream is not read up to the end
 */
func (tspq *TermStreamsPriorityQueue) CloseTermsStream() {
    for {
        termStream := tspq.termsStreamQueue.Pop()

        if termStream == nil {
            break
        }

        termStream.CloseTermsStream()
    }

    tspq.termsStreamQueue = nil
    tspq.lastTerm = nil
}
