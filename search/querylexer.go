package search

import (
    . "bitbucket.org/jtejido/inverted/fsm"
    "fmt"
    "strconv"
    "strings"
    "unicode/utf8"
)

type QueryLexer struct {
    AbstractFSM
    lexemes             []*QueryToken
    queryString         []rune
    queryStringPosition int
    currentLexeme       []rune // accomodate double-char type modifiers
}

const (
    ST_WHITE_SPACE = iota
    ST_SYNT_LEXEME
    ST_LEXEME
    ST_QUOTED_LEXEME
    ST_ESCAPED_CHAR
    ST_ESCAPED_QCHAR
    ST_LEXEME_MODIFIER
    ST_NUMBER
    ST_MANTISSA
    ST_ERROR

    /** Input symbols */
    IN_WHITE_SPACE = iota
    IN_SYNT_CHAR
    IN_LEXEME_MODIFIER
    IN_ESCAPE_CHAR
    IN_QUOTE
    IN_DECIMAL_POINT
    IN_ASCII_DIGIT
    IN_CHAR
    IN_MUTABLE_CHAR

    QUERY_WHITE_SPACE_CHARS      = " \n\r\t"
    QUERY_SYNT_CHARS             = ":()[]{}!|&"
    QUERY_MUTABLE_CHARS          = "+-"
    QUERY_DOUBLECHARLEXEME_CHARS = "|&"
    QUERY_LEXEMEMODIFIER_CHARS   = "~^"
    QUERY_ASCIIDIGITS_CHARS      = "0123456789"
)

var err error

func NewQueryLexer() (*QueryLexer, error) {
    states := map[int]int{
        ST_WHITE_SPACE:     ST_WHITE_SPACE,
        ST_SYNT_LEXEME:     ST_SYNT_LEXEME,
        ST_LEXEME:          ST_LEXEME,
        ST_QUOTED_LEXEME:   ST_QUOTED_LEXEME,
        ST_ESCAPED_CHAR:    ST_ESCAPED_CHAR,
        ST_ESCAPED_QCHAR:   ST_ESCAPED_QCHAR,
        ST_LEXEME_MODIFIER: ST_LEXEME_MODIFIER,
        ST_NUMBER:          ST_NUMBER,
        ST_MANTISSA:        ST_MANTISSA,
        ST_ERROR:           ST_ERROR,
    }

    inputAlphabet := map[int]int{
        IN_WHITE_SPACE:     IN_WHITE_SPACE,
        IN_SYNT_CHAR:       IN_SYNT_CHAR,
        IN_MUTABLE_CHAR:    IN_MUTABLE_CHAR,
        IN_LEXEME_MODIFIER: IN_LEXEME_MODIFIER,
        IN_ESCAPE_CHAR:     IN_ESCAPE_CHAR,
        IN_QUOTE:           IN_QUOTE,
        IN_DECIMAL_POINT:   IN_DECIMAL_POINT,
        IN_ASCII_DIGIT:     IN_ASCII_DIGIT,
        IN_CHAR:            IN_CHAR,
    }

    ql := &QueryLexer{
        AbstractFSM: AbstractFSM{
            States:            states,
            InputAlphabet:     inputAlphabet,
            Rules:             make(map[int]map[int]int),
            InputActions:      make(map[int]map[int][]*FSMAction),
            EntryActions:      make(map[int][]*FSMAction),
            TransitionActions: make(map[int]map[int][]*FSMAction),
            ExitActions:       make(map[int][]*FSMAction),
        },
    }

    err = ql.initRules()

    if err != nil {
        return nil, err
    }

    syntaxLexemeAction := NewFSMAction(ql.AddQuerySyntaxLexeme)
    lexemeModifierAction := NewFSMAction(ql.AddLexemeModifier)
    addLexemeAction := NewFSMAction(ql.AddLexeme)
    addQuotedLexemeAction := NewFSMAction(ql.AddQuotedLexeme)
    addNumberLexemeAction := NewFSMAction(ql.AddNumberLexeme)
    addLexemeCharAction := NewFSMAction(ql.AddLexemeChar)

    /** Syntax lexeme */
    err = ql.AddEntryAction(ST_SYNT_LEXEME, syntaxLexemeAction)
    // Two lexemes in succession
    err = ql.AddTransitionAction(ST_SYNT_LEXEME, ST_SYNT_LEXEME, syntaxLexemeAction)

    /** Lexeme */
    err = ql.AddEntryAction(ST_LEXEME, addLexemeCharAction)
    err = ql.AddTransitionAction(ST_LEXEME, ST_LEXEME, addLexemeCharAction)
    // ST_ESCAPED_CHAR => ST_LEXEME transition is covered by ST_LEXEME entry action

    err = ql.AddTransitionAction(ST_LEXEME, ST_WHITE_SPACE, addLexemeAction)
    err = ql.AddTransitionAction(ST_LEXEME, ST_SYNT_LEXEME, addLexemeAction)
    err = ql.AddTransitionAction(ST_LEXEME, ST_QUOTED_LEXEME, addLexemeAction)
    err = ql.AddTransitionAction(ST_LEXEME, ST_LEXEME_MODIFIER, addLexemeAction)
    err = ql.AddTransitionAction(ST_LEXEME, ST_NUMBER, addLexemeAction)
    err = ql.AddTransitionAction(ST_LEXEME, ST_MANTISSA, addLexemeAction)

    /** Quoted lexeme */
    // We don't need entry action (skip quote)
    err = ql.AddTransitionAction(ST_QUOTED_LEXEME, ST_QUOTED_LEXEME, addLexemeCharAction)
    err = ql.AddTransitionAction(ST_ESCAPED_QCHAR, ST_QUOTED_LEXEME, addLexemeCharAction)
    // Closing quote changes state to the ST_WHITE_SPACE   other states are not used
    err = ql.AddTransitionAction(ST_QUOTED_LEXEME, ST_WHITE_SPACE, addQuotedLexemeAction)

    /** Lexeme modifier */
    err = ql.AddEntryAction(ST_LEXEME_MODIFIER, lexemeModifierAction)

    /** Number */
    err = ql.AddEntryAction(ST_NUMBER, addLexemeCharAction)
    err = ql.AddEntryAction(ST_MANTISSA, addLexemeCharAction)
    err = ql.AddTransitionAction(ST_NUMBER, ST_NUMBER, addLexemeCharAction)
    // ST_NUMBER => ST_MANTISSA transition is covered by ST_MANTISSA entry action
    err = ql.AddTransitionAction(ST_MANTISSA, ST_MANTISSA, addLexemeCharAction)

    err = ql.AddTransitionAction(ST_NUMBER, ST_WHITE_SPACE, addNumberLexemeAction)
    err = ql.AddTransitionAction(ST_NUMBER, ST_SYNT_LEXEME, addNumberLexemeAction)
    err = ql.AddTransitionAction(ST_NUMBER, ST_LEXEME_MODIFIER, addNumberLexemeAction)
    err = ql.AddTransitionAction(ST_MANTISSA, ST_WHITE_SPACE, addNumberLexemeAction)
    err = ql.AddTransitionAction(ST_MANTISSA, ST_SYNT_LEXEME, addNumberLexemeAction)
    err = ql.AddTransitionAction(ST_MANTISSA, ST_LEXEME_MODIFIER, addNumberLexemeAction)

    if err != nil {
        return nil, err
    }
    return ql, nil

}

func (ql *QueryLexer) initRules() error {
    lexemeModifierErrorAction := NewFSMAction(ql.LexModifierErrException)
    quoteWithinLexemeErrorAction := NewFSMAction(ql.QuoteWithinLexemeErrException)
    wrongNumberErrorAction := NewFSMAction(ql.WrongNumberErrException)

    var err_a error = nil
    err_a = ql.AddRule(ST_WHITE_SPACE, IN_WHITE_SPACE, ST_WHITE_SPACE, nil)
    if err_a == nil {
        err_a = ql.AddRule(ST_WHITE_SPACE, IN_SYNT_CHAR, ST_SYNT_LEXEME, nil)
        if err_a == nil {
            err_a = ql.AddRule(ST_WHITE_SPACE, IN_MUTABLE_CHAR, ST_SYNT_LEXEME, nil)
            if err_a == nil {
                err_a = ql.AddRule(ST_WHITE_SPACE, IN_LEXEME_MODIFIER, ST_LEXEME_MODIFIER, nil)
                if err_a == nil {
                    err_a = ql.AddRule(ST_WHITE_SPACE, IN_ESCAPE_CHAR, ST_ESCAPED_CHAR, nil)
                    if err_a == nil {
                        err_a = ql.AddRule(ST_WHITE_SPACE, IN_QUOTE, ST_QUOTED_LEXEME, nil)
                        if err_a == nil {
                            err_a = ql.AddRule(ST_WHITE_SPACE, IN_DECIMAL_POINT, ST_LEXEME, nil)
                            if err_a == nil {
                                err_a = ql.AddRule(ST_WHITE_SPACE, IN_ASCII_DIGIT, ST_LEXEME, nil)
                                if err_a == nil {
                                    err_a = ql.AddRule(ST_WHITE_SPACE, IN_CHAR, ST_LEXEME, nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_a != nil {
        return err_a
    }

    var err_b error = nil
    err_b = ql.AddRule(ST_SYNT_LEXEME, IN_WHITE_SPACE, ST_WHITE_SPACE, nil)
    if err_b == nil {
        err_b = ql.AddRule(ST_SYNT_LEXEME, IN_SYNT_CHAR, ST_SYNT_LEXEME, nil)
        if err_b == nil {
            err_b = ql.AddRule(ST_SYNT_LEXEME, IN_MUTABLE_CHAR, ST_SYNT_LEXEME, nil)
            if err_b == nil {
                err_b = ql.AddRule(ST_SYNT_LEXEME, IN_LEXEME_MODIFIER, ST_LEXEME_MODIFIER, nil)
                if err_b == nil {
                    err_b = ql.AddRule(ST_SYNT_LEXEME, IN_ESCAPE_CHAR, ST_ESCAPED_CHAR, nil)
                    if err_b == nil {
                        err_b = ql.AddRule(ST_SYNT_LEXEME, IN_QUOTE, ST_QUOTED_LEXEME, nil)
                        if err_b == nil {
                            err_b = ql.AddRule(ST_SYNT_LEXEME, IN_DECIMAL_POINT, ST_LEXEME, nil)
                            if err_b == nil {
                                err_b = ql.AddRule(ST_SYNT_LEXEME, IN_ASCII_DIGIT, ST_LEXEME, nil)
                                if err_b == nil {
                                    err_b = ql.AddRule(ST_SYNT_LEXEME, IN_CHAR, ST_LEXEME, nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_b != nil {
        return err_b
    }

    var err_c error = nil

    err_c = ql.AddRule(ST_LEXEME, IN_WHITE_SPACE, ST_WHITE_SPACE, nil)
    if err_c == nil {
        err_c = ql.AddRule(ST_LEXEME, IN_SYNT_CHAR, ST_SYNT_LEXEME, nil)
        if err_c == nil {
            err_c = ql.AddRule(ST_LEXEME, IN_MUTABLE_CHAR, ST_LEXEME, nil)
            if err_c == nil {
                err_c = ql.AddRule(ST_LEXEME, IN_LEXEME_MODIFIER, ST_LEXEME_MODIFIER, nil)
                if err_c == nil {
                    err_c = ql.AddRule(ST_LEXEME, IN_ESCAPE_CHAR, ST_ESCAPED_CHAR, nil)
                    if err_c == nil {
                        err_c = ql.AddRule(ST_LEXEME, IN_QUOTE, ST_ERROR, quoteWithinLexemeErrorAction)
                        if err_c == nil {
                            err_c = ql.AddRule(ST_LEXEME, IN_DECIMAL_POINT, ST_LEXEME, nil)
                            if err_c == nil {
                                err_c = ql.AddRule(ST_LEXEME, IN_ASCII_DIGIT, ST_LEXEME, nil)
                                if err_c == nil {
                                    err_c = ql.AddRule(ST_LEXEME, IN_CHAR, ST_LEXEME, nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_c != nil {
        return err_c
    }

    var err_d error = nil

    err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_WHITE_SPACE, ST_QUOTED_LEXEME, nil)
    if err_d == nil {
        err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_SYNT_CHAR, ST_QUOTED_LEXEME, nil)
        if err_d == nil {
            err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_MUTABLE_CHAR, ST_QUOTED_LEXEME, nil)
            if err_d == nil {
                err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_LEXEME_MODIFIER, ST_QUOTED_LEXEME, nil)
                if err_d == nil {
                    err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_ESCAPE_CHAR, ST_ESCAPED_QCHAR, nil)
                    if err_d == nil {
                        err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_QUOTE, ST_WHITE_SPACE, nil)
                        if err_d == nil {
                            err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_DECIMAL_POINT, ST_QUOTED_LEXEME, nil)
                            if err_d == nil {
                                err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_ASCII_DIGIT, ST_QUOTED_LEXEME, nil)
                                if err_d == nil {
                                    err_d = ql.AddRule(ST_QUOTED_LEXEME, IN_CHAR, ST_QUOTED_LEXEME, nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_d != nil {
        return err_d
    }

    var err_e error = nil

    err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_WHITE_SPACE, ST_LEXEME, nil)
    if err_e == nil {
        err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_SYNT_CHAR, ST_LEXEME, nil)
        if err_e == nil {
            err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_MUTABLE_CHAR, ST_LEXEME, nil)
            if err_e == nil {
                err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_LEXEME_MODIFIER, ST_LEXEME, nil)
                if err_e == nil {
                    err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_ESCAPE_CHAR, ST_LEXEME, nil)
                    if err_e == nil {
                        err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_QUOTE, ST_LEXEME, nil)
                        if err_e == nil {
                            err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_DECIMAL_POINT, ST_LEXEME, nil)
                            if err_e == nil {
                                err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_ASCII_DIGIT, ST_LEXEME, nil)
                                if err_e == nil {
                                    err_e = ql.AddRule(ST_ESCAPED_CHAR, IN_CHAR, ST_LEXEME, nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_e != nil {
        return err_e
    }

    var err_f error = nil

    err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_WHITE_SPACE, ST_QUOTED_LEXEME, nil)
    if err_f == nil {
        err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_SYNT_CHAR, ST_QUOTED_LEXEME, nil)
        if err_f == nil {
            err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_MUTABLE_CHAR, ST_QUOTED_LEXEME, nil)
            if err_f == nil {
                err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_LEXEME_MODIFIER, ST_QUOTED_LEXEME, nil)
                if err_f == nil {
                    err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_ESCAPE_CHAR, ST_QUOTED_LEXEME, nil)
                    if err_f == nil {
                        err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_QUOTE, ST_QUOTED_LEXEME, nil)
                        if err_f == nil {
                            err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_DECIMAL_POINT, ST_QUOTED_LEXEME, nil)
                            if err_f == nil {
                                err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_ASCII_DIGIT, ST_QUOTED_LEXEME, nil)
                                if err_f == nil {
                                    err_f = ql.AddRule(ST_ESCAPED_QCHAR, IN_CHAR, ST_QUOTED_LEXEME, nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_f != nil {
        return err_f
    }

    var err_g error = nil

    err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_WHITE_SPACE, ST_WHITE_SPACE, nil)
    if err_g == nil {
        err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_SYNT_CHAR, ST_SYNT_LEXEME, nil)
        if err_g == nil {
            err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_MUTABLE_CHAR, ST_SYNT_LEXEME, nil)
            if err_g == nil {
                err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_LEXEME_MODIFIER, ST_LEXEME_MODIFIER, nil)
                if err_g == nil {
                    err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_ESCAPE_CHAR, ST_ERROR, lexemeModifierErrorAction)
                    if err_g == nil {
                        err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_QUOTE, ST_ERROR, lexemeModifierErrorAction)
                        if err_g == nil {
                            err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_DECIMAL_POINT, ST_MANTISSA, nil)
                            if err_g == nil {
                                err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_ASCII_DIGIT, ST_NUMBER, nil)
                                if err_g == nil {
                                    err_g = ql.AddRule(ST_LEXEME_MODIFIER, IN_CHAR, ST_ERROR, lexemeModifierErrorAction)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_g != nil {
        return err_g
    }

    var err_h error = nil

    err_h = ql.AddRule(ST_NUMBER, IN_WHITE_SPACE, ST_WHITE_SPACE, nil)
    if err_h == nil {
        err_h = ql.AddRule(ST_NUMBER, IN_SYNT_CHAR, ST_SYNT_LEXEME, nil)
        if err_h == nil {
            err_h = ql.AddRule(ST_NUMBER, IN_MUTABLE_CHAR, ST_SYNT_LEXEME, nil)
            if err_h == nil {
                err_h = ql.AddRule(ST_NUMBER, IN_LEXEME_MODIFIER, ST_LEXEME_MODIFIER, nil)
                if err_h == nil {
                    err_h = ql.AddRule(ST_NUMBER, IN_ESCAPE_CHAR, ST_ERROR, wrongNumberErrorAction)
                    if err_h == nil {
                        err_h = ql.AddRule(ST_NUMBER, IN_QUOTE, ST_ERROR, wrongNumberErrorAction)
                        if err_h == nil {
                            err_h = ql.AddRule(ST_NUMBER, IN_DECIMAL_POINT, ST_MANTISSA, nil)
                            if err_h == nil {
                                err_h = ql.AddRule(ST_NUMBER, IN_ASCII_DIGIT, ST_NUMBER, nil)
                                if err_h == nil {
                                    err_h = ql.AddRule(ST_NUMBER, IN_CHAR, ST_ERROR, wrongNumberErrorAction)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_h != nil {
        return err_h
    }

    var err_i error = nil

    err_i = ql.AddRule(ST_MANTISSA, IN_WHITE_SPACE, ST_WHITE_SPACE, nil)
    if err_i == nil {
        err_i = ql.AddRule(ST_MANTISSA, IN_SYNT_CHAR, ST_SYNT_LEXEME, nil)
        if err_i == nil {
            err_i = ql.AddRule(ST_MANTISSA, IN_MUTABLE_CHAR, ST_SYNT_LEXEME, nil)
            if err_i == nil {
                err_i = ql.AddRule(ST_MANTISSA, IN_LEXEME_MODIFIER, ST_LEXEME_MODIFIER, nil)
                if err_i == nil {
                    err_i = ql.AddRule(ST_MANTISSA, IN_ESCAPE_CHAR, ST_ERROR, wrongNumberErrorAction)
                    if err_i == nil {
                        err_i = ql.AddRule(ST_MANTISSA, IN_QUOTE, ST_ERROR, wrongNumberErrorAction)
                        if err_i == nil {
                            err_i = ql.AddRule(ST_MANTISSA, IN_DECIMAL_POINT, ST_ERROR, wrongNumberErrorAction)
                            if err_i == nil {
                                err_i = ql.AddRule(ST_MANTISSA, IN_ASCII_DIGIT, ST_MANTISSA, nil)
                                if err_i == nil {
                                    err_i = ql.AddRule(ST_MANTISSA, IN_CHAR, ST_ERROR, wrongNumberErrorAction)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err_i != nil {
        return err_i
    }

    return nil

}

func (ql *QueryLexer) Tokenize(inputString string) []*QueryToken {
    ql.Reset()

    ql.lexemes = make([]*QueryToken, 0)
    ql.queryString = make([]rune, utf8.RuneCountInString(inputString))
    ql.queryStringPosition = 0

    for len(inputString) > 0 {
        r, size := utf8.DecodeRuneInString(inputString)
        ql.queryString[ql.queryStringPosition] = r
        ql.queryStringPosition++
        t := translateInput(r)
        ql.Process(t)
        inputString = inputString[size:]
    }

    ql.Process(IN_WHITE_SPACE)

    if ql.GetState() != ST_WHITE_SPACE {
        panic("Unexpected end of query")
    }

    ql.queryString = nil

    return ql.lexemes
}

/*********************************************************************
 * Actions implementation
 *
 * Actions affect on recognized lexemes list
 *********************************************************************/

func (ql *QueryLexer) AddQuerySyntaxLexeme() error {
    lex := ql.queryString[ql.queryStringPosition]
    lexeme := []rune{lex}

    // Process two char lexemes
    if strchr(QUERY_DOUBLECHARLEXEME_CHARS, lex) {
        // increase current position in a query string
        ql.queryStringPosition++

        // duplicate character
        lexeme = append(lexeme, lex)
    }

    token := NewQueryToken(TC_SYNTAX_ELEMENT, lexeme, ql.queryStringPosition)

    // Skip this lexeme if it's a field indicator ':' and treat previous as 'field' instead of 'word'
    if token.typ == TT_FIELD_INDICATOR {
        // pop
        token, ql.lexemes = ql.lexemes[len(ql.lexemes)-1], ql.lexemes[:len(ql.lexemes)-1]
        if token == nil || token.typ != TT_WORD {
            return fmt.Errorf("Field mark ':' must follow field name. ")
        }

        token.typ = TT_FIELD
    }

    ql.lexemes = append(ql.lexemes, token)
    return nil
}

func (ql *QueryLexer) AddLexemeModifier() error {
    ql.lexemes = append(ql.lexemes, NewQueryToken(TC_SYNTAX_ELEMENT, []rune{ql.queryString[ql.queryStringPosition]}, ql.queryStringPosition))
    return nil
}

func (ql *QueryLexer) AddLexeme() error {
    ql.lexemes = append(ql.lexemes, NewQueryToken(TC_WORD, ql.currentLexeme, ql.queryStringPosition-1))
    ql.currentLexeme = nil
    return nil
}

func (ql *QueryLexer) AddQuotedLexeme() error {
    ql.lexemes = append(ql.lexemes, NewQueryToken(TC_PHRASE, ql.currentLexeme, ql.queryStringPosition))
    ql.currentLexeme = nil
    return nil
}

func (ql *QueryLexer) AddNumberLexeme() error {
    ql.lexemes = append(ql.lexemes, NewQueryToken(TC_NUMBER, ql.currentLexeme, ql.queryStringPosition-1))
    ql.currentLexeme = nil
    return nil
}

func (ql *QueryLexer) AddLexemeChar() error {
    ql.currentLexeme = append(ql.currentLexeme, ql.queryString[ql.queryStringPosition])
    return nil
}

/**
 * Position message
 *
 * @return string
 */
func (ql *QueryLexer) positionMsg() string {
    return "Position is " + strconv.Itoa(ql.queryStringPosition) + "."
}

/*********************************************************************
 * Syntax errors actions
 *********************************************************************/
func (ql *QueryLexer) LexModifierErrException() error {
    return fmt.Errorf("Lexeme modifier character can be followed only by number, white space or query syntax element. " + ql.positionMsg())
}
func (ql *QueryLexer) QuoteWithinLexemeErrException() error {
    return fmt.Errorf("Quote within lexeme must be escaped by '\\' char. " + ql.positionMsg())
}
func (ql *QueryLexer) WrongNumberErrException() error {
    return fmt.Errorf("Wrong number syntax." + ql.positionMsg())
}

func translateInput(char rune) int {

    if strchr(QUERY_WHITE_SPACE_CHARS, char) {
        return IN_WHITE_SPACE
    } else if strchr(QUERY_SYNT_CHARS, char) {
        return IN_SYNT_CHAR
    } else if strchr(QUERY_MUTABLE_CHARS, char) {
        return IN_MUTABLE_CHAR
    } else if strchr(QUERY_LEXEMEMODIFIER_CHARS, char) {
        return IN_LEXEME_MODIFIER
    } else if strchr(QUERY_ASCIIDIGITS_CHARS, char) {
        return IN_ASCII_DIGIT
    } else if strchr(QUERY_ASCIIDIGITS_CHARS, char) {
        return IN_ASCII_DIGIT
    } else if char == '"' {
        return IN_QUOTE
    } else if char == '.' {
        return IN_DECIMAL_POINT
    } else if char == '\\' {
        return IN_ESCAPE_CHAR
    } else {
        return IN_CHAR
    }

}

func strchr(s string, ch rune) bool {
    return strings.IndexFunc(s, func(c rune) bool { return c == ch }) >= 0
}
