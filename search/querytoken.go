package search

import (
    // "fmt"
    "strings"
)

type QueryToken struct {
    text     []rune
    strVal   string
    position int
    typ      int
}

const (
    TT_WORD             = iota // Word
    TT_PHRASE                  // Phrase (one or several quoted words)
    TT_FIELD                   // Field name in 'field:word', field:<phrase> or field:(<subquery>) pairs
    TT_FIELD_INDICATOR         // ':'
    TT_REQUIRED                // '+'
    TT_PROHIBITED              // '-'
    TT_FUZZY_PROX_MARK         // '~'
    TT_BOOSTING_MARK           // '^'
    TT_RANGE_INCL_START        // '['
    TT_RANGE_INCL_END          // ']'
    TT_RANGE_EXCL_START        // '{'
    TT_RANGE_EXCL_END          // '}'
    TT_SUBQUERY_START          // '('
    TT_SUBQUERY_END            // ')'
    TT_AND_LEXEME              // 'AND' or 'and'
    TT_OR_LEXEME               // 'OR'  or 'or'
    TT_NOT_LEXEME              // 'NOT' or 'not'
    TT_TO_LEXEME               // 'TO'  or 'to'
    TT_NUMBER                  // Number, like: 10, 0.8, .64, ....
)
const (
    TC_WORD           = iota // Word
    TC_PHRASE                // Phrase (one or several quoted words)
    TC_NUMBER                // Nubers, which are used with syntax elements. Ex. roam~0.8
    TC_SYNTAX_ELEMENT        // +  -  ( )  [ ]  { }  !  ||  && ~ ^
)

func NewQueryToken(tokenCategory int, tokenText []rune, position int) *QueryToken {
    qt := new(QueryToken)
    qt.text = tokenText
    qt.strVal = string(tokenText)
    qt.position = position + 1

    switch tokenCategory {
    case TC_WORD:
        if strings.ToLower(string(tokenText)) == "and" {
            qt.typ = TT_AND_LEXEME
        } else if strings.ToLower(string(tokenText)) == "or" {
            qt.typ = TT_OR_LEXEME
        } else if strings.ToLower(string(tokenText)) == "not" {
            qt.typ = TT_NOT_LEXEME
        } else if strings.ToLower(string(tokenText)) == "to" {
            qt.typ = TT_TO_LEXEME
        } else {
            qt.typ = TT_WORD
        }
        break

    case TC_PHRASE:
        qt.typ = TT_PHRASE
        break

    case TC_NUMBER:
        qt.typ = TT_NUMBER
        break

    case TC_SYNTAX_ELEMENT:
        switch string(tokenText) {
        case ":":
            qt.typ = TT_FIELD_INDICATOR
            break

        case "+":
            qt.typ = TT_REQUIRED
            break

        case "-":
            qt.typ = TT_PROHIBITED
            break

        case "~":
            qt.typ = TT_FUZZY_PROX_MARK
            break

        case "^":
            qt.typ = TT_BOOSTING_MARK
            break

        case "[":
            qt.typ = TT_RANGE_INCL_START
            break

        case "]":
            qt.typ = TT_RANGE_INCL_END
            break

        case "{":
            qt.typ = TT_RANGE_EXCL_START
            break

        case "}":
            qt.typ = TT_RANGE_EXCL_END
            break

        case "(":
            qt.typ = TT_SUBQUERY_START
            break

        case ")":
            qt.typ = TT_SUBQUERY_END
            break

        case "!":
            qt.typ = TT_NOT_LEXEME
            break

        case "&&":
            qt.typ = TT_AND_LEXEME
            break

        case "||":
            qt.typ = TT_OR_LEXEME
            break

        default:
            panic("Unrecognized query syntax lexeme.")
        }
        break
    default:
        panic("Unrecognized lexeme type.")
    }

    return qt
}

func lexTypes() []int {
    return []int{
        TT_WORD,
        TT_PHRASE,
        TT_FIELD,
        TT_FIELD_INDICATOR,
        TT_REQUIRED,
        TT_PROHIBITED,
        TT_FUZZY_PROX_MARK,
        TT_BOOSTING_MARK,
        TT_RANGE_INCL_START,
        TT_RANGE_INCL_END,
        TT_RANGE_EXCL_START,
        TT_RANGE_EXCL_END,
        TT_SUBQUERY_START,
        TT_SUBQUERY_END,
        TT_AND_LEXEME,
        TT_OR_LEXEME,
        TT_NOT_LEXEME,
        TT_TO_LEXEME,
        TT_NUMBER,
    }
}
