package queryentry

import (
    "bitbucket.org/jtejido/inverted/search/query"
    "bitbucket.org/jtejido/inverted/search/query/preprocessing"
)

type Term struct {
    *BaseQueryEntry
    term       string
    field      string
    fuzzyQuery bool
    similarity float32
}

func NewTerm(term string, field string) *Term {
    ans := &Term{
        term:       term,
        field:      field,
        similarity: 1.0,
    }

    ans.BaseQueryEntry = newBaseQueryEntry()

    return ans
}

func (term *Term) Query() query.Query {
    var q query.Query

    if term.fuzzyQuery {
        q = preprocessing.NewFuzzy(term.term, term.field, term.similarity)
        q.SetBoost(term.boost)
        return q
    }

    q = preprocessing.NewTerm(term.term, term.field)
    q.SetBoost(term.boost)

    return q
}

func (term *Term) ProcessFuzzyProximityModifier(parameter float32) {

    term.fuzzyQuery = true

    if parameter != 0.0 {
        term.similarity = parameter
    } else {
        term.similarity = query.DEFAULT_MIN_SIMILARITY
    }

}
