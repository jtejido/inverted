package queryentry

import (
    "bitbucket.org/jtejido/inverted/search/query"
)

type SubQuery struct {
    *BaseQueryEntry
    query query.Query
}

func NewSubQuery(query query.Query) *SubQuery {
    ans := &SubQuery{
        query: query,
    }

    ans.BaseQueryEntry = newBaseQueryEntry()

    return ans
}

func (sq *SubQuery) Query() query.Query {
    sq.query.SetBoost(sq.boost)
    return sq.query
}

func (sq *SubQuery) ProcessFuzzyProximityModifier(parameter float32) {
    panic("'~' sign must follow term or phrase")
}
