package queryentry

import (
    "bitbucket.org/jtejido/inverted/search/query"
    "bitbucket.org/jtejido/inverted/search/query/preprocessing"
)

type Phrase struct {
    *BaseQueryEntry
    phrase         string
    field          string
    proximityQuery bool
    wordsDistance  int32
}

func NewPhrase(phrase string, field string) *Phrase {
    ans := &Phrase{
        phrase: phrase,
        field:  field,
    }

    ans.BaseQueryEntry = newBaseQueryEntry()

    return ans
}

func (phrase *Phrase) Query() query.Query {
    q := preprocessing.NewPhrase(phrase.phrase, phrase.field)

    if phrase.proximityQuery {
        q.SetSlop(phrase.wordsDistance)
    }

    q.SetBoost(phrase.boost)

    return q

}

func (phrase *Phrase) ProcessFuzzyProximityModifier(parameter float32) {
    phrase.proximityQuery = true
    phrase.wordsDistance = int32(parameter)
}
