package queryentry

import (
    "bitbucket.org/jtejido/inverted/search/query"
)

type QueryEntry interface {
    Query() query.Query
    ProcessFuzzyProximityModifier(parameter float32)
    Boost(boostFactor float32)
}

type BaseQueryEntry struct {
    boost float32
}

func newBaseQueryEntry() *BaseQueryEntry {
    return &BaseQueryEntry{boost: 1.}
}

func (bqe *BaseQueryEntry) Boost(boostFactor float32) {
    bqe.boost *= boostFactor
}
