package search

import (
    . "bitbucket.org/jtejido/inverted/fsm"
    "bitbucket.org/jtejido/inverted/search/queryentry"
    "fmt"
)

type BooleanExpressionRecognizer struct {
    AbstractFSM
    negativeLiteral    bool
    literal            queryentry.QueryEntry
    conjunctions       [][][]interface{}
    currentConjunction [][]interface{}
}

const (
    ST_START = iota
    ST_LITERAL
    ST_NOT_OPERATOR
    ST_AND_OPERATOR
    ST_OR_OPERATOR
)

const (
    /** Input symbols */
    IN_LITERAL = iota
    IN_NOT_OPERATOR
    IN_AND_OPERATOR
    IN_OR_OPERATOR
)

func NewBooleanExpressionRecognizer() (*BooleanExpressionRecognizer, error) {
    var err error

    states := map[int]int{
        ST_START:        ST_START,
        ST_LITERAL:      ST_LITERAL,
        ST_NOT_OPERATOR: ST_NOT_OPERATOR,
        ST_AND_OPERATOR: ST_AND_OPERATOR,
        ST_OR_OPERATOR:  ST_OR_OPERATOR,
    }

    inputAlphabets := map[int]int{
        IN_LITERAL:      IN_LITERAL,
        IN_NOT_OPERATOR: IN_NOT_OPERATOR,
        IN_AND_OPERATOR: IN_AND_OPERATOR,
        IN_OR_OPERATOR:  IN_OR_OPERATOR,
    }

    ber := &BooleanExpressionRecognizer{
        AbstractFSM: AbstractFSM{
            States:        states,
            InputAlphabet: inputAlphabets,
            Rules:         make(map[int]map[int]int),
            InputActions:  make(map[int]map[int][]*FSMAction),
            EntryActions:  make(map[int][]*FSMAction),
        },
        literal:            nil,
        currentConjunction: make([][]interface{}, 0),
        conjunctions:       make([][][]interface{}, 0),
    }

    err = ber.initRules()

    notOperatorAction := NewFSMAction(ber.NotOperatorAction)
    orOperatorAction := NewFSMAction(ber.OrOperatorAction)
    literalAction := NewFSMAction(ber.LiteralAction)

    err = ber.AddEntryAction(ST_NOT_OPERATOR, notOperatorAction)
    err = ber.AddEntryAction(ST_OR_OPERATOR, orOperatorAction)
    err = ber.AddEntryAction(ST_LITERAL, literalAction)

    if err != nil {
        return nil, err
    }

    return ber, nil

}

func (ber *BooleanExpressionRecognizer) initRules() error {

    emptyOperatorAction := NewFSMAction(ber.EmptyOperatorAction)
    emptyNotOperatorAction := NewFSMAction(ber.EmptyNotOperatorAction)

    var err error = nil
    err = ber.AddRule(ST_START, IN_LITERAL, ST_LITERAL, nil)
    if err == nil {
        err = ber.AddRule(ST_START, IN_NOT_OPERATOR, ST_NOT_OPERATOR, nil)
        if err == nil {
            err = ber.AddRule(ST_LITERAL, IN_AND_OPERATOR, ST_AND_OPERATOR, nil)
            if err == nil {
                err = ber.AddRule(ST_LITERAL, IN_OR_OPERATOR, ST_OR_OPERATOR, nil)
                if err == nil {
                    err = ber.AddRule(ST_LITERAL, IN_LITERAL, ST_LITERAL, emptyOperatorAction)
                    if err == nil {
                        err = ber.AddRule(ST_LITERAL, IN_NOT_OPERATOR, ST_NOT_OPERATOR, emptyNotOperatorAction)
                        if err == nil {
                            err = ber.AddRule(ST_NOT_OPERATOR, IN_LITERAL, ST_LITERAL, nil)
                            if err == nil {
                                err = ber.AddRule(ST_AND_OPERATOR, IN_LITERAL, ST_LITERAL, nil)
                                if err == nil {
                                    err = ber.AddRule(ST_AND_OPERATOR, IN_NOT_OPERATOR, ST_NOT_OPERATOR, nil)
                                    if err == nil {
                                        err = ber.AddRule(ST_OR_OPERATOR, IN_LITERAL, ST_LITERAL, nil)
                                        if err == nil {
                                            err = ber.AddRule(ST_OR_OPERATOR, IN_NOT_OPERATOR, ST_NOT_OPERATOR, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err != nil {
        return err
    }

    return nil

}

func (ber *BooleanExpressionRecognizer) ProcessOperator(operator int) {
    ber.Process(operator)
}

func (ber *BooleanExpressionRecognizer) ProcessLiteral(literal queryentry.QueryEntry) {
    ber.literal = literal

    ber.Process(IN_LITERAL)
}

func (ber *BooleanExpressionRecognizer) FinishExpression() ([][][]interface{}, error) {
    if ber.GetState() != ST_LITERAL {
        return nil, fmt.Errorf("Literal Expected.")
    }

    ber.conjunctions = append(ber.conjunctions, ber.currentConjunction)

    return ber.conjunctions, nil
}

/*********************************************************************
 * Actions implementation
 *********************************************************************/

/**
 * default (omitted) operator processing
 */
func (ber *BooleanExpressionRecognizer) EmptyOperatorAction() error {
    if GetDefaultOperator() == B_AND {
        // Do nothing
    } else {
        ber.OrOperatorAction()
    }

    // Process literal
    ber.LiteralAction()

    return nil
}

func (ber *BooleanExpressionRecognizer) EmptyNotOperatorAction() error {
    if GetDefaultOperator() == B_AND {
        // Do nothing
    } else {
        return ber.OrOperatorAction()
    }

    // Process literal
    return ber.NotOperatorAction()
}

func (ber *BooleanExpressionRecognizer) NotOperatorAction() error {
    ber.negativeLiteral = true
    return nil
}

func (ber *BooleanExpressionRecognizer) OrOperatorAction() error {
    ber.conjunctions = append(ber.conjunctions, ber.currentConjunction)
    ber.currentConjunction = make([][]interface{}, 0)
    return nil
}

func (ber *BooleanExpressionRecognizer) LiteralAction() error {
    // Add literal to the current conjunction
    var item = make([]interface{}, 0)
    item = append(item, ber.literal)
    item = append(item, !ber.negativeLiteral)
    ber.currentConjunction = append(ber.currentConjunction, item)

    // Switch off negative signal
    ber.negativeLiteral = false
    return nil
}
