package search

import (
    . "bitbucket.org/jtejido/inverted/analysis"
    . "bitbucket.org/jtejido/inverted/analysis/analyzer"
    . "bitbucket.org/jtejido/inverted/analysis/analyzer/common/text/lang"
    . "bitbucket.org/jtejido/inverted/analysis/analyzer/tokenizer"
    . "bitbucket.org/jtejido/inverted/fsm"
    "bitbucket.org/jtejido/inverted/index"
    "bitbucket.org/jtejido/inverted/search/query"
    "bitbucket.org/jtejido/inverted/search/queryentry"
    "fmt"
)

type QueryParser struct {
    AbstractFSM
    suppressQueryParsingExceptions bool
    rqFirstTerm                    []rune
    lastToken                      *QueryToken
    defaultOperator                int
    lexer                          *QueryLexer
    context                        *QueryParserContext
    contextStack                   []*QueryParserContext
    currentToken                   *QueryToken
    transformation                 *TransformationSet
    tokens                         []*QueryToken
}

const (
    B_OR = iota
    B_AND
    ST_COMMON_QUERY_ELEMENT
    ST_CLOSEDINT_RQ_START
    ST_CLOSEDINT_RQ_FIRST_TERM
    ST_CLOSEDINT_RQ_TO_TERM
    ST_CLOSEDINT_RQ_LAST_TERM
    ST_CLOSEDINT_RQ_END
    ST_OPENEDINT_RQ_START
    ST_OPENEDINT_RQ_FIRST_TERM
    ST_OPENEDINT_RQ_TO_TERM
    ST_OPENEDINT_RQ_LAST_TERM
    ST_OPENEDINT_RQ_END
)

func NewQueryParser() (*QueryParser, error) {
    var err error

    english := NewEnglish()
    transformers := []TransformationInterface{english}
    states := map[int]int{
        ST_COMMON_QUERY_ELEMENT:    ST_COMMON_QUERY_ELEMENT,
        ST_CLOSEDINT_RQ_START:      ST_CLOSEDINT_RQ_START,
        ST_CLOSEDINT_RQ_FIRST_TERM: ST_CLOSEDINT_RQ_FIRST_TERM,
        ST_CLOSEDINT_RQ_TO_TERM:    ST_CLOSEDINT_RQ_TO_TERM,
        ST_CLOSEDINT_RQ_LAST_TERM:  ST_CLOSEDINT_RQ_LAST_TERM,
        ST_CLOSEDINT_RQ_END:        ST_CLOSEDINT_RQ_END,
        ST_OPENEDINT_RQ_START:      ST_OPENEDINT_RQ_START,
        ST_OPENEDINT_RQ_FIRST_TERM: ST_OPENEDINT_RQ_FIRST_TERM,
        ST_OPENEDINT_RQ_TO_TERM:    ST_OPENEDINT_RQ_TO_TERM,
        ST_OPENEDINT_RQ_LAST_TERM:  ST_OPENEDINT_RQ_LAST_TERM,
        ST_OPENEDINT_RQ_END:        ST_OPENEDINT_RQ_END,
    }

    inputAlphabets := lexTypes()
    inputAlphabet := make(map[int]int)
    for _, v := range inputAlphabets {
        inputAlphabet[v] = v
    }

    qp := &QueryParser{
        AbstractFSM: AbstractFSM{
            States:            states,
            InputAlphabet:     inputAlphabet,
            Rules:             make(map[int]map[int]int),
            InputActions:      make(map[int]map[int][]*FSMAction),
            EntryActions:      make(map[int][]*FSMAction),
            TransitionActions: make(map[int]map[int][]*FSMAction),
            ExitActions:       make(map[int][]*FSMAction),
        },
        suppressQueryParsingExceptions: true,
        rqFirstTerm:                    nil,
        context:                        nil,
        contextStack:                   make([]*QueryParserContext, 0),
        lastToken:                      nil,
        defaultOperator:                B_OR,
        transformation:                 NewTransformationSet(transformers),
    }

    err = qp.initRules()

    if err != nil {
        return nil, err
    }

    addTermEntryAction := NewFSMAction(qp.AddTermEntry)
    addPhraseEntryAction := NewFSMAction(qp.AddPhraseEntry)
    setFieldAction := NewFSMAction(qp.SetField)
    setSignAction := NewFSMAction(qp.SetSign)
    setFuzzyProxAction := NewFSMAction(qp.ProcessFuzzyProximityModifier)
    processModifierParameterAction := NewFSMAction(qp.ProcessModifierParameter)
    subqueryStartAction := NewFSMAction(qp.SubqueryStart)
    subqueryEndAction := NewFSMAction(qp.SubqueryEnd)
    logicalOperatorAction := NewFSMAction(qp.LogicalOperator)
    openedRQFirstTermAction := NewFSMAction(qp.OpenedRQFirstTerm)
    openedRQLastTermAction := NewFSMAction(qp.OpenedRQLastTerm)
    closedRQFirstTermAction := NewFSMAction(qp.ClosedRQFirstTerm)
    closedRQLastTermAction := NewFSMAction(qp.ClosedRQLastTerm)

    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_WORD, addTermEntryAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_PHRASE, addPhraseEntryAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_FIELD, setFieldAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_REQUIRED, setSignAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_PROHIBITED, setSignAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_FUZZY_PROX_MARK, setFuzzyProxAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_NUMBER, processModifierParameterAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_SUBQUERY_START, subqueryStartAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_SUBQUERY_END, subqueryEndAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_AND_LEXEME, logicalOperatorAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_OR_LEXEME, logicalOperatorAction)
    err = qp.AddInputAction(ST_COMMON_QUERY_ELEMENT, TT_NOT_LEXEME, logicalOperatorAction)

    if err != nil {
        return nil, err
    }

    err = qp.AddEntryAction(ST_OPENEDINT_RQ_FIRST_TERM, openedRQFirstTermAction)
    err = qp.AddEntryAction(ST_OPENEDINT_RQ_LAST_TERM, openedRQLastTermAction)
    err = qp.AddEntryAction(ST_CLOSEDINT_RQ_FIRST_TERM, closedRQFirstTermAction)
    err = qp.AddEntryAction(ST_CLOSEDINT_RQ_LAST_TERM, closedRQLastTermAction)

    if err != nil {
        return nil, err
    }

    qp.lexer, err = NewQueryLexer()

    if err != nil {
        return nil, err
    }

    return qp, nil

}

func (qp *QueryParser) initRules() error {
    var err error = nil
    err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_WORD, ST_COMMON_QUERY_ELEMENT, nil)
    if err == nil {
        err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_PHRASE, ST_COMMON_QUERY_ELEMENT, nil)
        if err == nil {
            err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_FIELD, ST_COMMON_QUERY_ELEMENT, nil)
            if err == nil {
                err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_REQUIRED, ST_COMMON_QUERY_ELEMENT, nil)
                if err == nil {
                    err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_PROHIBITED, ST_COMMON_QUERY_ELEMENT, nil)
                    if err == nil {
                        err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_FUZZY_PROX_MARK, ST_COMMON_QUERY_ELEMENT, nil)
                        if err == nil {
                            err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_BOOSTING_MARK, ST_COMMON_QUERY_ELEMENT, nil)
                            if err == nil {
                                err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_RANGE_INCL_START, ST_CLOSEDINT_RQ_START, nil)
                                if err == nil {
                                    err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_RANGE_EXCL_START, ST_OPENEDINT_RQ_START, nil)
                                    if err == nil {
                                        err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_SUBQUERY_START, ST_COMMON_QUERY_ELEMENT, nil)
                                        if err == nil {
                                            err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_SUBQUERY_END, ST_COMMON_QUERY_ELEMENT, nil)
                                            if err == nil {
                                                err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_AND_LEXEME, ST_COMMON_QUERY_ELEMENT, nil)
                                                if err == nil {
                                                    err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_OR_LEXEME, ST_COMMON_QUERY_ELEMENT, nil)
                                                    if err == nil {
                                                        err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_NOT_LEXEME, ST_COMMON_QUERY_ELEMENT, nil)
                                                        if err == nil {
                                                            err = qp.AddRule(ST_COMMON_QUERY_ELEMENT, TT_NUMBER, ST_COMMON_QUERY_ELEMENT, nil)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if err != nil {
        return err
    }

    var err_b error = nil

    err_b = qp.AddRule(ST_CLOSEDINT_RQ_START, TT_WORD, ST_CLOSEDINT_RQ_FIRST_TERM, nil)
    if err_b == nil {
        err_b = qp.AddRule(ST_CLOSEDINT_RQ_FIRST_TERM, TT_TO_LEXEME, ST_CLOSEDINT_RQ_TO_TERM, nil)
        if err_b == nil {
            err_b = qp.AddRule(ST_CLOSEDINT_RQ_TO_TERM, TT_WORD, ST_CLOSEDINT_RQ_LAST_TERM, nil)
            if err_b == nil {
                err_b = qp.AddRule(ST_CLOSEDINT_RQ_LAST_TERM, TT_RANGE_INCL_END, ST_COMMON_QUERY_ELEMENT, nil)
            }
        }
    }

    if err_b != nil {
        return err_b
    }

    var err_c error = nil

    err_c = qp.AddRule(ST_OPENEDINT_RQ_START, TT_WORD, ST_OPENEDINT_RQ_FIRST_TERM, nil)
    if err_c == nil {
        err_c = qp.AddRule(ST_OPENEDINT_RQ_FIRST_TERM, TT_TO_LEXEME, ST_OPENEDINT_RQ_TO_TERM, nil)
        if err_c == nil {
            err_c = qp.AddRule(ST_OPENEDINT_RQ_TO_TERM, TT_WORD, ST_OPENEDINT_RQ_LAST_TERM, nil)
            if err_c == nil {
                err_c = qp.AddRule(ST_OPENEDINT_RQ_LAST_TERM, TT_RANGE_EXCL_END, ST_COMMON_QUERY_ELEMENT, nil)
            }
        }
    }

    if err_c != nil {
        return err_c
    }

    return nil
}

func GetDefaultOperator() int {
    return B_OR
}

func (qp *QueryParser) Parse(strQuery string) (query.Query, error) {

    // Reset FSM if previous parse operation didn't return it into a correct state
    qp.Reset()

    qp.lastToken = nil
    qp.context = NewQueryParserContext("")

    qp.contextStack = make([]*QueryParserContext, 0)
    qp.tokens = qp.lexer.Tokenize(strQuery)

    // Empty query
    if len(qp.tokens) == 0 {
        return query.NewInsignificant(), nil
    }

    for _, token := range qp.tokens {

        qp.currentToken = token
        err := qp.Process(token.typ)

        if err != nil {
            return nil, err
        }

        qp.lastToken = token
    }

    if len(qp.contextStack) != 0 {
        return nil, fmt.Errorf("Syntax Error: mismatched parentheses, every opening must have closing.")
    }

    return qp.context.Query(), nil

}

/*********************************************************************
 * Actions implementation
 *
 * Actions affect on recognized lexemes list
 *********************************************************************/

func (qp *QueryParser) AddTermEntry() error {
    entry := queryentry.NewTerm(qp.currentToken.strVal, qp.context.Field())
    qp.context.AddEntry(entry)
    return nil
}

func (qp *QueryParser) AddPhraseEntry() error {
    entry := queryentry.NewPhrase(qp.currentToken.strVal, qp.context.Field())
    qp.context.AddEntry(entry)
    return nil
}

func (qp *QueryParser) SetField() error {
    qp.context.SetNextEntryField(qp.currentToken.strVal)
    return nil
}

func (qp *QueryParser) SetSign() error {
    err := qp.context.SetNextEntrySign(qp.currentToken.typ)

    if err != nil {
        return err
    }

    return nil
}

func (qp *QueryParser) ProcessFuzzyProximityModifier() error {
    err := qp.context.ProcessFuzzyProximityModifier(0.0)

    if err != nil {
        return err
    }

    return nil
}

func (qp *QueryParser) ProcessModifierParameter() error {
    if qp.lastToken == nil {
        return fmt.Errorf("Lexeme modifier parameter must follow lexeme modifier. Char position 0.")
    }

    switch qp.lastToken.typ {
    case TT_FUZZY_PROX_MARK:
        qp.context.ProcessFuzzyProximityModifier(float32(qp.currentToken.text[0]))
        break

    case TT_BOOSTING_MARK:
        qp.context.Boost(float32(qp.currentToken.text[0]))
        break

    default:
        // It's not a user input exception
        panic("Lexeme modifier parameter must follow lexeme modifier. Char position 0.")
    }

    return nil
}

func (qp *QueryParser) SubqueryStart() error {

    qp.contextStack = append(qp.contextStack, qp.context)
    qp.context = NewQueryParserContext(qp.context.Field())
    return nil
}

func (qp *QueryParser) SubqueryEnd() error {
    if len(qp.contextStack) == 0 {
        return fmt.Errorf("Syntax Error: mismatched parentheses, every opening must have closing.")
    }

    query := qp.context.Query()
    qp.context = qp.contextStack[len(qp.contextStack)-1]

    qp.context.AddEntry(queryentry.NewSubQuery(query))

    return nil
}

func (qp *QueryParser) LogicalOperator() error {
    err := qp.context.AddLogicalOperator(qp.currentToken.typ)

    if err != nil {
        return err
    }

    return nil
}

func (qp *QueryParser) OpenedRQFirstTerm() error {
    qp.rqFirstTerm = qp.currentToken.text
    return nil
}

func (qp *QueryParser) OpenedRQLastTerm() error {
    var to, from *index.Term
    var tokens []*Token
    var err error = nil
    tokenizer := NewWhitespaceTokenizer()

    tokens = tokenizer.Tokenize(string(qp.rqFirstTerm))
    if len(tokens) > 1 {
        return fmt.Errorf("Range query boundary terms must be non-multiple word terms.")
    } else if len(tokens) == 1 {
        from = index.NewTerm(tokens[0].GetTermText(), qp.context.Field())
    } else {
        from = nil
    }

    tokens = tokenizer.Tokenize(qp.currentToken.strVal)
    if len(tokens) > 1 {
        return fmt.Errorf("Range query boundary terms must be non-multiple word terms.")
    } else if len(tokens) == 1 {
        to = index.NewTerm(tokens[0].GetTermText(), qp.context.Field())
    } else {
        to = nil
    }

    if to == nil && from == nil {
        return fmt.Errorf("At least one range query boundary term must be non-empty term.")
    }

    rangeQuery := query.NewRange(from, to, false)

    entry := queryentry.NewSubQuery(rangeQuery)
    qp.context.AddEntry(entry)
    return err
}

func (qp *QueryParser) ClosedRQFirstTerm() error {
    qp.rqFirstTerm = qp.currentToken.text
    return nil
}

func (qp *QueryParser) ClosedRQLastTerm() error {
    var to, from *index.Term
    var tokens []*Token
    var err error = nil
    tokenizer := NewWhitespaceTokenizer()

    tokens = tokenizer.Tokenize(string(qp.rqFirstTerm))
    if len(tokens) > 1 {
        return fmt.Errorf("Range query boundary terms must be non-multiple word terms.")
    } else if len(tokens) == 1 {
        from = index.NewTerm(tokens[0].GetTermText(), qp.context.Field())
    } else {
        from = nil
    }

    tokens = tokenizer.Tokenize(qp.currentToken.strVal)
    if len(tokens) > 1 {
        return fmt.Errorf("Range query boundary terms must be non-multiple word terms.")
    } else if len(tokens) == 1 {
        to = index.NewTerm(tokens[0].GetTermText(), qp.context.Field())
    } else {
        to = nil
    }

    if from == nil && to == nil {
        return fmt.Errorf("At least one range query boundary term must be non-empty term.")
    }

    rangeQuery := query.NewRange(from, to, true)
    entry := queryentry.NewSubQuery(rangeQuery)
    qp.context.AddEntry(entry)
    return err
}
