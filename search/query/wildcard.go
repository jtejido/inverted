package query

import (
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/util"
	"fmt"
	"math"
	"regexp"
	"strings"
)

const (
	MIN_PREFIX_LENGTH int = 3
)

type WildCard struct {
	*BaseQuery
	pattern         *index.Term
	matches         []*index.Term
	minPrefixLength int
}

func NewWildCard(pattern *index.Term) *WildCard {
	ans := &WildCard{pattern: pattern, minPrefixLength: MIN_PREFIX_LENGTH}
	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

func (wc *WildCard) MinPrefixLength() int {
	return wc.minPrefixLength
}

func (wc *WildCard) SetMinPrefixLength(minPrefixLength int) {
	wc.minPrefixLength = minPrefixLength
}

func (wc *WildCard) _getPrefix(word string) string {
	questionMarkPosition := strings.Index(word, "?")
	astrericPosition := strings.Index(word, "*")

	if questionMarkPosition != -1 {
		if astrericPosition != -1 {
			len := int(math.Min(float64(questionMarkPosition), float64(astrericPosition)))
			return word[0:len]
		}

		return word[0:questionMarkPosition]
	} else if astrericPosition != -1 {
		return word[0:astrericPosition]
	}

	return word
}

func (wc *WildCard) Rewrite(idx SearchIndexInterface) Query {
	wc.matches = make([]*index.Term, 0)
	var fields []string

	if wc.pattern.Field == "" {
		// Search through all fields
		fields = idx.FieldNames(true)
	} else {
		fields = []string{wc.pattern.Field}
	}

	prefix := wc._getPrefix(wc.pattern.Text)
	prefixLength := len(prefix)

	rpl := strings.NewReplacer("\\?", ".", "\\*", ".*")

	matchExpression := "/^" + rpl.Replace(wc.pattern.Text) + "$/"

	if prefixLength < wc.minPrefixLength {
		panic(fmt.Sprintf("At least %d non-wildcard characters are required at the beginning of pattern.", wc.minPrefixLength))
	}

	pattern := regexp.MustCompile(matchExpression)

	maxTerms := util.TERMS_PER_QUERY_LIMIT
	for _, field := range fields {
		idx.ResetTermsStream()

		if prefix != "" {
			idx.SkipTo(index.NewTerm(prefix, field))

			for idx.CurrentTerm() != nil && idx.CurrentTerm().Field == field && idx.CurrentTerm().Text[0:prefixLength] == prefix {
				if pattern.Match([]byte(idx.CurrentTerm().Text)) {
					wc.matches = append(wc.matches, idx.CurrentTerm())

					if maxTerms != 0 && len(wc.matches) > maxTerms {
						panic("Terms per query limit is reached.")
					}
				}

				idx.NextTerm()
			}
		} else {
			idx.SkipTo(index.NewTerm("", field))

			for idx.CurrentTerm() != nil && idx.CurrentTerm().Field == field {
				if pattern.Match([]byte(idx.CurrentTerm().Text)) {
					wc.matches = append(wc.matches, idx.CurrentTerm())

					if maxTerms != 0 && len(wc.matches) > maxTerms {
						panic("Terms per query limit is reached.")
					}
				}

				idx.NextTerm()
			}
		}

		idx.CloseTermsStream()
	}

	if len(wc.matches) == 0 {
		return NewEmptyResult()
	} else if len(wc.matches) == 1 {
		return NewTerm(wc.matches[0])
	} else {
		rewrittenQuery := NewMultiTerm(nil, nil)

		for _, matchedTerm := range wc.matches {
			rewrittenQuery.AddTerm(matchedTerm)
		}

		return rewrittenQuery
	}
}

func (wc *WildCard) CreateWeight(reader SearchIndexInterface) Weight {
	panic("This query is not intended to be executed.")
}

func (wc *WildCard) Execute(reader SearchIndexInterface) {
	panic("This query is not intended to be executed.")
}

func (wc *WildCard) MatchedDocs() *TopDocs {
	panic("This query is not intended to be executed.")
}

func (wc *WildCard) Score(docId int, reader SearchIndexInterface) float32 {
	panic("This query is not intended to be executed.")
}

func (wc *WildCard) QueryTerms() []*index.Term {
	if wc.matches == nil {
		panic("Search has to be performed first to get matched terms")
	}

	return wc.matches
}
