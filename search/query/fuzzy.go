package query

import (
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/util"
	"math"
)

const (
	DEFAULT_MIN_SIMILARITY     = 0.5
	MAX_CLAUSE_COUNT       int = 1024
)

type Fuzzy struct {
	*BaseQuery
	maxDistances      map[int32]int32
	term              *index.Term
	minimumSimilarity float32
	prefixLength      int32
	matches           []*index.Term
	termKeys          []string
	scores            []float32
}

func NewFuzzy(term *index.Term, minimumSimilarity float32, prefixLength int32) *Fuzzy {
	if prefixLength < 0 {
		prefixLength = int32(MIN_PREFIX_LENGTH)
	}
	ans := &Fuzzy{term: term, minimumSimilarity: minimumSimilarity, prefixLength: prefixLength, maxDistances: make(map[int32]int32)}
	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

func (f *Fuzzy) DefaultPrefixLength() int32 {
	return int32(MIN_PREFIX_LENGTH)
}

func (f *Fuzzy) _calculateMaxDistance(prefixLength, termLength, length int32) int32 {
	f.maxDistances[length] = ((1 - int32(f.minimumSimilarity)) * (int32(math.Min(float64(termLength), float64(length))) + prefixLength))
	return f.maxDistances[length]
}

func (f *Fuzzy) Rewrite(idx SearchIndexInterface) Query {
	f.matches = make([]*index.Term, 0)
	f.scores = make([]float32, 0)
	f.termKeys = make([]string, 0)
	var fields []string

	if f.term.Field == "" {
		// Search through all fields
		fields = idx.FieldNames(true)
	} else {
		fields = []string{f.term.Field}
	}

	prefix := util.GetPrefix(f.term.Text, int(f.prefixLength))
	prefixByteLength := len([]byte(prefix))
	prefixUtf8Length := int32(util.GetLength(prefix))

	termRest := f.term.Text[prefixByteLength:]
	// we calculate length of the rest in bytes since levenshtein() is not UTF-8 compatible
	termRestLength := int32(len(termRest))

	scaleFactor := 1 / (1 - f.minimumSimilarity)

	maxTerms := util.TERMS_PER_QUERY_LIMIT
	for _, field := range fields {
		idx.ResetTermsStream()
		var target string

		if prefix != "" {
			idx.SkipTo(index.NewTerm(prefix, field))

			for idx.CurrentTerm() != nil && idx.CurrentTerm().Field == field && idx.CurrentTerm().Text[0:prefixByteLength] == prefix {
				// Calculate similarity
				target = idx.CurrentTerm().Text[prefixByteLength:]
				t := int32(len(target))

				var maxDistance int32
				var similarity float32

				if i, ok := f.maxDistances[t]; ok {
					maxDistance = i
				} else {
					maxDistance = f._calculateMaxDistance(prefixUtf8Length, termRestLength, t)
				}

				if termRestLength == 0 {
					// we don't have anything to compare.  That means if we just add
					// the letters for current term we get the new word
					if prefixUtf8Length == 0 {
						similarity = 0
					} else {
						similarity = float32(1 - t/prefixUtf8Length)
					}

				} else if t == 0 {
					if prefixUtf8Length == 0 {
						similarity = 0
					} else {
						similarity = float32(1 - termRestLength/prefixUtf8Length)
					}

				} else if maxDistance < int32(math.Abs(float64(termRestLength-t))) {
					//just adding the characters of term to target or vice-versa results in too many edits
					//for example "pre" length is 3 and "prefixes" length is 8.  We can see that
					//given this optimal circumstance, the edit distance cannot be less than 5.
					//which is 8-3 or more precisesly abs(3-8).
					//if our maximum edit distance is 4, then we can discard this word
					//without looking at it.
					similarity = 0
				} else {
					similarity = 1 - float32(util.Levenshtein(termRest, target))/(float32(prefixUtf8Length)+float32(math.Min(float64(termRestLength), float64(t))))
				}

				if similarity > f.minimumSimilarity {
					f.matches = append(f.matches, idx.CurrentTerm())
					f.termKeys = append(f.termKeys, idx.CurrentTerm().Key())
					f.scores = append(f.scores, (similarity-f.minimumSimilarity)*scaleFactor)

					if maxTerms != 0 && len(f.matches) > maxTerms {
						panic("Terms per query limit is reached.")
					}
				}

				idx.NextTerm()
			}
		} else {
			idx.SkipTo(index.NewTerm("", field))

			for idx.CurrentTerm() != nil && idx.CurrentTerm().Field == field {
				// Calculate similarity
				target = idx.CurrentTerm().Text
				t := int32(len(target))

				var maxDistance int32
				var similarity float32

				if i, ok := f.maxDistances[t]; ok {
					maxDistance = i
				} else {
					maxDistance = f._calculateMaxDistance(0, termRestLength, t)
				}

				if maxDistance < int32(math.Abs(float64(termRestLength-t))) {
					//just adding the characters of term to target or vice-versa results in too many edits
					//for example "pre" length is 3 and "prefixes" length is 8.  We can see that
					//given this optimal circumstance, the edit distance cannot be less than 5.
					//which is 8-3 or more precisesly abs(3-8).
					//if our maximum edit distance is 4, then we can discard this word
					//without looking at it.
					similarity = 0
				} else {
					similarity = 1 - float32(util.Levenshtein(termRest, target))/float32(math.Min(float64(termRestLength), float64(t)))
				}

				if similarity > f.minimumSimilarity {
					f.matches = append(f.matches, idx.CurrentTerm())
					f.termKeys = append(f.termKeys, idx.CurrentTerm().Key())
					f.scores = append(f.scores, (similarity-f.minimumSimilarity)*scaleFactor)

					if maxTerms != 0 && len(f.matches) > maxTerms {
						panic("Terms per query limit is reached.")
					}
				}

				idx.NextTerm()
			}
		}

		idx.CloseTermsStream()
	}

	if len(f.matches) == 0 {
		return NewEmptyResult()
	} else if len(f.matches) == 1 {
		return NewTerm(f.matches[0])
	} else {
		rewrittenQuery := NewBoolean()

		// array_multisort($this->_scores, SORT_DESC, SORT_NUMERIC,
		// 	$this->_termKeys, SORT_ASC, SORT_STRING,
		// 	$this->_matches);

		termCount := 0
		for id, matchedTerm := range f.matches {
			subquery := NewTerm(matchedTerm)
			subquery.SetBoost(f.scores[id])

			rewrittenQuery.AddSubquery(subquery, false)

			termCount++
			if termCount >= MAX_CLAUSE_COUNT {
				break
			}
		}

		return rewrittenQuery
	}
}

func (f *Fuzzy) CreateWeight(reader SearchIndexInterface) Weight {
	panic("This query is not intended to be executed.")
}

func (f *Fuzzy) Execute(reader SearchIndexInterface) {
	panic("This query is not intended to be executed.")
}

func (f *Fuzzy) MatchedDocs() *TopDocs {
	panic("This query is not intended to be executed.")
}

func (f *Fuzzy) Score(docId int, reader SearchIndexInterface) float32 {
	panic("This query is not intended to be executed.")
}

func (f *Fuzzy) QueryTerms() []*index.Term {
	if f.matches == nil {
		panic("Search has to be performed first to get matched terms")
	}

	return f.matches
}
