package query

import (
	"bitbucket.org/jtejido/inverted/index"
)

type Insignificant struct {
	*BaseQuery
}

func NewInsignificant() *Insignificant {
	ans := &Insignificant{}
	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

func (i *Insignificant) AddSubquery(subquery Query, sign bool, isSigned bool) {
}

func (i *Insignificant) Subqueries() []Query {
	return nil
}

func (i *Insignificant) MatchedDocs() *TopDocs {
	return nil
}

func (i *Insignificant) Score(docId int, reader SearchIndexInterface) float32 {
	return 0
}

func (i *Insignificant) QueryTerms() []*index.Term {
	return nil
}

func (i *Insignificant) Execute(reader SearchIndexInterface) {
}

func (i *Insignificant) Rewrite(index SearchIndexInterface) Query {
	return i
}

func (i *Insignificant) CreateWeight(reader SearchIndexInterface) Weight {
	return newEmptyResultWeight()
}

type EmptyResultWeight struct {
	*BaseWeight
}

func newEmptyResultWeight() *EmptyResultWeight {
	ans := new(EmptyResultWeight)
	ans.BaseWeight = newBaseWeight(ans)

	return ans
}

func (erw *EmptyResultWeight) value() float32 {
	return 0
}

func (erw *EmptyResultWeight) sumOfSquaredWeights() float32 {
	return 1
}

func (erw *EmptyResultWeight) normalize(queryNorm float32) {
	return
}
