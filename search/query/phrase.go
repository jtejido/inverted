package query

import (
	"bitbucket.org/jtejido/inverted/index"
	"fmt"
	"math"
)

type Phrase struct {
	*BaseQuery
	terms          []*index.Term
	offsets        []int32
	slop           int32
	resVector      *TopDocs
	termsPositions []map[int32][]int32
}

func NewPhrase(terms []string, offsets []int32, field string) *Phrase {

	ans := new(Phrase)
	if terms != nil {
		ans.terms = make([]*index.Term, len(terms))
		for i, term := range terms {
			ans.terms[i] = index.NewTerm(term, field)
		}
	} else {
		ans.terms = make([]*index.Term, 0)
	}

	if terms != nil && offsets != nil {
		if len(terms) != len(offsets) {
			panic("terms and offsets arguments must have the same size.")
		}
		ans.offsets = offsets
	}

	if offsets == nil {
		ans.offsets = make([]int32, len(ans.terms))
		for i := 0; i < len(ans.terms); i++ {
			position := len(ans.offsets)
			ans.offsets[i] = int32(position)
		}
	}

	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

/**
 * Set slop
 */
func (p *Phrase) SetSlop(slop int32) {
	p.slop = slop
}

/**
 * Get slop
 */
func (p *Phrase) Slop() int32 {
	return p.slop
}

func (p *Phrase) AddTerm(term *index.Term, position int32) {
	if (len(p.terms) != 0) && (p.terms[len(p.terms)-1].Field != term.Field) {
		panic(fmt.Sprintf("All phrase terms must be in the same field: %s : %s", term.Field, term.Text))
	}

	p.terms = append(p.terms, term)
	p.offsets = append(p.offsets, position)
}

func (p *Phrase) Rewrite(idx SearchIndexInterface) Query {

	if len(p.terms) == 0 {
		return NewEmptyResult()
	} else if p.terms[0].Field != "" {
		return p
	} else {
		q := NewBoolean()
		q.SetBoost(p.Boost())

		for _, fieldName := range idx.FieldNames(true) {
			subquery := NewPhrase(nil, nil, "")
			subquery.SetSlop(p.Slop())

			for termId, term := range p.terms {
				qualifiedTerm := index.NewTerm(term.Text, fieldName)

				subquery.AddTerm(qualifiedTerm, p.offsets[termId])
			}

			q.AddSubquery(subquery, false)
		}

		return q
	}
}

func (p *Phrase) Terms() []*index.Term {
	return p.terms
}

func (p *Phrase) CreateWeight(reader SearchIndexInterface) Weight {
	// p.weight = newPhraseWeight(p, reader)
	// return p.weight
	return nil
}

func (p *Phrase) _exactPhraseFreq(docId int) float32 {
	var freq float32
	dId := int32(docId)
	// Term Id with lowest cardinality
	lowCardTermId := -1

	// Calculate $lowCardTermId
	for i := 0; i < len(p.terms); i++ {
		if lowCardTermId < 0 || len(p.termsPositions[i][dId]) < len(p.termsPositions[lowCardTermId][dId]) {
			lowCardTermId = i
		}
	}

	// Walk through positions of the term with lowest cardinality
	for _, lowCardPos := range p.termsPositions[lowCardTermId][dId] {
		// We expect phrase to be found
		freq++

		// Walk through other terms
		for i := 0; i < len(p.terms); i++ {
			if i != lowCardTermId {
				expectedPosition := lowCardPos + (p.offsets[i] - p.offsets[lowCardTermId])

				var inTp bool

				for _, termsPosition := range p.termsPositions[i][dId] {
					if termsPosition == expectedPosition {
						inTp = true
						break
					}
				}

				if !inTp {
					freq-- // Phrase wasn't found.
					break
				}
			}
		}
	}

	return freq
}

func (p *Phrase) _sloppyPhraseFreq(docId int, reader SearchIndexInterface) float32 {
	var freq float32
	did := int32(docId)
	phraseQueue := make([][]int32, 0)
	phraseQueue[0] = make([]int32, len(p.terms))
	lastTerm := -1

	// Walk through the terms to create phrases.
	for i := 0; i < len(p.terms); i++ {
		queueSize := len(phraseQueue)
		firstPass := true

		// Walk through the term positions.
		// Each term position produces a set of phrases.
		for _, termPosition := range p.termsPositions[i][did] {
			if firstPass {
				for count := 0; count < queueSize; count++ {
					phraseQueue[count][i] = termPosition
				}
			} else {
				for count := 0; count < queueSize; count++ {
					if lastTerm < 0 && int32(math.Abs(float64(termPosition-phraseQueue[count][lastTerm]-(p.offsets[i]-p.offsets[lastTerm])))) > p.slop {
						continue
					}

					newPhraseId := len(phraseQueue)
					phraseQueue = append(phraseQueue, phraseQueue[count])
					phraseQueue[newPhraseId][i] = termPosition
				}

			}

			firstPass = false
		}
		lastTerm = i
	}

	for _, phrasePos := range phraseQueue {
		var minDistance int32 = -1

		for shift := -p.slop; shift <= p.slop; shift++ {
			var distance int32
			start := phrasePos[0] - p.offsets[0] + int32(shift)

			for i := 0; i < len(p.terms); i++ {
				distance += int32(math.Abs(float64(phrasePos[i] - p.offsets[i] - start)))

				if distance > p.slop {
					break
				}
			}

			if minDistance < 0 || distance < minDistance {
				minDistance = distance
			}
		}

		if minDistance <= p.slop {
			freq += reader.Similarity().SloppyFreq(minDistance)
		}
	}

	return freq
}

func (p *Phrase) Execute(reader SearchIndexInterface) {

	resVectors := make([]*TopDocs, len(p.terms))
	resVectorsSizes := make([]int, len(p.terms))
	resVectorsIds := make([]int, len(p.terms)) // is used to prevent arrays comparison
	for termId, term := range p.terms {
		if resVectors[termId] == nil {
			resVectors[termId] = newTopDocs()
		}
		termDocs := reader.TermDocs(term)

		for _, v := range termDocs {
			resVectors[termId].AddDoc(&ScoreDoc{Doc: v})
		}

		resVectorsSizes[termId] = len(termDocs)
		resVectorsIds[termId] = termId

		p.termsPositions[termId] = reader.TermPositions(term)
	}

	// sort resvectors in order of subquery cardinality increasing
	// array_multisort($resVectorsSizes, SORT_ASC, SORT_NUMERIC,
	//                 $resVectorsIds,   SORT_ASC, SORT_NUMERIC,
	//                 $resVectors);

	for _, nextResVector := range resVectors {
		if p.resVector == nil {
			p.resVector = nextResVector
		} else {

			/**
			 * This code is used as workaround for array_intersect_key() slowness problem.
			 */
			updatedVector := newTopDocs()
			for _, value := range p.resVector.ScoreDocs {
				for _, d := range nextResVector.ScoreDocs {
					if d.Doc == value.Doc {
						updatedVector.AddDoc(value)
					}
				}

			}
			p.resVector = updatedVector
		}

		if len(p.resVector.ScoreDocs) == 0 {
			// Empty result set, we don't need to check other terms
			break
		}
	}

	// Initialize weight if it's not done yet
	p.initWeight(reader)
}

func (p *Phrase) MatchedDocs() *TopDocs {
	return p.resVector
}

func (p *Phrase) Score(docId int, reader SearchIndexInterface) float32 {
	var freq float32
	for _, doc := range p.resVector.ScoreDocs {
		if docId == int(doc.Doc) {
			if p.slop == 0 {
				freq = p._exactPhraseFreq(docId)
			} else {
				freq = p._sloppyPhraseFreq(docId, reader)
			}

			if freq != 0 {
				tf := reader.Similarity().Tf(freq)
				weight := p.weight.Value()
				// norm = $reader->norm($docId, reset($this->_terms)->field);

				return tf * weight * p.Boost()
			}

			break
		}
	}

	return 0

}

func (p *Phrase) QueryTerms() []*index.Term {
	return p.terms
}
