package query

import (
    "bitbucket.org/jtejido/inverted/index"
    // "bitbucket.org/jtejido/inverted/util"
)

type Range struct {
    *BaseQuery
    field     string
    lowerTerm *index.Term
    upperTerm *index.Term
    inclusive bool
    matches   []*index.Term
}

func NewRange(lowerTerm, upperTerm *index.Term, inclusive bool) *Range {

    var lt string

    if lowerTerm == nil && upperTerm == nil {
        panic("At least one term must be non-null")
    }
    if lowerTerm != nil && upperTerm != nil && lowerTerm.Field != upperTerm.Field {
        panic("Both terms must be for the same field")
    }

    if lowerTerm != nil {
        lt = lowerTerm.Field
    } else {
        lt = upperTerm.Field
    }

    ans := &Range{
        lowerTerm: lowerTerm,
        upperTerm: upperTerm,
        field:     lt,
        inclusive: inclusive,
    }

    ans.BaseQuery = NewBaseQuery(1)

    return ans
}

func (r *Range) AddSubquery(subquery Query, sign bool, isSigned bool) {

}

func (r *Range) GetSubqueries() []Query {
    return nil
}

func (r *Range) MatchedDocs() *TopDocs {
    return nil
}

func (r *Range) Score(docId int, reader SearchIndexInterface) float32 {
    return 0
}

func (r *Range) QueryTerms() []*index.Term {
    return nil
}

func (r *Range) CreateWeight(reader SearchIndexInterface) Weight {
    return nil
}

func (r *Range) Rewrite(index SearchIndexInterface) Query {
    // r.matches = make([]string, 0)

    // if (r.field == "") {
    //     // Search through all fields
    //     fields := index.FieldNames(true);
    // }

    // maxTerms := util.TERMS_PER_QUERY_LIMIT
    // for _, field := range fields {
    //     index.ResetTermsStream();

    //     if ($this->_lowerTerm !== null) {
    //         $lowerTerm = new Index\Term($this->_lowerTerm->text, $field);

    //         $index->skipTo($lowerTerm);

    //         if (!$this->_inclusive  &&
    //             $index->currentTerm() == $lowerTerm) {
    //             // Skip lower term
    //             $index->nextTerm();
    //         }
    //     } else {
    //         $index->skipTo(new Index\Term('', $field));
    //     }

    //     if ($this->_upperTerm !== null) {
    //         // Walk up to the upper term
    //         $upperTerm = new Index\Term($this->_upperTerm->text, $field);

    //         while ($index->currentTerm() !== null          &&
    //                $index->currentTerm()->field == $field  &&
    //                $index->currentTerm()->text  <  $upperTerm->text) {
    //             $this->_matches[] = $index->currentTerm();

    //             if ($maxTerms != 0  &&  count($this->_matches) > $maxTerms) {
    //                 throw new OutOfBoundsException('Terms per query limit is reached.');
    //             }

    //             $index->nextTerm();
    //         }

    //         if ($this->_inclusive  &&  $index->currentTerm() == $upperTerm) {
    //             // Include upper term into result
    //             $this->_matches[] = $upperTerm;
    //         }
    //     } else {
    //         // Walk up to the end of field data
    //         while ($index->currentTerm() !== null  &&  $index->currentTerm()->field == $field) {
    //             $this->_matches[] = $index->currentTerm();

    //             if ($maxTerms != 0  &&  count($this->_matches) > $maxTerms) {
    //                 throw new OutOfBoundsException('Terms per query limit is reached.');
    //             }

    //             $index->nextTerm();
    //         }
    //     }

    //     $index->closeTermsStream();
    // }

    // if (count($this->_matches) == 0) {
    //     return new EmptyResult();
    // } elseif (count($this->_matches) == 1) {
    //     return new Term(reset($this->_matches));
    // } else {
    //     $rewrittenQuery = new MultiTerm();

    //     foreach ($this->_matches as $matchedTerm) {
    //         $rewrittenQuery->addTerm($matchedTerm);
    //     }

    //     return $rewrittenQuery;
    // }

    return nil
}

func (r *Range) Execute(index SearchIndexInterface) {

}
