package query

import (
    "bitbucket.org/jtejido/inverted/index"
)

type Boolean struct {
    *BaseQuery
    signs      []bool
    subqueries []Query
    resVector  *TopDocs
    coord      float32
}

func NewBoolean() *Boolean {
    ans := &Boolean{signs: nil, subqueries: []Query{}}
    ans.BaseQuery = NewBaseQuery(1)
    return ans
}

func (b *Boolean) AddSubquery(subquery Query, sign bool) {
    if sign != true || b.signs != nil { // Skip, if all subqueries are required
        if b.signs == nil { // Check, If all previous subqueries are required
            b.signs = make([]bool, len(b.subqueries))
            for i := 0; i <= len(b.subqueries); i++ {
                b.signs[i] = true
            }
        }

        b.signs = append(b.signs, sign)
    }

    b.subqueries = append(b.subqueries, subquery)
}

func (b *Boolean) Subqueries() []Query {
    return b.subqueries
}

func (b *Boolean) Rewrite(index SearchIndexInterface) Query {
    var signs bool
    query := NewBoolean()
    query.boost = b.Boost()

    for i, subquery := range b.subqueries {

        if b.signs == nil {
            signs = true
        } else {
            signs = b.signs[i]
        }

        query.AddSubquery(subquery.Rewrite(index), signs)
    }

    return query
}

func (b *Boolean) CreateWeight(reader SearchIndexInterface) Weight {
    b.weight = newBooleanWeight(b, reader)
    return b.weight
}

func (b *Boolean) Execute(reader SearchIndexInterface) {
    b.initWeight(reader)

    for _, subQuery := range b.subqueries {
        subQuery.Execute(reader)
    }

    if b.signs == nil {
        b._calculateConjunctionResult()
    } else {
        b._calculateNonConjunctionResult()
    }
}

func (b *Boolean) _calculateConjunctionResult() {

    resVectors := make([]*TopDocs, len(b.subqueries))
    resVectorsSizes := make([]int, len(b.subqueries))
    resVectorsIds := make([]int, len(b.subqueries))

    for i, subQuery := range b.subqueries {
        resVectors[i] = subQuery.MatchedDocs()
        resVectorsSizes[i] = resVectors[i].TotalHits
        resVectorsIds[i] = i
    }

    for _, nextResVector := range resVectors {
        if b.resVector == nil {
            b.resVector = nextResVector
        } else {
            updatedVector := newTopDocs()
            for _, value := range b.resVector.ScoreDocs {
                for _, d := range nextResVector.ScoreDocs {
                    if d.Doc == value.Doc {
                        updatedVector.AddDoc(value)
                    }
                }

            }
            b.resVector = updatedVector
        }

        if len(b.resVector.ScoreDocs) == 0 {
            break
        }
    }
}

func (b *Boolean) _calculateNonConjunctionResult() {

    requiredVectors := make([]*TopDocs, len(b.subqueries))
    requiredVectorsSizes := make([]int, len(b.subqueries))
    requiredVectorsIds := make([]int, len(b.subqueries))

    optional := newTopDocs()

    for i, subQuery := range b.subqueries {
        if b.signs[i] {
            requiredVectors[i] = subQuery.MatchedDocs()
            requiredVectorsSizes[i] = requiredVectors[i].TotalHits
            requiredVectorsIds[i] = i
        } else if !b.signs[i] {
            panic("nothing")
        } else {
            for _, d := range subQuery.MatchedDocs().ScoreDocs {
                optional.AddDoc(d)
            }
        }

    }

    var required *TopDocs
    for _, nextResVector := range requiredVectors {
        if required == nil {
            required = nextResVector
        } else {
            updatedVector := newTopDocs()
            for _, value := range required.ScoreDocs {
                for _, d := range nextResVector.ScoreDocs {
                    if d.Doc == value.Doc {
                        updatedVector.AddDoc(value)
                    }
                }
            }
            required = updatedVector
        }

        if len(b.resVector.ScoreDocs) == 0 {
            break
        }
    }

    if required != nil {
        b.resVector = required
    } else {
        b.resVector = optional
    }
}

func (b *Boolean) MatchedDocs() *TopDocs {
    return b.resVector
}

func (b *Boolean) Score(docId int, reader SearchIndexInterface) float32 {
    for _, val := range b.resVector.ScoreDocs {
        if val.Doc == int32(docId) {
            if b.signs == nil {
                return b._conjunctionScore(docId, reader)
            } else {
                return b._nonConjunctionScore(docId, reader)
            }
        }
    }

    return 0
}

func (b *Boolean) _conjunctionScore(docId int, reader SearchIndexInterface) float32 {

    coord := reader.Similarity().Coord(int32(len(b.subqueries)), int32(len(b.subqueries)))

    var score float32

    for _, subquery := range b.subqueries {
        subscore := subquery.Score(docId, reader)

        if subscore == 0 {
            return 0
        }

        score += subquery.Score(docId, reader) * coord
    }

    return score * coord * b.Boost()
}

func (b *Boolean) _nonConjunctionScore(docId int, reader SearchIndexInterface) float32 {

    var maxCoord int
    var score float32
    for _, sign := range b.signs {
        if sign {
            maxCoord++
        }
    }

    coords := make([]float32, maxCoord)

    for count := 0; count <= maxCoord; count++ {
        coords[count] = reader.Similarity().Coord(int32(count), int32(maxCoord))
    }

    matchedSubqueries := 0
    for subqueryId, subquery := range b.subqueries {
        subscore := subquery.Score(docId, reader)

        // Prohibited
        if !b.signs[subqueryId] && subscore != 0 {
            return 0
        }

        // is required, but doen't match
        if b.signs[subqueryId] && subscore == 0 {
            return 0
        }

        if subscore != 0 {
            matchedSubqueries++
            score += subscore
        }
    }

    return score * coords[matchedSubqueries] * b.Boost()
}

func (b *Boolean) QueryTerms() []*index.Term {
    terms := []*index.Term{}

    for i, subquery := range b.subqueries {
        if b.signs == nil || b.signs[i] {
            terms = append(terms, subquery.QueryTerms()...)
        }
    }

    return terms
}

// func (b *Boolean) String() string {

//     query := ""

//     for i, subquery := range b.subqueries {
//         if i != 0 {
//             query += " "
//         }

//         if b.signs == nil || b.signs[i] {
//             query += "+"
//         } else if !b.signs[i] {
//             query += "-"
//         }

//         query += "(" + subquery.String() + ")"
//     }

//     if b.Boost() != 1 {
//         query = "(" + query + ")^" + fmt.Sprintf("%.4f", b.Boost())
//     }

//     return query
// }

type BooleanWeight struct {
    *BaseWeight
    reader  SearchIndexInterface
    query   *Boolean
    weights []Weight
}

func newBooleanWeight(query *Boolean, reader SearchIndexInterface) *BooleanWeight {
    ans := &BooleanWeight{reader: reader, query: query, weights: make([]Weight, len(query.subqueries))}
    ans.BaseWeight = newBaseWeight(ans)

    signs := query.signs

    for num, subquery := range query.subqueries {
        if signs == nil || signs[num] {
            ans.weights[num] = subquery.CreateWeight(reader)
        }
    }

    return ans
}

func (bw *BooleanWeight) value() float32 {
    return bw.query.Boost()
}

func (bw *BooleanWeight) sumOfSquaredWeights() float32 {
    var sum float32
    for _, weight := range bw.weights {
        // sum sub weights
        sum += weight.SumOfSquaredWeights()
    }

    // boost each sub-weight
    sum *= bw.query.Boost() * bw.query.Boost()

    // check for empty query (like '-something -another')
    if sum == 0 {
        sum = 1.0
    }
    return sum
}

func (bw *BooleanWeight) normalize(queryNorm float32) {
    queryNorm *= bw.query.Boost()

    for _, weight := range bw.weights {
        weight.Normalize(queryNorm)
    }
}
