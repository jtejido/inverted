package preprocessing

import (
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/search/query"
)

type BasePreprocessing struct {
	*query.BaseQuery
	matches []*index.Term
}

func newBasePreprocessing() *BasePreprocessing {
	ans := &BasePreprocessing{}
	ans.BaseQuery = query.NewBaseQuery(1)
	return ans
}

/**
 * Constructs an appropriate Weight implementation for this query.
 *
 * @param \ZendSearch\Lucene\SearchIndexInterface $reader
 * @throws \ZendSearch\Lucene\Exception\UnsupportedMethodCallException
 */
func (bp *BasePreprocessing) CreateWeight(reader query.SearchIndexInterface) query.Weight {
	panic("This query is not intended to be executed.")
}

/**
 * Execute query in context of index reader
 * It also initializes necessary internal structures
 *
 * @param \ZendSearch\Lucene\SearchIndexInterface $reader
 * @param \ZendSearch\Lucene\Index\DocsFilter|null $docsFilter
 * @throws \ZendSearch\Lucene\Exception\UnsupportedMethodCallException
 */
func (bp *BasePreprocessing) Execute(reader query.SearchIndexInterface) {
	panic("This query is not intended to be executed.")
}

/**
 * Get document ids likely matching the query
 *
 * It's an array with document ids as keys (performance considerations)
 *
 * @throws \ZendSearch\Lucene\Exception\UnsupportedMethodCallException
 * @return array
 */
func (bp *BasePreprocessing) MatchedDocs() *query.TopDocs {
	panic("This query is not intended to be executed.")
}

/**
 * Score specified document
 *
 * @param integer $docId
 * @param \ZendSearch\Lucene\SearchIndexInterface $reader
 * @throws \ZendSearch\Lucene\Exception\UnsupportedMethodCallException
 * @return float
 */
func (bp *BasePreprocessing) Score(docId int, reader query.SearchIndexInterface) float32 {
	panic("This query is not intended to be executed.")
}

/**
 * Return query terms
 *
 * @throws \ZendSearch\Lucene\Exception\UnsupportedMethodCallException
 * @return array
 */
func (bp *BasePreprocessing) QueryTerms() []*index.Term {
	panic("This query is not intended to be executed.")
}
