package preprocessing

import (
	"bitbucket.org/jtejido/inverted/analysis/analyzer/tokenizer"
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/search/query"
)

type Phrase struct {
	*BasePreprocessing
	phrase, field string
	slop          int32
}

func NewPhrase(phrase string, field string) *Phrase {
	ans := &Phrase{
		phrase: phrase,
		field:  field,
		slop:   0,
	}

	ans.BasePreprocessing = newBasePreprocessing()
	return ans
}

/**
 * Set slop
 */
func (p *Phrase) SetSlop(slop int32) {
	p.slop = slop
}

/**
 * Get slop
 */
func (p *Phrase) Slop() int32 {
	return p.slop
}

func (p *Phrase) Rewrite(idx query.SearchIndexInterface) query.Query {
	var q query.Query
	var term *index.Term
	tok := tokenizer.NewWhitespaceTokenizer()
	// Split query into subqueries if field name is not specified
	if p.field == "" {
		q = query.NewBoolean()
		q.SetBoost(p.Boost())

		searchFields := idx.FieldNames(true)

		for _, fieldName := range searchFields {
			subquery := NewPhrase(p.phrase, fieldName)
			subquery.SetSlop(p.slop)

			q.(*query.Boolean).AddSubquery(subquery.Rewrite(idx), false)
		}

		p.matches = q.QueryTerms()
		return q
	}

	// Recognize exact term matching (it corresponds to Keyword fields stored in the index)
	// encoding is not used since we expect binary matching
	term = index.NewTerm(p.phrase, p.field)
	if idx.HasTerm(term) {
		q = query.NewTerm(term)
		q.SetBoost(p.Boost())

		p.matches = q.QueryTerms()
		return q
	}

	// tokenize phrase using current analyzer and process it as a phrase query
	tokens := tok.Tokenize(p.phrase)

	if len(tokens) == 0 {
		p.matches = make([]*index.Term, 0)
		return query.NewInsignificant()
	}

	if len(tokens) == 1 {
		term = index.NewTerm(tokens[0].GetTermText(), p.field)
		q = query.NewTerm(term)
		q.SetBoost(p.Boost())

		p.matches = q.QueryTerms()
		return q
	}

	//It's non-trivial phrase query
	position := -1
	q = query.NewPhrase(nil, nil, "")
	for _, token := range tokens {
		position += token.GetPositionIncrement()
		term := index.NewTerm(token.GetTermText(), p.field)
		q.(*query.Phrase).AddTerm(term, int32(position))
		q.(*query.Phrase).SetSlop(p.slop)
	}

	p.matches = q.QueryTerms()
	return q

}
