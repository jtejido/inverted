package preprocessing

import (
	"bitbucket.org/jtejido/inverted/analysis/analyzer/tokenizer"
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/search/query"
	"regexp"
)

type Term struct {
	*BasePreprocessing
	word, field string
}

func NewTerm(word string, field string) *Term {
	ans := &Term{
		word:  word,
		field: field,
	}

	ans.BasePreprocessing = newBasePreprocessing()
	return ans
}

func (t *Term) Rewrite(idx query.SearchIndexInterface) query.Query {
	var q query.Query
	var term *index.Term
	tok := tokenizer.NewWhitespaceTokenizer()

	if t.field == "" {
		q = query.NewMultiTerm(nil, nil)
		q.SetBoost(t.Boost())

		var hasInsignificantSubqueries bool

		searchFields := idx.FieldNames(true)

		for _, fieldName := range searchFields {
			subquery := NewTerm(t.word, fieldName)
			rewrittenSubquery := subquery.Rewrite(idx)
			for _, tm := range rewrittenSubquery.QueryTerms() {
				q.(*query.MultiTerm).AddTerm(tm)
			}

			if _, ok := rewrittenSubquery.(*query.Insignificant); ok {
				hasInsignificantSubqueries = true
			}
		}

		if len(q.(*query.MultiTerm).Terms()) == 0 {
			t.matches = make([]*index.Term, 0)
			if hasInsignificantSubqueries {
				return query.NewInsignificant()
			} else {
				return query.NewEmptyResult()
			}
		}

		t.matches = q.QueryTerms()
		return q
	}

	// -------------------------------------
	// Recognize exact term matching (it corresponds to Keyword fields stored in the index)
	// encoding is not used since we expect binary matching
	term = index.NewTerm(t.word, t.field)
	if idx.HasTerm(term) {
		q = query.NewTerm(term)
		q.SetBoost(t.Boost())

		t.matches = q.QueryTerms()
		return q
	}

	word := t.word

	wildcardsPattern := regexp.MustCompile(`[*?]+`)
	subPatterns := wildcardsPattern.Split(word, -1)
	patternIdx := wildcardsPattern.FindIndex([]byte(word))

	if len(subPatterns) > 1 {
		// Wildcard query is recognized

		pattern := ""

		for id, subPattern := range subPatterns {
			// Append corresponding wildcard character to the pattern before each sub-pattern (except first)
			if id != 0 {
				pattern += string(word[patternIdx[id]-1])
			}

			// Check if each subputtern is a single word in terms of current analyzer

			tokens := tok.Tokenize(subPattern)
			if len(tokens) > 1 {
				panic("Wildcard search is supported only for non-multiple word terms")
			}
			for _, token := range tokens {
				pattern += token.GetTermText()
			}
		}

		term = index.NewTerm(pattern, t.field)
		q = query.NewWildCard(term)
		q.SetBoost(t.Boost())

		// Get rewritten query. Important! It also fills terms matching container.
		rewrittenQuery := q.Rewrite(idx)
		t.matches = q.QueryTerms()

		return rewrittenQuery
	}

	// Recognize one-term multi-term and "insignificant" queries
	tokens := tok.Tokenize(t.word)

	if len(tokens) == 0 {
		t.matches = make([]*index.Term, 0)
		return query.NewInsignificant()
	}

	if len(tokens) == 1 {
		term = index.NewTerm(tokens[0].GetTermText(), t.field)
		q = query.NewTerm(term)
		q.SetBoost(t.Boost())

		t.matches = q.QueryTerms()
		return q
	}

	//It's not insignificant or one term query
	q = query.NewMultiTerm(nil, nil)

	/**
	 * @todo Process $token->getPositionIncrement() to support stemming, synonyms and other
	 * analizer design features
	 */
	for _, token := range tokens {
		term = index.NewTerm(token.GetTermText(), t.field)
		q.(*query.MultiTerm).AddTerm(term) // all subterms are required
	}

	q.SetBoost(t.Boost())

	t.matches = q.QueryTerms()
	return q
}
