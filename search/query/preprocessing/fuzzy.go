package preprocessing

import (
	"bitbucket.org/jtejido/inverted/analysis/analyzer/tokenizer"
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/search/query"
)

type Fuzzy struct {
	*BasePreprocessing
	word, field       string
	minimumSimilarity float32
}

func NewFuzzy(word, field string, minimumSimilarity float32) *Fuzzy {
	ans := &Fuzzy{
		word:              word,
		field:             field,
		minimumSimilarity: minimumSimilarity,
	}

	ans.BasePreprocessing = newBasePreprocessing()
	return ans
}

func (f *Fuzzy) Rewrite(idx query.SearchIndexInterface) query.Query {
	var q query.Query
	var term *index.Term
	tok := tokenizer.NewWhitespaceTokenizer()
	if f.field == "" {
		q = query.NewBoolean()

		var hasInsignificantSubqueries bool

		searchFields := idx.FieldNames(true)

		for _, fieldName := range searchFields {
			subquery := NewFuzzy(f.word, fieldName, f.minimumSimilarity)

			rewrittenSubquery := subquery.Rewrite(idx)

			_, ok_i := rewrittenSubquery.(*query.Insignificant)
			_, ok_e := rewrittenSubquery.(*query.EmptyResult)

			if !(ok_i || ok_e) {
				q.(*query.Boolean).AddSubquery(rewrittenSubquery, false)

			}

			if ok_i {
				hasInsignificantSubqueries = true
			}
		}

		subqueries := q.(*query.Boolean).Subqueries()

		if len(subqueries) == 0 {
			f.matches = make([]*index.Term, 0)
			if hasInsignificantSubqueries {
				return query.NewInsignificant()
			} else {
				return query.NewEmptyResult()
			}
		}

		if len(subqueries) == 1 {
			q = subqueries[0]
		}

		q.SetBoost(f.Boost())

		f.matches = q.QueryTerms()
		return q
	}

	// -------------------------------------
	// Recognize exact term matching (it corresponds to Keyword fields stored in the index)
	// encoding is not used since we expect binary matching
	term = index.NewTerm(f.word, f.field)
	if idx.HasTerm(term) {
		q = query.NewFuzzy(term, f.minimumSimilarity, int32(query.MIN_PREFIX_LENGTH))
		q.SetBoost(f.Boost())

		// Get rewritten query. Important! It also fills terms matching container.
		rewrittenQuery := q.Rewrite(idx)
		f.matches = q.QueryTerms()

		return rewrittenQuery
	}

	// -------------------------------------
	// Recognize wildcard queries

	// -------------------------------------
	// Recognize one-term multi-term and "insignificant" queries
	tokens := tok.Tokenize(f.word)

	if len(tokens) == 0 {
		f.matches = make([]*index.Term, 0)
		return query.NewInsignificant()
	}

	if len(tokens) == 1 {
		term = index.NewTerm(tokens[0].GetTermText(), f.field)
		q = query.NewFuzzy(term, f.minimumSimilarity, int32(query.MIN_PREFIX_LENGTH))
		q.SetBoost(f.Boost())

		// Get rewritten query. Important! It also fills terms matching container.
		rewrittenQuery := q.Rewrite(idx)
		f.matches = q.QueryTerms()

		return rewrittenQuery
	}

	// Word is tokenized into several tokens
	panic("Fuzzy search is supported only for non-multiple word terms")
}
