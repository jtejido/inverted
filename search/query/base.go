package query

import (
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/search/similarity"
	"sort"
)

type SearchIndexInterface interface {
	index.TermsStream
	Count() int32
	Similarity() similarity.Similarity
	DocFreq(*index.Term) float32
	FieldNames(indexed bool) []string
	HasTerm(term *index.Term) bool
	TermPositions(term *index.Term) map[int32][]int32
	TermFreqs(term *index.Term) map[int32]int32
	TermDocs(term *index.Term) []int32
}

type TopDocs struct {
	TotalHits int
	ScoreDocs []*ScoreDoc
	maxScore  float64
}

func newTopDocs() *TopDocs {
	return &TopDocs{ScoreDocs: make([]*ScoreDoc, 0)}
}

func (td *TopDocs) AddDoc(doc *ScoreDoc) {
	td.ScoreDocs = append(td.ScoreDocs, doc)
}

func (td *TopDocs) SortById() {
	by(id).Sort(td.ScoreDocs)
}

func (td *TopDocs) SortByScore() {
	by(score).Sort(td.ScoreDocs)
}

/** Holds one hit in {@link TopDocs}. */
type ScoreDoc struct {
	/** The score of this document for the query. */
	Score float32
	/** A hit document's number.
	 * @see IndexSearcher#doc(int) */
	Doc int32
	/** Only set by {@link TopDocs#merge} */
	shardIndex int
}

type by func(n1, n2 *ScoreDoc) bool

func id(n1, n2 *ScoreDoc) bool {
	return n1.Doc < n2.Doc
}

func score(n1, n2 *ScoreDoc) bool {
	return n1.Score < n2.Score
}

func (b by) Sort(docs []*ScoreDoc) {
	ns := &scoreDocSorter{
		docs: docs,
		by:   b,
	}
	sort.Sort(ns)
}

type scoreDocSorter struct {
	docs []*ScoreDoc
	by   by
}

func (s *scoreDocSorter) Len() int {
	return len(s.docs)
}

func (s *scoreDocSorter) Swap(i, j int) {
	s.docs[i], s.docs[j] = s.docs[j], s.docs[i]
}

func (s *scoreDocSorter) Less(i, j int) bool {
	return s.by(s.docs[i], s.docs[j])
}

func newScoreDoc(doc int32, score float32) *ScoreDoc {
	return newShardedScoreDoc(doc, score, -1)
}

func newShardedScoreDoc(doc int32, score float32, shardIndex int) *ScoreDoc {
	return &ScoreDoc{score, doc, shardIndex}
}

type Query interface {
	Rewrite(index SearchIndexInterface) Query
	Score(docId int, reader SearchIndexInterface) float32
	MatchedDocs() *TopDocs
	Execute(reader SearchIndexInterface)
	CreateWeight(reader SearchIndexInterface) Weight
	// Optimize(index SearchIndexInterface) Query
	QueryTerms() []*index.Term
	Boost() float32
	Weight() Weight
	SetBoost(float32)
	SetWeight(Weight)
	Reset()
}

type BaseQuery struct {
	boost  float32
	weight Weight
}

func NewBaseQuery(boost float32) *BaseQuery {
	return &BaseQuery{boost, nil}
}

func (bq *BaseQuery) initWeight(reader SearchIndexInterface) {
	bq.CreateWeight(reader)
	sum := bq.weight.SumOfSquaredWeights()
	queryNorm := reader.Similarity().QueryNorm(sum)
	bq.weight.Normalize(queryNorm)
}

func (bq *BaseQuery) Boost() float32 {
	return bq.boost
}

func (bq *BaseQuery) SetBoost(b float32) {
	bq.boost = b
}

func (bq *BaseQuery) Weight() Weight {
	return bq.weight
}

func (bq *BaseQuery) SetWeight(w Weight) {
	bq.weight = w
}

func (bq *BaseQuery) CreateWeight(reader SearchIndexInterface) {
	bq.CreateWeight(reader)
}

// func (bq *BaseQuery) Optimize(index SearchIndexInterface) Query {
// 	bq.spi.optimize(index)
// }

func (bq *BaseQuery) Reset() {
	bq.weight = nil
}

type Weight interface {
	Value() float32
	SumOfSquaredWeights() float32
	Normalize(float32)
}

type iWeight interface {
	value() float32
	sumOfSquaredWeights() float32
	normalize(float32)
}

type BaseWeight struct {
	spi            iWeight
	queryNorm, val float32
}

func newBaseWeight(spi iWeight) *BaseWeight {
	return &BaseWeight{spi: spi}
}

func (bw *BaseWeight) Value() float32 {
	return bw.spi.value()
}

func (bw *BaseWeight) SumOfSquaredWeights() float32 {
	return bw.spi.sumOfSquaredWeights()
}

func (bw *BaseWeight) Normalize(norm float32) {
	bw.spi.normalize(norm)
}
