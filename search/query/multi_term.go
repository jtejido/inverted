package query

import (
	"bitbucket.org/jtejido/inverted/index"
)

type MultiTerm struct {
	*BaseQuery
	terms      []*index.Term
	signs      []bool
	termsFreqs []map[int32]int32
	resVector  *TopDocs
	coord      float32
	weights    []Weight
}

func NewMultiTerm(terms []*index.Term, signs []bool) *MultiTerm {

	var s []bool

	if signs != nil {
		for _, sign := range signs {
			if !sign {
				s = signs
				break
			}

		}
	}

	ans := &MultiTerm{terms: terms, signs: s}
	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

func (mt *MultiTerm) Terms() []*index.Term {
	return mt.terms
}

func (mt *MultiTerm) AddTermSigned(term *index.Term, sign bool) {
	if !sign || mt.signs != nil { // Skip, if all terms are required
		if mt.signs == nil { // Check, If all previous terms are required
			mt.signs = make([]bool, len(mt.terms))
			for i, _ := range mt.terms {
				mt.signs[i] = true
			}
		}
		mt.signs = append(mt.signs, sign)
	}

	if mt.terms == nil {
		mt.terms = make([]*index.Term, 0)
	}

	mt.terms = append(mt.terms, term)
}

func (mt *MultiTerm) AddTerm(term *index.Term) {
	if mt.terms == nil {
		mt.terms = make([]*index.Term, 0)
	}

	mt.terms = append(mt.terms, term)
}

func (mt *MultiTerm) Rewrite(index SearchIndexInterface) Query {
	if len(mt.terms) == 0 {
		return NewEmptyResult()
	}

	// Check, that all fields are qualified
	allQualified := true
	for _, term := range mt.terms {
		if term.Field == "" {
			allQualified = false
			break
		}
	}

	if allQualified {
		return mt
	} else {
		/** transform multiterm query to boolean and apply rewrite() method to subqueries. */
		query := NewBoolean()
		query.SetBoost(mt.Boost())

		for termId, term := range mt.terms {
			subquery := NewTerm(term)
			var s bool
			if mt.signs == nil {
				s = true
			} else {
				s = mt.signs[termId]
			}

			query.AddSubquery(subquery.Rewrite(index), s)
		}

		return query
	}
}

func (mt *MultiTerm) CreateWeight(reader SearchIndexInterface) Weight {
	mt.weight = newMultiTermWeight(mt, reader)
	return mt.weight
}

func (mt *MultiTerm) _calculateConjunctionResult(reader SearchIndexInterface) {

	mt.resVector = newTopDocs()

	// Order terms by selectivity
	docFreqs := make([]float32, len(mt.terms))
	ids := make([]int, len(mt.terms))
	for id, term := range mt.terms {
		docFreqs[id] = reader.DocFreq(term)
		ids[id] = id // Used to keep original order for terms with the same selectivity and omit terms comparison
	}

	var termDocs []int32

	for _, term := range mt.terms {
		termDocs = reader.TermDocs(term)
	}
	// Treat last retrieved docs vector as a result set
	// (filter collects data for other terms)

	for _, v := range termDocs {
		mt.resVector.AddDoc(&ScoreDoc{Doc: v})
	}

	mt.termsFreqs = make([]map[int32]int32, len(mt.terms))

	for termId, term := range mt.terms {
		mt.termsFreqs[termId] = reader.TermFreqs(term)
	}

}

func (mt *MultiTerm) _calculateNonConjunctionResult(reader SearchIndexInterface) {
	requiredVectors := make([]*TopDocs, len(mt.terms))
	requiredVectorsSizes := make([]int, len(mt.terms))
	requiredVectorsIds := make([]int, len(mt.terms))

	optional := make([]int32, 0)
	prohibited := make([]int32, 0)

	for termId, term := range mt.terms {
		termDocs := reader.TermDocs(term)

		if !mt.signs[termId] {
			// required

			for _, v := range termDocs {
				if requiredVectors[termId] == nil {
					requiredVectors[termId] = newTopDocs()
				}

				requiredVectors[termId].AddDoc(&ScoreDoc{Doc: v})

			}

			requiredVectorsSizes[termId] = len(termDocs)
			requiredVectorsIds[termId] = termId

		} else if !mt.signs[termId] {
			// prohibited
			// array union
			prohibited = append(prohibited, termDocs...)
		} else {
			// neither required, nor prohibited
			// array union
			optional = append(optional, termDocs...)
		}

		mt.termsFreqs[termId] = reader.TermFreqs(term)
	}

	var required *TopDocs
	optionalDocs := newTopDocs()
	for _, nextResVector := range requiredVectors {
		if required == nil {
			required = nextResVector
		} else {
			updatedVector := newTopDocs()

			for _, value := range required.ScoreDocs {
				for _, d := range nextResVector.ScoreDocs {
					if d.Doc == value.Doc {
						updatedVector.AddDoc(value)
					}
				}
			}
			required = updatedVector
		}

		if len(required.ScoreDocs) == 0 {
			break
		}
	}

	for _, op := range optional {
		optionalDocs.AddDoc(&ScoreDoc{Doc: op})
	}

	if required != nil {
		mt.resVector = required
	} else {
		mt.resVector = optionalDocs
	}

	if len(prohibited) != 0 {

		if len(mt.resVector.ScoreDocs) < len(prohibited) {
			updatedVector := mt.resVector
			for i := 0; i < len(mt.resVector.ScoreDocs); i++ {
				for _, d := range prohibited {
					if d == mt.resVector.ScoreDocs[i].Doc {
						copy(updatedVector.ScoreDocs[i:], updatedVector.ScoreDocs[i+1:])
						updatedVector.ScoreDocs[len(updatedVector.ScoreDocs)-1] = nil // or the zero value of T
						updatedVector.ScoreDocs = updatedVector.ScoreDocs[:len(updatedVector.ScoreDocs)-1]
					}
				}
			}
			mt.resVector = updatedVector
		} else {
			updatedVector := mt.resVector
			for i := 0; i < len(prohibited); i++ {
				copy(updatedVector.ScoreDocs[i:], updatedVector.ScoreDocs[i+1:])
				updatedVector.ScoreDocs[len(updatedVector.ScoreDocs)-1] = nil // or the zero value of T
				updatedVector.ScoreDocs = updatedVector.ScoreDocs[:len(updatedVector.ScoreDocs)-1]
			}
			mt.resVector = updatedVector
		}
	}

	mt.resVector.SortById()

}

func (mt *MultiTerm) _conjunctionScore(docId int, reader SearchIndexInterface) float32 {
	dId := int32(docId)
	mt.coord = reader.Similarity().Coord(int32(len(mt.terms)), int32(len(mt.terms)))

	var score float32

	for termId, _ := range mt.terms {
		/**
		 * We don't need to check that term freq is not 0
		 * Score calculation is performed only for matched docs
		 */
		score += reader.Similarity().Tf(float32(mt.termsFreqs[termId][dId])) * mt.weights[termId].Value()
	}

	return score * mt.coord * mt.Boost()
}

func (mt *MultiTerm) _nonConjunctionScore(docId int, reader SearchIndexInterface) float32 {
	dId := int32(docId)
	var maxCoord, matchedTerms int
	var score float32
	for _, sign := range mt.signs {
		if sign {
			maxCoord++
		}
	}

	coords := make([]float32, maxCoord)

	for count := 0; count <= maxCoord; count++ {
		coords[count] = reader.Similarity().Coord(int32(count), int32(maxCoord))
	}

	for termId, _ := range mt.terms {
		// Check if term is
		if mt.signs[termId] {
			for id, tf := range mt.termsFreqs {
				if id == termId {
					if _, ok := tf[dId]; ok {
						matchedTerms++
					}
				}
			}

			/**
			 * We don't need to check that term freq is not 0
			 * Score calculation is performed only for matched docs
			 */
			score += reader.Similarity().Tf(float32(mt.termsFreqs[termId][dId])) * mt.weights[termId].Value()
		}
	}

	return score * coords[matchedTerms] * mt.Boost()
}

func (mt *MultiTerm) Execute(reader SearchIndexInterface) {
	if mt.signs == nil {
		mt._calculateConjunctionResult(reader)
	} else {
		mt._calculateNonConjunctionResult(reader)
	}

	// Initialize weight if it's not done yet
	mt.initWeight(reader)
}

func (mt *MultiTerm) MatchedDocs() *TopDocs {
	return mt.resVector
}

func (mt *MultiTerm) Score(docId int, reader SearchIndexInterface) float32 {

	for _, val := range mt.resVector.ScoreDocs {
		if val.Doc == int32(docId) {
			if mt.signs == nil {
				return mt._conjunctionScore(docId, reader)
			} else {
				return mt._nonConjunctionScore(docId, reader)
			}
		}
	}

	return 0
}

func (mt *MultiTerm) QueryTerms() []*index.Term {

	if mt.signs == nil {
		return mt.terms
	}

	terms := make([]*index.Term, len(mt.signs))

	for i, sign := range mt.signs {
		if sign {
			terms[i] = mt.terms[i]
		}
	}

	return terms
}

type MultiTermWeight struct {
	*BaseWeight
	reader  SearchIndexInterface
	query   *MultiTerm
	weights []Weight
}

func newMultiTermWeight(query *MultiTerm, reader SearchIndexInterface) *MultiTermWeight {
	ans := &MultiTermWeight{reader: reader, query: query, weights: make([]Weight, len(query.terms))}
	ans.BaseWeight = newBaseWeight(ans)

	signs := query.signs

	for id, term := range query.terms {
		if signs == nil || signs[id] {
			ans.weights[id] = newTermWeight(query, term, reader)
			query.weights[id] = ans.weights[id]
		}
	}

	return ans
}

func (mtw *MultiTermWeight) value() float32 {
	return mtw.query.Boost()
}

func (mtw *MultiTermWeight) sumOfSquaredWeights() float32 {
	var sum float32
	for _, weight := range mtw.weights {
		// sum sub weights
		sum += weight.SumOfSquaredWeights()
	}

	// boost each sub-weight
	sum *= mtw.query.Boost() * mtw.query.Boost()

	// check for empty query (like '-something -another')
	if sum == 0 {
		sum = 1.0
	}
	return sum
}

func (mtw *MultiTermWeight) normalize(queryNorm float32) {
	queryNorm *= mtw.query.Boost()

	for _, weight := range mtw.weights {
		weight.Normalize(queryNorm)
	}
}
