package query

import (
	"bitbucket.org/jtejido/inverted/index"
)

type Term struct {
	*BaseQuery
	term      *index.Term
	termFreqs map[int32]int32
	docVector *TopDocs
}

func NewTerm(term *index.Term) *Term {
	ans := &Term{term: term}
	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

func (t *Term) Rewrite(idx SearchIndexInterface) Query {
	if t.term.Field != "" {
		return t
	}

	query := NewMultiTerm(nil, nil)
	query.SetBoost(t.Boost())

	for _, fieldName := range idx.FieldNames(true) {
		term := index.NewTerm(t.term.Text, fieldName)

		query.AddTerm(term)
	}

	return query.Rewrite(idx)
}

func (t *Term) CreateWeight(reader SearchIndexInterface) Weight {
	t.weight = newTermWeight(t, t.term, reader)
	return t.weight
}

func (t *Term) Execute(reader SearchIndexInterface) {
	t.docVector = newTopDocs()

	for _, v := range reader.TermDocs(t.term) {
		t.docVector.AddDoc(&ScoreDoc{Doc: v})
	}

	t.termFreqs = reader.TermFreqs(t.term)

	// Initialize weight if it's not done yet
	t.initWeight(reader)
}

func (t *Term) MatchedDocs() *TopDocs {
	return t.docVector
}

func (t *Term) Score(docId int, reader SearchIndexInterface) float32 {
	dId := int32(docId)
	for _, d := range t.docVector.ScoreDocs {
		if d.Doc == dId {
			return reader.Similarity().Tf(float32(t.termFreqs[dId])) * t.weight.Value() * t.Boost()
		}

	}

	return 0
}

func (t *Term) QueryTerms() []*index.Term {
	return []*index.Term{t.term}
}

type TermWeight struct {
	*BaseWeight
	reader           SearchIndexInterface
	term             *index.Term
	query            Query
	idf, queryWeight float32
}

func newTermWeight(query Query, term *index.Term, reader SearchIndexInterface) *TermWeight {
	ans := &TermWeight{reader: reader, query: query, term: term}
	ans.BaseWeight = newBaseWeight(ans)

	return ans
}

func (tw *TermWeight) value() float32 {
	return tw.val
}

func (tw *TermWeight) sumOfSquaredWeights() float32 {

	tw.idf = tw.reader.Similarity().Idf([]*index.Term{tw.term}, tw.reader)

	// compute query weight
	tw.queryWeight = tw.idf * tw.query.Boost()

	// square it
	return tw.queryWeight * tw.queryWeight
}

func (tw *TermWeight) normalize(queryNorm float32) {
	tw.queryNorm = queryNorm

	// normalize query weight
	tw.queryWeight *= queryNorm

	// idf for documents
	tw.val = tw.queryWeight * tw.idf
}
