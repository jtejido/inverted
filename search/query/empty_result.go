package query

import (
	"bitbucket.org/jtejido/inverted/index"
)

type EmptyResult struct {
	*BaseQuery
}

func NewEmptyResult() *EmptyResult {
	ans := &EmptyResult{}
	ans.BaseQuery = NewBaseQuery(1)
	return ans
}

func (i *EmptyResult) Subqueries() []Query {
	return nil
}

func (i *EmptyResult) MatchedDocs() *TopDocs {
	return nil
}

func (i *EmptyResult) Score(docId int, reader SearchIndexInterface) float32 {
	return 0
}

func (i *EmptyResult) QueryTerms() []*index.Term {
	return nil
}

func (i *EmptyResult) Execute(reader SearchIndexInterface) {
}

func (i *EmptyResult) Rewrite(index SearchIndexInterface) Query {
	return i
}

func (i *EmptyResult) CreateWeight(reader SearchIndexInterface) Weight {
	return newEmptyResultWeight()
}
