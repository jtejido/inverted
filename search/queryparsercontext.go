package search

import (
    "bitbucket.org/jtejido/inverted/search/query"
    "bitbucket.org/jtejido/inverted/search/queryentry"

    "fmt"
    "reflect"
)

type QueryParserContext struct {
    DefaultField        string
    Mode                bool
    entries             []interface{}
    signs               []bool
    nextEntryField      string
    isNextEntryFieldSet bool
    nextEntrySign       bool
    isNextEntrySigned   bool
}

const (
    GM_BOOLEAN = true
    GM_SIGNS   = false
)

func NewQueryParserContext(defaultField string) *QueryParserContext {

    qpc := &QueryParserContext{
        DefaultField:        defaultField,
        Mode:                false,
        signs:               make([]bool, 0),
        nextEntryField:      "",
        isNextEntryFieldSet: false,
        nextEntrySign:       false,
        isNextEntrySigned:   false,
        entries:             make([]interface{}, 0),
    }

    return qpc

}

func (qpc *QueryParserContext) Query() query.Query {
    if qpc.Mode == GM_BOOLEAN {
        return qpc.BooleanExpressionQuery()
    } else {
        return qpc.SignStyleExpressionQuery()
    }
}

func (qpc *QueryParserContext) BooleanExpressionQuery() query.Query {
    /**
     * We treat each level of an expression as a boolean expression in
     * a Disjunctive Normal Form
     *
     * AND operator has higher precedence than OR
     *
     * Thus logical query is a disjunction of one or more conjunctions of
     * one or more query entries
     */
    var conjuctions = make([][][]interface{}, 0)

    expressionRecognizer, _ := NewBooleanExpressionRecognizer()

    for _, entry := range qpc.entries {

        if e, ok := entry.(queryentry.QueryEntry); ok {
            expressionRecognizer.ProcessLiteral(e)
        } else {
            switch v := entry.(type) {
            case int:
                switch v {
                case TT_AND_LEXEME:
                    expressionRecognizer.ProcessOperator(IN_AND_OPERATOR)
                    break
                case TT_OR_LEXEME:
                    expressionRecognizer.ProcessOperator(IN_OR_OPERATOR)
                    break
                case TT_NOT_LEXEME:
                    expressionRecognizer.ProcessOperator(IN_NOT_OPERATOR)
                    break
                default:
                    panic("Boolean expression error. Unknown operator type.")
                }
            default:
                panic(fmt.Sprintf("unknown entry %v", entry))
            }
        }
    }

    conjuctions, _ = expressionRecognizer.FinishExpression()

    // Remove 'only negative' conjunctions
    for conjuctionId, conjuction := range conjuctions {
        nonNegativeEntryFound := false

        for _, conjuctionEntry := range conjuction {
            if conjuctionEntry[1] == true {
                nonNegativeEntryFound = true
                break
            }
        }

        if nonNegativeEntryFound == false {
            conjuctions = append(conjuctions[:conjuctionId], conjuctions[conjuctionId+1:]...)
        }
    }

    var subqueries []query.Query = make([]query.Query, 0)
    for _, conjuction := range conjuctions {
        // Check, if it's a one term conjuction
        if len(conjuction) == 1 {
            subqueries = append(subqueries, conjuction[0][0].(queryentry.QueryEntry).Query())
        } else {
            subquery := query.NewBoolean()

            for _, conjuctionEnt := range conjuction {
                subquery.AddSubquery(conjuctionEnt[0].(queryentry.QueryEntry).Query(), conjuctionEnt[1].(bool))
            }

            subqueries = append(subqueries, subquery)
        }
    }

    // if (len(subqueries) == 0) {
    //     return new Query\Insignificant()
    // }

    if len(subqueries) == 1 {
        return subqueries[0].(query.Query)
    }

    var q = query.NewBoolean()

    for _, subquery := range subqueries {
        // Non-requirered entry/subquery
        q.AddSubquery(subquery, false)
    }

    return q
}

// *query.Boolean for now
func (qpc *QueryParserContext) SignStyleExpressionQuery() query.Query {
    var query = query.NewBoolean()

    var defaultSign bool = false

    if GetDefaultOperator() == B_AND {
        defaultSign = true // required
    }

    for entryId, entry := range qpc.entries {
        var sign bool = defaultSign
        if qpc.signs != nil {

            if len(qpc.signs) > entryId {
                sign = qpc.signs[entryId]
            }
        }

        query.AddSubquery(entry.(queryentry.QueryEntry).Query(), sign)
    }

    return query
}

func (qpc QueryParserContext) Field() string {
    if qpc.isNextEntryFieldSet == true {
        return qpc.nextEntryField
    } else {
        return qpc.DefaultField
    }

}

func (qpc *QueryParserContext) AddEntry(entry queryentry.QueryEntry) {
    if qpc.Mode != GM_BOOLEAN {
        qpc.signs = append(qpc.signs, qpc.nextEntrySign)
    }

    qpc.entries = append(qpc.entries, entry)

    qpc.nextEntryField = ""
    qpc.isNextEntryFieldSet = false
    qpc.nextEntrySign = false
    qpc.isNextEntrySigned = false
}

func (qpc *QueryParserContext) SetNextEntryField(field string) {
    qpc.nextEntryField = field
    qpc.isNextEntryFieldSet = true
}

func (qpc *QueryParserContext) SetNextEntrySign(sign int) error {
    if qpc.Mode == GM_BOOLEAN {
        return fmt.Errorf("It's not allowed to mix boolean and signs styles in the same subquery.")
    }

    qpc.Mode = GM_SIGNS

    if sign == TT_REQUIRED {
        qpc.nextEntrySign = true
        qpc.isNextEntrySigned = true
    } else if sign == TT_PROHIBITED {
        qpc.nextEntrySign = false
        qpc.isNextEntrySigned = true
    } else {
        return fmt.Errorf("Unrecognized sign type.")
    }

    return nil
}

func (qpc *QueryParserContext) ProcessFuzzyProximityModifier(parameter float32) (err error) {
    // Check, that modifier has came just after word or phrase
    if qpc.isNextEntryFieldSet == true || qpc.isNextEntrySigned == true {
        err = fmt.Errorf("'~' modifier must follow word or phrase.")
    }

    lastEntry := qpc.entries[len(qpc.entries)-1]

    lastEntry.(queryentry.QueryEntry).ProcessFuzzyProximityModifier(parameter)

    qpc.entries = append(qpc.entries, lastEntry)

    return
}

func (qpc *QueryParserContext) Boost(boostFactor float32) (err error) {
    // Check, that modifier has came just after word or phrase
    if qpc.isNextEntryFieldSet == true || qpc.isNextEntrySigned == true {
        err = fmt.Errorf("'~' modifier must follow word, phrase or subquery.")
    }

    lastEntry := qpc.entries[len(qpc.entries)-1]

    lastEntry.(queryentry.QueryEntry).Boost(boostFactor)

    qpc.entries = append(qpc.entries, lastEntry)

    return
}

func (qpc *QueryParserContext) AddLogicalOperator(operator int) (err error) {
    if qpc.Mode == GM_SIGNS {
        err = fmt.Errorf("It's not allowed to mix boolean and signs styles in the same subquery.")
    }

    qpc.Mode = GM_BOOLEAN

    qpc.entries = append(qpc.entries, operator)

    return
}

func (qpc QueryParserContext) instanceOf(objectPtr queryentry.QueryEntry, typePtr interface{}) bool {
    return reflect.TypeOf(objectPtr) == reflect.TypeOf(typePtr)
}
