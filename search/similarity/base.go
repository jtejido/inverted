package similarity

import (
	"bitbucket.org/jtejido/inverted/index"
	"bitbucket.org/jtejido/inverted/util"
)

var NORM_TABLE []float32

type SearchIndexInterface interface {
	Count() int32
	Similarity() Similarity
	DocFreq(*index.Term) float32
}

type iSimilarity interface {
	lengthNorm(fieldName string, numTokens int) float32
	queryNorm(sumOfSquaredWeights float32) float32
	tf(freq float32) float32
	idf(docFreq, docCount float32) float32
	sloppyFreq(distance int32) float32
	coord(overlap, maxOverlap int32) float32
}

type Similarity interface {
	EncodeNorm(f float32) int64
	DecodeNorm(norm int64) float32
	LengthNorm(fieldName string, numTokens int) float32
	QueryNorm(sumOfSquaredWeights float32) float32
	Tf(freq float32) float32
	Idf(input []*index.Term, reader SearchIndexInterface) float32
	SloppyFreq(distance int32) float32
	Coord(overlap, maxOverlap int32) float32
}

type BaseSimilarity struct {
	spi iSimilarity
}

func newBaseSimilarity(spi iSimilarity) *BaseSimilarity {
	return &BaseSimilarity{spi}
}

func (bs *BaseSimilarity) EncodeNorm(f float32) int64 {
	return int64(util.FloatToByte315(f))
}

func (bs *BaseSimilarity) DecodeNorm(norm int64) float32 {
	return NORM_TABLE[int(norm&0xff)] // & 0xFF maps negative bytes to positive above 127
}

func (bs *BaseSimilarity) Idf(input []*index.Term, reader SearchIndexInterface) float32 {
	var idf float32
	for _, term := range input {
		idf += bs.spi.idf(reader.DocFreq(term), float32(reader.Count()))
	}
	return idf
}

func (bs *BaseSimilarity) LengthNorm(fieldName string, numTokens int) float32 {
	return bs.spi.lengthNorm(fieldName, numTokens)
}

func (bs *BaseSimilarity) QueryNorm(sumOfSquaredWeights float32) float32 {
	return bs.spi.queryNorm(sumOfSquaredWeights)
}

func (bs *BaseSimilarity) Tf(freq float32) float32 {
	return bs.spi.tf(freq)
}

func (bs *BaseSimilarity) Coord(overlap, maxOverlap int32) float32 {
	return bs.spi.coord(overlap, maxOverlap)
}

func (bs *BaseSimilarity) SloppyFreq(distance int32) float32 {
	return bs.spi.sloppyFreq(distance)
}
