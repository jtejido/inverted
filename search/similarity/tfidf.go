package similarity

import (
	"bitbucket.org/jtejido/inverted/util"
	"math"
)

type TfIdf struct {
	*BaseSimilarity
}

func NewTfIdf() *TfIdf {
	ans := new(TfIdf)
	ans.BaseSimilarity = newBaseSimilarity(ans)
	NORM_TABLE = ans.buildNormTable()
	return ans
}

func (tfidf *TfIdf) buildNormTable() []float32 {
	table := make([]float32, 256)
	for i, _ := range table {
		table[i] = util.Byte315ToFloat(byte(i))
	}
	return table
}

/**
 * Implemented as '1/sqrt(numTerms)'.
 */
func (tfidf *TfIdf) lengthNorm(fieldName string, numTokens int) float32 {
	if numTokens == 0 {
		return 1E10
	}

	return 1.0 / float32(math.Sqrt(float64(numTokens)))
}

/**
 * Implemented as '1/sqrt(sumOfSquaredWeights)'.
 */
func (tfidf *TfIdf) queryNorm(sumOfSquaredWeights float32) float32 {
	return 1.0 / float32(math.Sqrt(float64(sumOfSquaredWeights)))
}

/**
 * Implemented as 'sqrt(freq)'.
 */
func (tfidf *TfIdf) tf(freq float32) float32 {
	return float32(math.Sqrt(float64(freq)))
}

/**
 * Implemented as '1/(distance + 1)'.
 */
func (tfidf *TfIdf) sloppyFreq(distance int32) float32 {
	return 1.0 / (float32(distance) + 1)
}

/**
 * Implemented as 'log(numDocs/(docFreq+1)) + 1'.
 */
func (tfidf *TfIdf) idf(docFreq, numDocs float32) float32 {
	return float32(math.Log(float64(numDocs/(docFreq+1)))) + 1.0
}

/**
 * Implemented as 'overlap/maxOverlap'.
 */
func (tfidf *TfIdf) coord(overlap, maxOverlap int32) float32 {
	return float32(overlap) / float32(maxOverlap)
}
