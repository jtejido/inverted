package fsm

type FSMAction struct {
    method func() error
}

func NewFSMAction(method func() error) *FSMAction {
    return &FSMAction{
        method: method,
    }
}

func (fsma *FSMAction) DoAction() error {
    return fsma.method()
}