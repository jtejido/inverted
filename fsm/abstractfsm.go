package fsm

import (
    "fmt"
)

type AbstractFSM struct {
    States map[int]int
    InputAlphabet map[int]int
    Rules map[int]map[int]int
    InputActions map[int]map[int][]*FSMAction
    EntryActions map[int][]*FSMAction
    ExitActions map[int][]*FSMAction
    TransitionActions map[int]map[int][]*FSMAction
    CurrentState int
}

func (qp *AbstractFSM) AddRules(rules [][]interface{}) error {
    var rule4 *FSMAction
    for _, rule := range rules {
        rule4 = nil

        if len(rule) > 3 {
            rule4 = rule[3].(*FSMAction)
        }
        
        err := qp.AddRule(rule[0].(int), rule[1].(int), rule[2].(int), rule4)
        if err != nil {
            return err
        }
    }

    return nil
}

func (qp *AbstractFSM) AddRule(sourceState, input, targetState int, inputAction *FSMAction) error {
        _, ok_sstates := qp.States[sourceState]
        if !ok_sstates {
            return fmt.Errorf("Undefined source state.")
        }
        _, ok_tstates := qp.States[targetState]
        if !ok_tstates {
            return fmt.Errorf("Undefined target state.")
        }
        _, ok_input := qp.InputAlphabet[input]
        if !ok_input {
            return fmt.Errorf("Undefined input symbol.")
        }
        _, ok_rules := qp.Rules[sourceState]
        if !ok_rules {
            qp.Rules[sourceState] = make(map[int]int)
        }
        _, ok_rulessi := qp.Rules[sourceState][input]
        if ok_rulessi {
            return fmt.Errorf("Rule for {state,input} pair is already defined.")
        }

        qp.Rules[sourceState][input] = targetState


        if inputAction != nil {
            err_ia := qp.AddInputAction(sourceState, input, inputAction)
            if err_ia != nil {
                return err_ia
            }
        }

        return nil
}

func (qp *AbstractFSM) AddInputAction(state, inputSymbol int, action *FSMAction) error {
        _, ok_sstates := qp.States[state]
        if !ok_sstates {
            return fmt.Errorf("Undefined state.")
        }
        _, ok_ialphabet := qp.InputAlphabet[inputSymbol]
        if !ok_ialphabet {
            return fmt.Errorf("Undefined input symbol.")
        }
        _, ok_iactions := qp.InputActions[state]
        if !ok_iactions {
            qp.InputActions[state] = make(map[int][]*FSMAction)
        }
        _, ok_isactions := qp.InputActions[state][inputSymbol]
        if !ok_isactions {
            qp.InputActions[state][inputSymbol] = make([]*FSMAction, 0)
        }

        qp.InputActions[state][inputSymbol] = append(qp.InputActions[state][inputSymbol], action)

        return nil
}

func (qp *AbstractFSM) AddEntryAction(state int, action *FSMAction) error {
        _, ok_iactions := qp.States[state]
        if !ok_iactions {
            return fmt.Errorf("Undefined state.")
        }

        _, ok_eactions := qp.EntryActions[state]
        if !ok_eactions {
            qp.EntryActions[state] = make([]*FSMAction, 0)
        }

        qp.EntryActions[state] = append(qp.EntryActions[state], action)

        return nil
}

func (qp *AbstractFSM) AddTransitionAction(sourceState, targetState int, action *FSMAction) error {

    _, ok_state := qp.States[sourceState]
    if !ok_state {
        return fmt.Errorf("Undefined source state.")
    }
    _, ok_tstates := qp.States[targetState]
    if !ok_tstates {
        return fmt.Errorf("Undefined source state.")
    }

    _, ok_tactions := qp.TransitionActions[sourceState]
    if !ok_tactions {

        qp.TransitionActions[sourceState] = make(map[int][]*FSMAction)
    }
    _, ok_tactionstate := qp.TransitionActions[sourceState][targetState]
    if !ok_tactionstate {
        qp.TransitionActions[sourceState][targetState] = make([]*FSMAction, 0)
    }

    qp.TransitionActions[sourceState][targetState] = append(qp.TransitionActions[sourceState][targetState], action)
    return nil
}

func (qp *AbstractFSM) Process(input int) error {

    _, ok_cs := qp.Rules[qp.CurrentState]
    if !ok_cs {
        return fmt.Errorf("There is no any rule for current state.")
    }
    _, ok_input := qp.Rules[qp.CurrentState][input]
    if !ok_input {
        return fmt.Errorf("There is no any rule for {current state, input} pair")
    }

    sourceState := qp.CurrentState
    targetState := qp.Rules[qp.CurrentState][input]

    _, ok_exitact := qp.ExitActions[sourceState]
    if sourceState != targetState  &&  ok_exitact {
        for _, action := range qp.ExitActions[sourceState] { 
            err := action.DoAction()
            if err != nil {
                return err
            }
        }
    }

    _, ok_inputactcs := qp.InputActions[sourceState]
    _, ok_inputactcsinput := qp.InputActions[sourceState][input]

    if ok_inputactcs && ok_inputactcsinput {
        for _, action := range qp.InputActions[sourceState][input] {
            err := action.DoAction()
            if err != nil {
                return err
            }
        }
    }

    qp.CurrentState = targetState

    _, ok_trnactcs := qp.TransitionActions[sourceState]
    _, ok_trnactcsinput := qp.TransitionActions[sourceState][targetState]


    if ok_trnactcs && ok_trnactcsinput {
        for _, action := range qp.TransitionActions[sourceState][targetState] {
            err := action.DoAction()
            if err != nil {
                return err
            }
        }
    }

    _, ok_enactts := qp.EntryActions[targetState]

    if sourceState != targetState  &&  ok_enactts {
        for _, action := range qp.EntryActions[targetState] {
            err := action.DoAction()
            if err != nil {
                return err
            }
        }
    }

    return nil
}

func (qp AbstractFSM) GetState() int {
    return qp.CurrentState
}


func (qp *AbstractFSM) Reset() {
    if len(qp.States) == 0 {
        panic("There is no any state defined for FSM.")
    }

    qp.CurrentState = qp.States[0]
}
