package file

import (
	"bufio"
	"fmt"
	"os"
)

const MIN_BUFFER_SIZE int64 = 1024

type FileSystem struct {
	FileHandle *os.File
	byte       []byte
	Pos        int64
}

func NewFileSystem(filepath string, create bool) (*FileSystem, error) {

	var file *os.File
	var err error
	var filesize int64

	if create == false {
		if _, err_r := os.Stat(filepath); os.IsNotExist(err_r) {
			return nil, err_r
		}
		file, err = os.OpenFile(filepath, os.O_RDWR, 0644)
		fileinfo, err := file.Stat()
		if err != nil {
			return nil, err
		}
		filesize = fileinfo.Size()
	} else {
		file, err = os.Create(filepath)
		filesize = MIN_BUFFER_SIZE
	}

	if err != nil {
		os.Exit(0)
		return nil, err
	}

	text := make([]byte, filesize)

	if create == false {
		file.Read(text)
	}

	return &FileSystem{
		FileHandle: file,
		byte:       text,
		Pos:        0,
	}, nil

}

func (fs *FileSystem) Flush() error {
	err := fs.FileHandle.Sync()

	if err != nil {
		return err
	}

	return nil
}

func (fs *FileSystem) Reset(bytes []byte) {
	fs.byte = bytes
	fs.Pos = 0
}

func (fs *FileSystem) Position() int64 {
	return fs.Pos
}

func (fs *FileSystem) Rewind() {
	fs.Pos = 0
}

func (fs *FileSystem) SkipBytes(count int64) {
	fs.Pos += count
}

func (fs *FileSystem) ReadShort() (n int16, err error) {
	fs.Pos += 2
	return (int16(fs.byte[fs.Pos-2]) << 8) | int16(fs.byte[fs.Pos-1]), nil
}

func (fs *FileSystem) ReadInt() (n int32, err error) {
	fs.Pos += 4
	return (int32(fs.byte[fs.Pos-4]) << 24) | (int32(fs.byte[fs.Pos-3]) << 16) |
		(int32(fs.byte[fs.Pos-2]) << 8) | int32(fs.byte[fs.Pos-1]), nil
}

func (fs *FileSystem) ReadString() string {
	length, _ := fs.ReadVInt()

	text := make([]byte, length)
	fs.FileHandle.Read(text)
	return string(text)
}

func (fs *FileSystem) ReadLong() (n int64, err error) {
	i1, _ := fs.ReadInt()
	i2, _ := fs.ReadInt()
	return (int64(i1) << 32) | int64(i2), nil
}

func (fs *FileSystem) ReadVInt() (n int32, err error) {

	if b, err := fs.ReadByte(); err == nil {
		n = int32(b) & 0x7F
		if b < 128 {
			return n, nil
		}
		if b, err = fs.ReadByte(); err == nil {
			n |= (int32(b) & 0x7F) << 7
			if b < 128 {
				return n, nil
			}
			if b, err = fs.ReadByte(); err == nil {
				n |= (int32(b) & 0x7F) << 14
				if b < 128 {
					return n, nil
				}
				if b, err = fs.ReadByte(); err == nil {
					n |= (int32(b) & 0x7F) << 21
					if b < 128 {
						return n, nil
					}
					if b, err = fs.ReadByte(); err == nil {
						// Warning: the next ands use 0x0F / 0xF0 - beware copy/paste errors:
						n |= (int32(b) & 0x0F) << 28

						if int32(b)&0xF0 == 0 {
							return n, nil
						}
						return 0, fmt.Errorf("Invalid vInt detected (too many bits)")
					}
				}
			}
		}
	}
	return 0, err
}

func (fs *FileSystem) ReadByte() (b byte, err error) {
	fs.Pos++
	return fs.byte[fs.Pos-1], nil
}

func (fs *FileSystem) ReadBytes(buf []byte) ([]byte, error) {
	copy(buf, fs.byte[fs.Pos:fs.Pos+int64(len(buf))])
	fs.Pos += int64(len(buf))
	return buf, nil
}

func (fs *FileSystem) Seek(offset int64, whence int) (int64, error) {

	var newoffset int64
	switch whence {
	case 0:
		newoffset = offset
	case 1:
		newoffset = fs.Pos + offset
	case 2:
		newoffset = int64(len(fs.byte)) - offset
	}

	if newoffset == fs.Pos {
		return newoffset, nil
	}

	fs.Pos = newoffset
	return fs.Pos, nil
}

func (fs *FileSystem) Tell() int64 {
	return fs.Pos
}

func (fs *FileSystem) Close() error {

	err := fs.FileHandle.Close()

	if err != nil {
		return err
	}

	return nil
}

func (fs *FileSystem) WriteByte(b byte) error {

	fs.byte[fs.Pos] = b

	_, err := fs.FileHandle.WriteAt([]byte{b}, fs.Pos)
	if err != nil {
		return fmt.Errorf("Error writing data: ", err)
	}

	fs.Pos++
	return nil
}

func (fs *FileSystem) WriteBytes(b []byte) error {

	for _, v := range b {
		fs.byte[fs.Pos] = v
		fs.Pos++
	}

	w := bufio.NewWriter(fs.FileHandle)

	_, err := w.Write(b)
	if err != nil {
		fmt.Errorf("Error writing data: ", err)
	}

	w.Flush()

	return nil
}

func (fs *FileSystem) WriteVInt(i int32) error {
	for (i & ^0x7F) != 0 {
		err := fs.WriteByte(byte(i&0x7F) | 0x80)
		if err != nil {
			return err
		}
		i = int32(uint32(i) >> 7)
	}
	return fs.WriteByte(byte(i))
}

func (fs *FileSystem) WriteString(s string) error {

	bytes := []byte(s)
	err := fs.WriteVInt(int32(len(bytes)))
	if err == nil {
		err = fs.WriteBytes(bytes)
	}
	return err
}

func (fs *FileSystem) WriteInt(i int32) error {

	err := fs.WriteByte(byte(i >> 24))
	if err == nil {
		err = fs.WriteByte(byte(i >> 16))
		if err == nil {
			err = fs.WriteByte(byte(i >> 8))
			if err == nil {
				err = fs.WriteByte(byte(i))
			}
		}
	}
	return err
}

func (fs *FileSystem) WriteLong(i int64) error {
	err := fs.WriteInt(int32(i >> 32))
	if err == nil {
		err = fs.WriteInt(int32(i))
	}
	return err
}
