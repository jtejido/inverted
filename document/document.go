package document

import (
	. "bitbucket.org/jtejido/inverted/document/field"
	"fmt"
)

type Document struct {
	fields map[string]*Field
}

func NewDocument() *Document {
	return &Document{
		fields: make(map[string]*Field),
	}
}

func (d *Document) AddField(f *Field) *Document {
	d.fields[f.Name] = f
	return d
}

func (d *Document) GetFieldNames() []string {
	keys := make([]string, 0, len(d.fields))
    for k := range d.fields {
        keys = append(keys, k)
    }
    return keys
}

func (d *Document) GetField(field string) *Field {
	v, ok := d.fields[field]
	if !ok {
		fmt.Errorf("field name not found!")
	}

	return v
}

func (d *Document) GetFieldValue(field string) string {
	v, ok := d.fields[field]
	if !ok {
		fmt.Errorf("field name not found!")
	}

	return v.Value
}

