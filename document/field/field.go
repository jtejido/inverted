package field

type Field struct {
	Name 			  string
	Value             string
	Stored 			  bool
	Indexed			  bool
	Tokenized		  bool
	StoreTermVector   bool
	Binary 		      bool
}

func NewField(name string, value string, stored bool, indexed bool, tokenized bool) *Field {
	return &Field{
		Name: name,
		Value: value,
		Stored: stored,
		Indexed:	indexed,
		Tokenized: tokenized,
		StoreTermVector: false,
		Binary: false,
	}
}