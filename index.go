package goffer

import (
    . "bitbucket.org/jtejido/inverted/document"
    "bitbucket.org/jtejido/inverted/index"
    . "bitbucket.org/jtejido/inverted/search"
    "bitbucket.org/jtejido/inverted/search/query"
    "bitbucket.org/jtejido/inverted/search/similarity"
    "bitbucket.org/jtejido/inverted/store"
    . "bitbucket.org/jtejido/inverted/util"
    "fmt"
)

type SearchIndexInterface interface {
    index.TermsStream
    Count() int32
    Similarity() similarity.Similarity
    DocFreq(*index.Term) float32
    FieldNames(indexed bool) []string
    HasTerm(term *index.Term) bool
    TermPositions(term *index.Term) map[int32][]int32
    TermFreqs(term *index.Term) map[int32]int32
    TermDocs(term *index.Term) []int32
}

type Index struct {
    Directory    store.Directory
    CloseOnExit  bool
    Generation   int64
    Version      int
    DocCount     int32
    HasChanges   bool
    SegmentInfos map[string]*index.SegmentInfo
    Writer       *index.Writer
    termsStream  *TermStreamsPriorityQueue
}

func NewIndex(path string, create bool) *Index {

    var nameCounter int32

    dir, err := store.OpenFSDirectory(path)

    if err != nil {
        panic(err)
    }

    var gen = index.GetActualGeneration(dir)
    if create {
        if gen == -1 {
            // Directory doesn't contain existing index, start from 1
            gen = 1
            nameCounter = 0
        } else {
            // Directory contains existing index
            segmentsFile, _ := dir.OpenInput(GetSegmentFileName(gen))

            segmentsFile.Seek(12, 0) // 12 = 4 (int, file format marker) + 8 (long, index version)

            nameCounter, _ = segmentsFile.ReadInt()

            gen++
        }

        index.CreateIndex(dir, gen, nameCounter)
    }

    idx := &Index{
        Directory:    dir,
        CloseOnExit:  true,
        Generation:   gen,
        DocCount:     0,
        HasChanges:   false,
        Writer:       nil,
        SegmentInfos: make(map[string]*index.SegmentInfo),
    }

    if gen == -1 {
        fmt.Errorf("Index doesn't exists in the specified directory.")
    } else {
        idx.ReadSegmentsFile()
    }

    return idx

}

func (idx *Index) ReadSegmentsFile() {

    segmentsFile, err := idx.Directory.OpenInput(GetSegmentFileName(idx.Generation))

    if err != nil {
        panic(err)
    }

    format, err := segmentsFile.ReadInt()

    if err != nil {
        panic(err)
    }

    if format != FORMAT_SEGMENTS_GEN_SUB_VER {
        panic("Unsupported segments file format")
    }

    idx.Version = VERSION_1

    // read version
    segmentsFile.ReadLong()

    // read segment name counter
    segmentsFile.ReadInt()

    segments, _ := segmentsFile.ReadInt()

    idx.DocCount = 0

    // read segmentInfos
    for count := 0; count < int(segments); count++ {

        segName, _ := segmentsFile.ReadString()
        segSize, _ := segmentsFile.ReadInt()

        // 2.1+ specific properties
        delGen, _ := segmentsFile.ReadLong()
        docStoreOffset, _ := segmentsFile.ReadInt()

        var docStoreOptions *index.DocStoreOptions

        if docStoreOffset != FORMAT_SEGMENTS_SUB {
            docStoreSegment, _ := segmentsFile.ReadString()
            docStoreIsCompoundFile, _ := segmentsFile.ReadByte()

            var isCompound bool

            if int(docStoreIsCompoundFile) == 1 {
                isCompound = true
            }

            docStoreOptions = &index.DocStoreOptions{offset: docStoreOffset, segment: docStoreSegment, isCompound: isCompound}

        }

        var hasNorm bool

        hasSingleNormFile, _ := segmentsFile.ReadByte()

        if int(hasSingleNormFile) == 1 {
            hasNorm = true
        }
        numField, _ := segmentsFile.ReadInt()

        normGens := make([]int64, numField)

        if numField != FORMAT_SEGMENTS_SUB {
            for count1 := 0; count1 < int(numField); count1++ {
                normGens[count1], _ = segmentsFile.ReadLong()
            }
        }

        isCompoundByte, _ := segmentsFile.ReadByte()

        var isCompound bool

        if isCompoundByte == 0xFF {
            // The segment is not a compound file
            isCompound = false
        } else if isCompoundByte == 0x01 {
            // The segment is a compound file
            isCompound = true
        } else {
            panic("status unknown")
        }

        idx.DocCount += segSize

        idx.SegmentInfos[segName] = index.NewSegmentInfo(idx.Directory,
            segName,
            segSize,
            delGen,
            docStoreOptions,
            hasNorm,
            isCompound,
        )
    }
}

func (index *Index) AddDocument(doc *Document) {

    index.GetIndexWriter().AddDocument(doc)
    index.DocCount++
    index.HasChanges = true
}

func (idx *Index) GetIndexWriter() *index.Writer {
    if idx.Writer == nil {
        idx.Writer = index.NewWriter(idx.Directory, &idx.SegmentInfos, idx.Version)
    }

    return idx.Writer
}

func (index *Index) UpdateDocCount() {
    index.DocCount = 0
    for _, segInfo := range index.SegmentInfos {
        index.DocCount += segInfo.Count()
    }
}

func (index *Index) Commit() {
    if index.HasChanges == true {
        index.GetIndexWriter().Commit()

        index.UpdateDocCount()

        index.HasChanges = false
    }
}

func (index *Index) Optimize() {

    index.Commit()

    if len(index.SegmentInfos) > 1 || index.HasDeletions() == true {
        index.GetIndexWriter().Optimize()
        index.UpdateDocCount()
    }
}

func (index *Index) HasDeletions() bool {
    for _, segmentInfo := range index.SegmentInfos {
        if segmentInfo.HasDeletions() == true {
            return true
        }
    }

    return false
}

func (index *Index) Count() int32 {
    return index.DocCount
}

func (index *Index) FieldNames(indexed bool) []string {
    var result = make([]string, 0)
    for _, segmentInfo := range index.SegmentInfos {
        for _, value := range segmentInfo.Fields(indexed) {
            result = append(result, value)
        }
    }
    return result
}

func (index *Index) DocFreq(term *index.Term) float32 {
    var result float32
    for _, segInfo := range index.SegmentInfos {
        termInfo := segInfo.TermInfo(term)
        if termInfo != nil {
            result += float32(termInfo.DocFreq)
        }
    }

    return result
}

func (index *Index) TermFreqs(term *index.Term) map[int32]int32 {
    result := make(map[int32]int32)
    var segmentStartDocId int32

    for _, segmentInfo := range index.SegmentInfos {
        res := segmentInfo.TermFreqs(term, segmentStartDocId)
        for k, v := range res {
            if i, ok := result[k]; ok {
                result[k] += v
            } else {
                result[k] = v
            }
        }

        segmentStartDocId += segmentInfo.Count()
    }

    return result
}

func (index *Index) TermPositions(term *index.Term) map[int32][]int32 {
    result := make(map[int32][]int32)
    segmentStartDocId := 0

    for _, segmentInfo := range index.SegmentInfos {
        res := segmentInfo.TermPositions(term, segmentStartDocId)

        for k, v := range res {
            if i, ok := result[k]; ok {
                result[k] = append(result[k], v...)
            } else {
                if result[k] == nil {
                    result[k] = make([]int32, 0)
                }

                result[k] = append(result[k], v...)
            }
        }

        segmentStartDocId += segmentInfo.Count()
    }

    return result
}

func (index *Index) TermDocs(term *index.Term) []int32 {
    subResults := make([][]int32, len(index.SegmentInfos))
    result := make([]int32, 0)
    segmentStartDocId := 0

    for i, segmentInfo := range index.SegmentInfos {
        subResults[i] = segmentInfo.TermDocs(term, segmentStartDocId)

        segmentStartDocId += segmentInfo.Count()
    }

    if len(subResults) == 0 {
        return
    } else if len(subResults) == 1 {
        // Index is optimized (only one segment)
        // Do not perform array reindexing
        return subResults[0]
    } else {
        for _, subResult := range subResults {
            result = append(result, subResult...)
        }
    }

    return result
}

func (index *Index) HasTerm(term *index.Term) bool {
    for _, segInfo := range index.SegmentInfos {
        if segInfo.TermInfo(term) != nil {
            return true
        }
    }

    return false
}

func (index *Index) Similarity() similarity.Similarity {
    return similarity.NewTfIdf()
}

func (index *Index) Find(str string) (query.Query, error) {
    parser, err := NewQueryParser()

    if err != nil {
        return nil, err
    }

    query, err := parser.Parse(str)

    if err != nil {
        return nil, err
    }

    index.Commit()

    query = query.Rewrite(index) //.Optimize(index)
    query.Execute(index)
    return query, nil
}

// public function find($query)
// {
//     if (is_string($query)) {
//         $query = Search\QueryParser::parse($query);
//     } elseif (!$query instanceof Search\Query\AbstractQuery) {
//         throw new InvalidArgumentException('Query must be a string or ZendSearch\Lucene\Search\Query object');
//     }

//     $this->commit();

//     $hits   = array();
//     $scores = array();
//     $ids    = array();

//     $query = $query->rewrite($this)->optimize($this);

//     $query->execute($this);

//     $topScore = 0;

//     $resultSetLimit = Lucene::getResultSetLimit();
//     foreach ($query->matchedDocs() as $id => $num) {
//         $docScore = $query->score($id, $this);
//         if( $docScore != 0 ) {
//             $hit = new Search\QueryHit($this);
//             $hit->document_id = $hit->id = $id;
//             $hit->score = $docScore;

//             $hits[]   = $hit;
//             $ids[]    = $id;
//             $scores[] = $docScore;

//             if ($docScore > $topScore) {
//                 $topScore = $docScore;
//             }
//         }

//         if ($resultSetLimit != 0  &&  count($hits) >= $resultSetLimit) {
//             break;
//         }
//     }

//     if (count($hits) == 0) {
//         // skip sorting, which may cause a error on empty index
//         return array();
//     }

//     if ($topScore > 1) {
//         foreach ($hits as $hit) {
//             $hit->score /= $topScore;
//         }
//     }

//     if (func_num_args() == 1) {
//         // sort by scores
//         array_multisort($scores, SORT_DESC, SORT_NUMERIC,
//                         $ids,    SORT_ASC,  SORT_NUMERIC,
//                         $hits);
//     } else {
//         // sort by given field names

//         $argList    = func_get_args();
//         $fieldNames = $this->getFieldNames();
//         $sortArgs   = array();

//         // PHP 5.3 now expects all arguments to array_multisort be passed by
//         // reference (if it's invoked through call_user_func_array());
//         // since constants can't be passed by reference, create some placeholder variables.
//         $sortReg    = SORT_REGULAR;
//         $sortAsc    = SORT_ASC;
//         $sortNum    = SORT_NUMERIC;

//         $sortFieldValues = array();

//         for ($count = 1; $count < count($argList); $count++) {
//             $fieldName = $argList[$count];

//             if (!is_string($fieldName)) {
//                 throw new RuntimeException('Field name must be a string.');
//             }

//             if (strtolower($fieldName) == 'score') {
//                 $sortArgs[] = &$scores;
//             } else {
//                 if (!in_array($fieldName, $fieldNames)) {
//                     throw new RuntimeException('Wrong field name.');
//                 }

//                 if (!isset($sortFieldValues[$fieldName])) {
//                     $valuesArray = array();
//                     foreach ($hits as $hit) {
//                         try {
//                             $value = $hit->getDocument()->getFieldValue($fieldName);
//                         } catch (\Exception $e) {
//                             if (strpos($e->getMessage(), 'not found') === false) {
//                                 throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
//                             } else {
//                                 $value = null;
//                             }
//                         }

//                         $valuesArray[] = $value;
//                     }

//                     // Collect loaded values in $sortFieldValues
//                     // Required for PHP 5.3 which translates references into values when source
//                     // variable is destroyed
//                     $sortFieldValues[$fieldName] = $valuesArray;
//                 }

//                 $sortArgs[] = &$sortFieldValues[$fieldName];
//             }

//             if ($count + 1 < count($argList)  &&  is_integer($argList[$count+1])) {
//                 $count++;
//                 $sortArgs[] = &$argList[$count];

//                 if ($count + 1 < count($argList)  &&  is_integer($argList[$count+1])) {
//                     $count++;
//                     $sortArgs[] = &$argList[$count];
//                 } else {
//                     if ($argList[$count] == SORT_ASC  || $argList[$count] == SORT_DESC) {
//                         $sortArgs[] = &$sortReg;
//                     } else {
//                         $sortArgs[] = &$sortAsc;
//                     }
//                 }
//             } else {
//                 $sortArgs[] = &$sortAsc;
//                 $sortArgs[] = &$sortReg;
//             }
//         }

//         // Sort by id's if values are equal
//         $sortArgs[] = &$ids;
//         $sortArgs[] = &$sortAsc;
//         $sortArgs[] = &$sortNum;

//         // Array to be sorted
//         $sortArgs[] = &$hits;

//         // Do sort
//         call_user_func_array('array_multisort', $sortArgs);
//     }

//     return $hits;
// }

func (index *Index) ResetTermsStream() {
    if index.termsStream == nil {
        index.termsStream = newTermStreamsPriorityQueue(index.SegmentInfos)
    } else {
        index.termsStream.ResetTermsStream()
    }
}

/**
 * Skip terms stream up to specified term preffix.
 *
 * Prefix contains fully specified field info and portion of searched term
 */
func (index *Index) SkipTo(prefix *index.Term) {
    index.termsStream.SkipTo(prefix)
}

/**
 * Scans terms dictionary and returns next term
 */
func (index *Index) NextTerm() *index.Term {
    return index.termsStream.NextTerm()
}

/**
 * Returns term in current position
 *
 * @return \ZendSearch\Lucene\Index\Term|null
 */
func (index *Index) CurrentTerm() *index.Term {
    return index.termsStream.CurrentTerm()
}

/**
 * Close terms stream
 *
 * Should be used for resources clean up if stream is not read up to the end
 */
func (index *Index) CloseTermsStream() {
    index.termsStream.CloseTermsStream()
    index.termsStream = nil
}
