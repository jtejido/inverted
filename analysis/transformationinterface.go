package transformation


type TransformationInterface interface {
	Transform(token []byte) []byte
}
