package tokenizer

import (
	"strings"
	. "bitbucket.org/jtejido/inverted/analysis/analyzer"
	"unicode"
	"unicode/utf8"
	"bufio"
	
)

type WhitespaceTokenizer struct {}

func NewWhitespaceTokenizer() *WhitespaceTokenizer {
	return new(WhitespaceTokenizer)
}

func (t WhitespaceTokenizer) Tokenize(text string) []*Token {
	tokens := make([]*Token, 0)
	scanner := bufio.NewScanner(strings.NewReader(text))
	scanner.Split(t.SplitToken)
	for scanner.Scan() {
		text := scanner.Text()
		tokens = append(tokens, NewToken(text))
	}

	return tokens

}

func (t WhitespaceTokenizer) SplitToken(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var class func(r rune) bool
	for width, i := 0, 0; i < len(data); i += width {
		var r rune
		r, width = utf8.DecodeRune(data[i:])

		if class == nil {
			switch {
			case unicode.IsSpace(r):
				class = unicode.IsSpace
			default:
				class = func(r rune) bool {
					return !unicode.IsSpace(r)
				}
			}
		}

		if !class(r) {
			return i, data[:i], nil
		}
	}

	if atEOF && len(data) > 0 {
		return len(data), data[:], nil
	}

	return 0, nil, nil
}


