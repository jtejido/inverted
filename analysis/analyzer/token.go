package token


type Token struct {
	TermText string
	PositionIncrement int
}

func NewToken(value string) *Token {
	return &Token{
		TermText: value,
		PositionIncrement: 1,
	}
}

func (token Token) GetTermText() string {
	return token.TermText
}

func (token Token) GetPositionIncrement() int {
	return token.PositionIncrement
}

func (token *Token) SetPositionIncrement(value int) {
	token.PositionIncrement = value
}

