package english

import (
	"bytes"

)

type English struct {}

// Normalizer for English, lowercase is just as fine
func NewEnglish() *English {
	return new(English)
}

func (english English) Normalize(b []byte) []byte {
	return bytes.ToLower(b)
}

func (english English) Transform(token []byte) []byte {
	return english.Normalize(token)
}