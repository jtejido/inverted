package transformation

type TransformationSet struct {
	transformers []TransformationInterface
}

func NewTransformationSet(transformers []TransformationInterface) *TransformationSet {
	tr := new(TransformationSet)
	tr.transformers = transformers
	return tr
}

func (tr *TransformationSet) Register(transformer []TransformationInterface) {
	for _, v := range transformer {
		tr.transformers = append(tr.transformers, v)
	}
}

func (tr TransformationSet) Transformers() []TransformationInterface {
	return tr.transformers
}

func (tr TransformationSet) Transform(token []byte) []byte {
	for _, v := range tr.transformers {
		a := v.Transform(token)
		token := make([]byte, len(a))
		copy(token, a)
	}

	return token
	
}
