package util

import (
    "bytes"
    "fmt"
    "io"
    "math"
    "sort"
    "strconv"
    "unicode/utf8"
)

const (
    VERSION_1                   = 1
    MAX_BUFFERED_DOCS           = 5
    MAX_MERGE_DOCS              = 100000
    MERGE_FACTOR                = 10
    GENERATION_RETRIEVE_COUNT   = 10
    RESULT_SET_LIMIT            = 0
    TERMS_PER_QUERY_LIMIT       = 1024
    FORMAT_SEGMENTS_SUB         = -1 // ^uint32(0)
    FORMAT_SEGMENTS_GEN_VER     = -2
    FORMAT_SEGMENTS_GEN_SUB_VER = -3
)

func FloatToByte315(f float32) int8 {
    bits := math.Float32bits(f)
    smallfloat := bits >> (24 - 3)
    if smallfloat <= ((63 - 15) << 3) {
        if bits <= 0 {
            return 0
        }
        return 1
    }
    if smallfloat >= ((63-15)<<3)+0x100 {
        return -1
    }
    return int8(smallfloat - ((63 - 15) << 3))
}

func Byte315ToFloat(b byte) float32 {
    // on Java1.5 & 1.6 JVMs, prebuilding a decoding array and doing a lookup
    // is only a little bit faster (anywhere from 0% to 7%)
    // However, is it faster for Go? TODO need benchmark
    if b == 0 {
        return 0
    }
    bits := uint32(b) << (24 - 3)
    bits += (63 - 15) << 24
    return math.Float32frombits(bits)
}

func SegmentFileName(name, suffix, ext string) string {
    if len(ext) > 0 || len(suffix) > 0 {
        // assert ext[0] != '.'
        var buffer bytes.Buffer
        buffer.WriteString(name)
        if len(suffix) > 0 {
            buffer.WriteString("_")
            buffer.WriteString(suffix)
        }
        if len(ext) > 0 {
            buffer.WriteString(".")
            buffer.WriteString(ext)
        }
        return buffer.String()
    }
    return name
}

func GetSegmentFileName(generation int64) string {
    if generation == 0 {
        return "segments"
    }

    str, err := BaseConvert(strconv.FormatInt(generation, 10), 10, 36)

    if err != nil {
        panic(err)
    }

    return "segments_" + str
}

func BaseConvert(number string, frombase, tobase int) (string, error) {
    i, err := strconv.ParseInt(number, frombase, 0)
    if err != nil {
        return "", err
    }
    return strconv.FormatInt(i, tobase), nil
}

func MapSort(m map[string]int32) map[string]int32 {

    n := map[int32][]string{}
    new_m := make(map[string]int32)
    var a []int
    for k, v := range m {
        n[v] = append(n[v], k)
    }
    for k := range n {
        a = append(a, int(k))
    }
    sort.Sort(sort.IntSlice(a))
    for _, k := range a {
        for _, s := range n[int32(k)] {
            new_m[s] = int32(k)
        }
    }
    return new_m
}

func MapMultisort(fieldNames map[int]string, fieldNums map[int]int) (map[int]string, map[int]int) {
    m := fieldNames
    n := map[string][]int{}
    a_fieldNames := make(map[int]string)
    a_fieldNums := make(map[int]int)
    var a []string
    for k, v := range m {
        n[v] = append(n[v], k)
    }
    for k := range n {
        a = append(a, k)
    }
    sort.Sort(sort.StringSlice(a))
    for _, k := range a {
        for _, s := range n[k] {
            a_fieldNames[s] = k
            a_fieldNums[s] = s
        }
    }

    return a_fieldNames, a_fieldNums
}

func MapFlip(m map[int]int) map[int]int {
    n := make(map[int]int)
    for i, v := range m {
        n[v] = i
    }
    return n
}

func GetPrefix(str string, length int) string {

    strb := []byte(str)
    prefixBytes := 0
    prefixChars := 0

    if len(strb) > prefixBytes {
        for prefixChars < length {

            charBytes := 1
            if (str[prefixBytes] & 0xC0) == 0xC0 {
                charBytes++
                if (str[prefixBytes] & 0x20) == 0x20 {
                    charBytes++
                    if (str[prefixBytes] & 0x10) == 0x10 {
                        charBytes++
                    }
                }
            }

            if len(strb) < (prefixBytes + charBytes - 1) {
                // wrong character
                break
            }
            prefixChars++
            prefixBytes += charBytes
        }
    }

    return string(str[:prefixBytes])
}

func GetLength(str string) int {
    var bytes, chars int
    for bytes < len(str) {
        charBytes := 1
        if (str[bytes] & 0xC0) == 0xC0 {
            charBytes++
            if (str[bytes] & 0x20) == 0x20 {
                charBytes++
                if (str[bytes] & 0x10) == 0x10 {
                    charBytes++
                }
            }
        }

        if bytes+charBytes > len(str) {
            // wrong character
            break
        }

        chars++
        bytes += charBytes
    }

    return chars
}

func assert(ok bool) {
    assert2(ok, "assert fail")
}

func assert2(ok bool, msg string, args ...interface{}) {
    if !ok {
        panic(fmt.Sprintf(msg, args...))
    }
}

func Fsync(fileToSync string, isDir bool) error {
    // TODO enable fsync, now just ignored
    return nil
}

func CloseWhileSuppressingError(objects ...io.Closer) {
    for _, object := range objects {
        if object == nil {
            continue
        }
        safeClose(object)
    }
}

func Close(objects ...io.Closer) error {
    var th error = nil

    for _, object := range objects {
        if object == nil {
            continue
        }
        t := safeClose(object)
        if t != nil {
            addSuppressed(th, t)
            if th == nil {
                th = t
            }
        }
    }

    return th
}

func safeClose(obj io.Closer) (err error) {
    return obj.Close()
}

func addSuppressed(err error, suppressed error) error {
    assert2(err != suppressed, "Self-suppression not permitted")
    if suppressed == nil {
        return err
    }
    if ce, ok := err.(*CompoundError); ok {
        ce.errs = append(ce.errs, suppressed)
        return ce
    }
    return &CompoundError{[]error{suppressed}}
}

type CompoundError struct {
    errs []error
}

func (e *CompoundError) Error() string {
    return e.errs[0].Error()
}

func Levenshtein(a, b string) int {
    if len(a) == 0 {
        return utf8.RuneCountInString(b)
    }

    if len(b) == 0 {
        return utf8.RuneCountInString(a)
    }

    if a == b {
        return 0
    }

    // We need to convert to []rune if the strings are non-ascii.
    // This could be avoided by using utf8.RuneCountInString
    // and then doing some juggling with rune indices.
    // The primary challenge is keeping track of the previous rune.
    // With a range loop, its not that easy. And with a for-loop
    // we need to keep track of the inter-rune width using utf8.DecodeRuneInString
    s1 := []rune(a)
    s2 := []rune(b)

    // swap to save some memory O(min(a,b)) instead of O(a)
    if len(s1) > len(s2) {
        s1, s2 = s2, s1
    }
    lenS1 := len(s1)
    lenS2 := len(s2)

    // init the row
    x := make([]int, lenS1+1)
    for i := 0; i < len(x); i++ {
        x[i] = i
    }

    // make a dummy bounds check to prevent the 2 bounds check down below.
    // The one inside the loop is particularly costly.
    _ = x[lenS1]
    // fill in the rest
    for i := 1; i <= lenS2; i++ {
        prev := i
        var current int
        for j := 1; j <= lenS1; j++ {
            if s2[i-1] == s1[j-1] {
                current = x[j-1] // match
            } else {
                current = min(min(x[j-1]+1, prev+1), x[j]+1)
            }
            x[j-1] = prev
            prev = current
        }
        x[lenS1] = prev
    }
    return x[lenS1]
}

func min(a, b int) int {
    if a < b {
        return a
    }
    return b
}
