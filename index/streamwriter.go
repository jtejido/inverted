package index

import (
    "bitbucket.org/jtejido/inverted/store"
)

type StreamWriter struct {
    AbstractWriter
}

func NewStreamWriter(directory store.Directory, name string) *StreamWriter {
    return &StreamWriter{
        AbstractWriter: AbstractWriter{
            Directory: directory,
            Name:      name,
            DocCount:  0,
            Fields:    make(map[string]*FieldInfo),
            FdxFile:   nil,
            FdtFile:   nil,
            FrqFile:   nil,
            PrxFile:   nil,
            TiiFile:   nil,
            TisFile:   nil,
            Files:     make([]string, 0),
        },
    }
}

func (sw *StreamWriter) Close() *SegmentInfo {
    if sw.DocCount == 0 {
        return nil
    }

    sw.dumpFNM()
    sw.generateCFS()

    return NewSegmentInfo(sw.Directory, sw.Name, int32(sw.DocCount), -1, nil, false, true)
}
