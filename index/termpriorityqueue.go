package index

type TermsPriorityQueue struct {
	heap 			  map[int]*SegmentInfo
}

func NewTermsPriorityQueue() *TermsPriorityQueue {
	return &TermsPriorityQueue{
		heap: make(map[int]*SegmentInfo),
	}
}

func (tpq *TermsPriorityQueue) Put(element *SegmentInfo) {

    var nodeId = len(tpq.heap)
    var parentId = int((nodeId-1) >> 1)   // floor( ($nodeId-1)/2 )

    for nodeId != 0  &&  tpq.Less(element, tpq.heap[parentId]) {
        // Move parent node down
        tpq.heap[nodeId] = tpq.heap[parentId]

        // Move pointer to the next level of tree
        nodeId   = parentId
        parentId = int((nodeId-1) >> 1)   // floor( ($nodeId-1)/2 )
    }

    // Put new node into the tree

    tpq.heap[nodeId] = element
}

func (tpq *TermsPriorityQueue) Less(termsStream1, termsStream2 *SegmentInfo) bool {
    var val int
	if termsStream1.CurrentTerm().Key() < termsStream2.CurrentTerm().Key() {
        val = -1
    } else if termsStream1.CurrentTerm().Key() > termsStream2.CurrentTerm().Key() {
        val = 1
    } else {
        val = 0
    }

    return val < 0
}

func (tpq *TermsPriorityQueue) Pop() *SegmentInfo {
    if len(tpq.heap) == 0 {
        return nil
    }

    var top = tpq.heap[0]
    var lastId = len(tpq.heap) - 1

    /**
     * Find appropriate position for last node
     */
    var nodeId  = 0     // Start from a top
    var childId = 1     // First child

    // Choose smaller child
    if (lastId > 2  &&  tpq.Less(tpq.heap[2], tpq.heap[1])) {
        childId = 2
    }

    for childId < lastId  && tpq.Less(tpq.heap[childId], tpq.heap[lastId]) {
        // Move child node up
        tpq.heap[nodeId] = tpq.heap[childId]

        nodeId  = childId               // Go down
        childId = (nodeId << 1) + 1     // First child

        // Choose smaller child
        if (childId+1) < lastId  && tpq.Less(tpq.heap[childId+1], tpq.heap[childId]) {
            childId++
        }
    }

    // Move last element to the new position
    tpq.heap[nodeId] = tpq.heap[lastId]
    delete(tpq.heap, lastId)
    return top
}

func (tpq *TermsPriorityQueue) Top() *SegmentInfo {

    if len(tpq.heap) == 0 {
        return nil
    }

    return tpq.heap[0]
}
