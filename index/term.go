package index

type Term struct {
	Text  string
	Field string
}

func NewTerm(text string, field string) *Term {
	return &Term{
		Text:  text,
		Field: field,
	}
}

func (term *Term) Key() string {
	return term.Field + " " + term.Text
}
