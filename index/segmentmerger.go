package index

import (
    . "bitbucket.org/jtejido/inverted/document/field"
    "bitbucket.org/jtejido/inverted/store"
    "sort"
    // "fmt"
)

type SegmentMerger struct {
    Writer       *StreamWriter
    SegmentInfos []*SegmentInfo
    MergeDone    bool
    FieldsMap    map[string]map[int]int
    DocCount     int
}

func NewSegmentMerger(directory store.Directory, name string) *SegmentMerger {
    return &SegmentMerger{
        Writer:       NewStreamWriter(directory, name),
        SegmentInfos: make([]*SegmentInfo, 0),
        FieldsMap:    make(map[string]map[int]int),
        MergeDone:    false,
    }
}

func (sm *SegmentMerger) AddSource(segmentInfo *SegmentInfo) {
    sm.SegmentInfos = append(sm.SegmentInfos, segmentInfo)
}

func (sm *SegmentMerger) Merge() *SegmentInfo {

    if sm.MergeDone == true {
        panic("Merge is already done.")
    }

    if len(sm.SegmentInfos) < 1 {
        panic("Wrong number of segments to be merged.")
        return nil
    }

    sm.MergeFields()
    sm.MergeStoredFields()
    sm.MergeTerms()

    sm.MergeDone = true

    return sm.Writer.Close()
}

func (sm *SegmentMerger) MergeFields() {
    for _, segmentInfo := range sm.SegmentInfos {
        if sm.FieldsMap[segmentInfo.GetName()] == nil {
            sm.FieldsMap[segmentInfo.GetName()] = make(map[int]int)
        }

        infos := segmentInfo.GetFieldInfos()
        var keys = make([]int, 0, len(infos))

        for k, _ := range infos {
            keys = append(keys, k)
        }

        sort.Ints(keys)

        for _, key := range keys {
            for k, fieldInfo := range segmentInfo.GetFieldInfos() {
                if k == key {
                    sm.FieldsMap[segmentInfo.GetName()][fieldInfo.DocCount] = sm.Writer.AddFieldInfo(fieldInfo)
                    break
                }
            }
        }

    }
}

func (sm *SegmentMerger) MergeStoredFields() {
    sm.DocCount = 0

    for _, segmentInfo := range sm.SegmentInfos {
        fdtFile := segmentInfo.OpenCompoundFile(".fdt")

        for count := 0; int32(count) < segmentInfo.Count(); count++ {
            fieldCount, _ := fdtFile.ReadVInt()
            storedFields := make([]*Field, 0)

            for count2 := 0; int32(count2) < fieldCount; count2++ {
                fieldNum, _ := fdtFile.ReadVInt()
                bits, _ := fdtFile.ReadByte()
                fieldInfo := segmentInfo.GetField(int(fieldNum))

                if (bits & 2) == 0 {
                    b := (bits & 1) != 0
                    str, _ := fdtFile.ReadString()
                    storedFields = append(storedFields, NewField(fieldInfo.Name, str, true, fieldInfo.IsIndexed, b))
                }
            }

            if segmentInfo.IsDeleted(count) == false {
                sm.DocCount++
                sm.Writer.AddStoredFields(storedFields)
            }
        }
    }
}

func (sm *SegmentMerger) MergeTerms() {
    segmentInfoQueue := NewTermsPriorityQueue()

    var segmentStartId = 0
    for _, segmentInfo := range sm.SegmentInfos {

        args := []int{segmentStartId, SM_MERGE_INFO}
        segmentStartId = segmentInfo.ResetTermsStream(args...)

        // Skip "empty" segments
        if segmentInfo.CurrentTerm() != nil {
            segmentInfoQueue.Put(segmentInfo)
        }
    }

    sm.Writer.initializeDictionaryFiles()

    var termDocs = make(map[int][]int)

    for {
        si := segmentInfoQueue.Pop()
        // If i equals 3, set "valid" to false.

        if si == nil {
            break
        }

        for k, v := range si.CurrentTermPositions() {
            termDocs[k] = v
        }

        if segmentInfoQueue.Top() == nil || segmentInfoQueue.Top().CurrentTerm().Key() != si.CurrentTerm().Key() {

            // Add term if it's contained in any document
            if len(termDocs) > 0 {

                sm.Writer.AddTerm(si.CurrentTerm(), termDocs)
            }
            termDocs = make(map[int][]int)
        }

        si.NextTerm()
        // check, if segment dictionary is finished
        if si.CurrentTerm() != nil {
            // Put segment back into the priority queue
            segmentInfoQueue.Put(si)
        }

    }

    sm.Writer.closeDictionaryFiles()
}
