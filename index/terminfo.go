package index

type TermInfo struct {
	DocFreq      int64
	FreqPointer  int64
	ProxPointer  int64
	SkipOffset   int64
	IndexPointer int64
}

func NewTermInfo(docFreq, freqPointer, proxPointer, skipOffset, indexPointer int64) *TermInfo {
	return &TermInfo{
		DocFreq:      docFreq,
		FreqPointer:  freqPointer,
		ProxPointer:  proxPointer,
		SkipOffset:   skipOffset,
		IndexPointer: indexPointer,
	}
}

func (ti *TermInfo) Clone() *TermInfo {
	return &TermInfo{ti.DocFreq, ti.FreqPointer, ti.ProxPointer, ti.SkipOffset, ti.IndexPointer}
}
