package index

import (
    "bitbucket.org/jtejido/inverted/store"
    . "bitbucket.org/jtejido/inverted/util"
    "encoding/binary"
    "fmt"
    "math"
    "strconv"
    "strings"
)

var _termInfoCache = map[string]*TermInfo{}

type SegmentInfo struct {
    directory             store.Directory
    name                  string
    docCount              int32
    termDictionary        []*DictionaryEntry
    termDictionaryInfos   []*TermInfo
    hasSingleNormFile     bool
    isCompound            bool
    norms                 [][]byte
    delGen                int64
    fieldsDicPositions    map[int]int
    segFiles              []*CfsFiles
    segFileSizes          map[string]int64
    sharedDocStoreOptions *DocStoreOptions
    usesSharedDocStore    bool
    fields                map[int]*FieldInfo
    deleteddirty          bool
    deleted               map[int]int
    lastTermPositions     map[int][]int
    lastTerm              *Term
    lastTermInfo          *TermInfo
    tisFileOffset         int64
    tisFile               store.IndexInput
    frqFile               store.IndexInput
    frqFileOffset         int64
    prxFile               store.IndexInput
    prxFileOffset         int64
    termNum               int64
    docMap                map[int]int
    termsScanMode         int
    termCount             int64
    skipInterval          int32
    indexInterval         int32
}

const (
    SM_TERMS_ONLY = 0
    SM_FULL_INFO  = 1
    SM_MERGE_INFO = 2
    // Field flags
    FI_IS_INDEXED       = 0x1
    FI_STORE_TERMVECTOR = 0x2
    FI_NORMS_OMITTED    = 0x10
    FI_STORE_PAYLOADS   = 0x20
)

type CfxFiles struct {
    filename string
    offset   int64
}

type CfsFiles struct {
    filename string
    offset   int64
}

type DocStoreOptions struct {
    offset     int32
    segment    string
    isCompound bool
    files      []*CfxFiles
    fileSizes  map[string]int64
}

func NewSegmentInfo(directory store.Directory, name string, docCount int32, delGen int64, docStoreOptions *DocStoreOptions, hasSingleNormFile, isCompound bool) *SegmentInfo {

    si := new(SegmentInfo)
    si.directory = directory
    si.name = name
    si.docCount = docCount
    si.delGen = delGen
    si.hasSingleNormFile = hasSingleNormFile
    si.isCompound = isCompound
    si.termDictionary = nil
    si.deleteddirty = false
    si.deleted = make(map[int]int)
    si.lastTermPositions = make(map[int][]int)
    si.lastTerm = nil
    si.lastTermInfo = nil
    si.tisFileOffset = 0
    si.tisFile = nil
    si.frqFile = nil
    si.frqFileOffset = 0
    si.prxFile = nil
    si.prxFileOffset = 0
    si.termNum = 0
    si.termCount = 0

    var dataOffset int64

    if docStoreOptions != nil {
        si.usesSharedDocStore = true
        si.sharedDocStoreOptions = docStoreOptions

        if docStoreOptions.isCompound {
            cfxFile, _ := si.directory.OpenInput(docStoreOptions.segment + ".cfx")
            cfxFilesCount, _ := cfxFile.ReadVInt()

            cfxFiles := make([]*CfxFiles, cfxFilesCount)
            cfxFileSizes := make(map[string]int64)

            var fileName string

            var count int

            for count = 0; count < int(cfxFilesCount); count++ {
                dataOffset, _ := cfxFile.ReadLong()
                if count != 0 {
                    cfxFileSizes[fileName] = dataOffset - cfxFiles[len(cfxFiles)-1].offset
                }

                fileName, _ = cfxFile.ReadString()
                cfxFiles[count] = &CfxFiles{filename: fileName, offset: dataOffset}
            }

            if count != 0 {
                cfxFileSizes[fileName] = cfxFile.Length() - dataOffset
            }

            si.sharedDocStoreOptions.files = cfxFiles
            si.sharedDocStoreOptions.fileSizes = cfxFileSizes
        }
    }

    si.segFiles = make([]*CfsFiles, 0)
    si.segFileSizes = make(map[string]int64)
    if si.isCompound {
        cfsFile, _ := si.directory.OpenInput(name + ".cfs")
        segFilesCount, _ := cfsFile.ReadVInt()
        var fileName string
        var count int
        for count = 0; count < int(segFilesCount); count++ {
            dataOffset, _ = cfsFile.ReadLong()
            if count != 0 {
                si.segFileSizes[fileName] = dataOffset - si.segFiles[len(si.segFiles)-1].offset
            }
            fileName, _ = cfsFile.ReadString()
            si.segFiles = append(si.segFiles, &CfsFiles{filename: fileName, offset: dataOffset})
        }
        if count != 0 {
            si.segFileSizes[fileName] = cfsFile.Length() - dataOffset
        }
    }

    fnmFile := si.OpenCompoundFile(".fnm")
    fieldsCount, _ := fnmFile.ReadVInt()
    fieldNames := make(map[int]string)
    fieldNums := make(map[int]int)
    si.fields = make(map[int]*FieldInfo)

    for count := 0; count < int(fieldsCount); count++ {
        fieldName, _ := fnmFile.ReadString()
        fieldBits, _ := fnmFile.ReadByte()
        isIndexed := (fieldBits & FI_IS_INDEXED) != 0
        storeTermVector := (fieldBits & FI_STORE_TERMVECTOR) != 0
        storePayloads := (fieldBits & FI_STORE_PAYLOADS) != 0
        normsOmitted := (fieldBits & FI_NORMS_OMITTED) != 0
        si.fields[count] = NewFieldInfo(fieldName, isIndexed, count, storeTermVector, normsOmitted, storePayloads)

        // if normsOmitted {
        //         b := []byte{}
        //         for i:=0; i<int(docCount); i++ {
        //             b = append(b, byte(si.similarity.EncodeNorm(1.0)))
        //         }
        //         si.norms[count] = b
        //     }

        fieldNums[count] = count
        fieldNames[count] = fieldName
    }
    _, a_fieldNums := MapMultisort(fieldNames, fieldNums)
    si.fieldsDicPositions = MapFlip(a_fieldNums)

    if si.delGen == -2 {
        si.delGen = si.DetectLatestDelGen()
    }

    si.deleted = si.LoadDelFile()

    return si

}

func (si *SegmentInfo) ResetTermsStream(args ...int) int {
    /**
     * SegmentInfo->resetTermsStream() method actually takes two optional parameters:
     *   $startId (default value is 0)
     *   $mode (default value is self::SM_TERMS_ONLY)
     */
    var mode, startId int
    argList := args
    if len(argList) > 2 {
        panic("Wrong number of arguments")
    } else if len(argList) == 2 {
        startId = argList[0]
        mode = argList[1]
    } else if len(argList) == 1 {
        startId = argList[0]
        mode = SM_TERMS_ONLY
    } else {
        startId = 0
        mode = SM_TERMS_ONLY
    }

    if si.tisFile != nil {
        si.tisFile = nil
    }

    si.tisFile = si.OpenCompoundFile(".tis")
    si.tisFileOffset = si.tisFile.FilePointer()

    tiVersion, _ := si.tisFile.ReadInt()
    if tiVersion != FORMAT_SEGMENTS_GEN_SUB_VER {
        panic("Wrong TermInfoFile file format")
    }
    tc, _ := si.tisFile.ReadLong()

    si.termCount = tc // Read terms count
    si.termNum = tc

    si.indexInterval, _ = si.tisFile.ReadInt() // Read Index interval
    si.skipInterval, _ = si.tisFile.ReadInt()  // Read skip interval
    if tiVersion == FORMAT_SEGMENTS_GEN_SUB_VER {
        si.tisFile.ReadInt()
    }

    if si.frqFile != nil {
        si.frqFile = nil
    }
    if si.prxFile != nil {
        si.prxFile = nil
    }
    si.docMap = make(map[int]int)

    si.lastTerm = NewTerm("", strconv.Itoa(-1))
    si.lastTermInfo = NewTermInfo(0, 0, 0, 0, 0)
    si.lastTermPositions = nil

    si.termsScanMode = mode

    switch mode {
    case SM_TERMS_ONLY:
        // Do nothing
        break

    case SM_FULL_INFO:
        // break intentionally omitted
    case SM_MERGE_INFO:
        si.frqFile = si.OpenCompoundFile(".frq")
        si.frqFileOffset = si.frqFile.FilePointer()

        si.prxFile = si.OpenCompoundFile(".prx")
        si.prxFileOffset = si.prxFile.FilePointer()

        for count := 0; int32(count) < si.docCount; count++ {

            if si.IsDeleted(count) == false {
                var i int
                if mode == SM_MERGE_INFO {
                    i = len(si.docMap)

                } else {
                    i = count
                }

                si.docMap[count] = startId + i

            }
        }
        break

    default:
        fmt.Errorf("Wrong terms scaning mode specified.")
        break
    }

    var j int
    // Calculate next segment start id (since $this->_docMap structure may be cleaned by $this->nextTerm() call)
    if mode == SM_MERGE_INFO {
        j = len(si.docMap)
    } else {
        j = int(si.docCount)
    }
    nextSegmentStartId := startId + j

    si.NextTerm()

    return nextSegmentStartId
}

func (si *SegmentInfo) NextTerm() *Term {
    if si.tisFile == nil || si.termCount == 0 {
        si.lastTerm = nil
        si.lastTermInfo = nil
        si.lastTermPositions = nil
        si.docMap = nil

        // may be necessary for "empty" segment
        si.tisFile = nil
        si.frqFile = nil
        si.prxFile = nil

        return nil
    }

    termPrefixLength, _ := si.tisFile.ReadVInt()
    termSuffix, _ := si.tisFile.ReadString()
    termFieldNum, _ := si.tisFile.ReadVInt()

    termValue := GetPrefix(si.lastTerm.Text, int(termPrefixLength)) + termSuffix

    si.lastTerm = NewTerm(termValue, si.fields[int(termFieldNum)].Name)

    docFreq, _ := si.tisFile.ReadVInt()

    a, _ := si.tisFile.ReadVInt()
    freqPointer := si.lastTermInfo.FreqPointer + int64(a)
    b, _ := si.tisFile.ReadVInt()
    proxPointer := si.lastTermInfo.ProxPointer + int64(b)
    var skipOffset int32
    if docFreq >= si.skipInterval {
        skipOffset, _ = si.tisFile.ReadVInt()
    } else {
        skipOffset = 0
    }

    si.lastTermInfo = NewTermInfo(int64(docFreq), freqPointer, proxPointer, int64(skipOffset), 0)

    if si.termsScanMode == SM_FULL_INFO || si.termsScanMode == SM_MERGE_INFO {
        si.lastTermPositions = make(map[int][]int)
        si.frqFile.Seek(si.lastTermInfo.FreqPointer+si.frqFileOffset, 0)
        freqs := make(map[float64]int32)
        var docId float64 = 0
        for count := 0; int64(count) < si.lastTermInfo.DocFreq; count++ {
            docDelta, _ := si.frqFile.ReadVInt()

            if docDelta%2 == 1 {
                docId += float64((docDelta - 1) / 2)
                freqs[docId] = 1
            } else {
                docId += float64(docDelta / 2)
                freqs[docId], _ = si.frqFile.ReadVInt()
            }
        }

        si.prxFile.Seek(si.lastTermInfo.ProxPointer+si.prxFileOffset, 0)

        for docId, freq := range freqs {
            var termPosition int = 0
            var positions = make([]int, 0)

            for count := 0; int32(count) < freq; count++ {
                a, _ := si.prxFile.ReadVInt()

                termPosition += int(a)
                positions = append(positions, termPosition)
            }

            _, ok := si.docMap[int(docId)]
            if ok {
                si.lastTermPositions[si.docMap[int(docId)]] = positions
            }
        }
    }

    si.termCount--
    if si.termCount == 0 {
        si.tisFile = nil
        si.frqFile = nil
        si.prxFile = nil
    }

    return si.lastTerm
}

func (si *SegmentInfo) SkipTo(prefix *Term) {
    if si.termDictionary == nil {
        si.loadDictionaryIndex()
    }

    searchField := si.GetFieldNum(prefix.Field)

    if searchField == -1 {
        /**
         * Field is not presented in this segment
         * Go to the end of dictionary
         */
        si.tisFile = nil
        si.frqFile = nil
        si.prxFile = nil

        si.lastTerm = nil
        si.lastTermInfo = nil
        si.lastTermPositions = nil

        return
    }
    searchDicField := si.GetFieldPosition(searchField)

    // search for appropriate value in dictionary
    lowIndex := 0
    highIndex := len(si.termDictionary) - 1
    for highIndex >= lowIndex {
        // $mid = ($highIndex - $lowIndex)/2;
        mid := (highIndex + lowIndex) >> 1
        midTerm := si.termDictionary[mid]

        fieldNum := si.GetFieldPosition(int(midTerm.fieldNum))
        delta := searchDicField - fieldNum
        if delta == 0 {
            delta = strings.Compare(prefix.Text, midTerm.term)
        }

        if delta < 0 {
            highIndex = mid - 1
        } else if delta > 0 {
            lowIndex = mid + 1
        } else {
            // We have reached term we are looking for
            break
        }
    }

    if highIndex == -1 {
        // Term is out of the dictionary range
        si.tisFile = nil
        si.frqFile = nil
        si.prxFile = nil

        si.lastTerm = nil
        si.lastTermInfo = nil
        si.lastTermPositions = nil

        return
    }

    prevPosition := highIndex
    prevTerm := si.termDictionary[prevPosition]
    prevTermInfo := si.termDictionaryInfos[prevPosition]

    if si.tisFile == nil {
        // The end of terms stream is reached and terms dictionary file is closed
        // Perform mini-reset operation
        si.tisFile = si.OpenCompoundFile(".tis")

        if si.termsScanMode == SM_FULL_INFO || si.termsScanMode == SM_MERGE_INFO {
            si.frqFile = si.OpenCompoundFile(".frq")
            si.prxFile = si.OpenCompoundFile(".prx")
        }
    }
    si.tisFile.Seek(si.tisFileOffset+prevTermInfo.IndexPointer, 0)
    var f string
    if prevTerm.fieldNum != -1 {
        t := int(prevTerm.fieldNum)
        f = si.fields[t].Name
    }
    si.lastTerm = NewTerm(prevTerm.term, f)
    si.lastTermInfo = prevTermInfo.Clone()
    si.lastTermInfo.IndexPointer = 0

    si.termCount = int64(si.termNum) - int64(prevPosition)*int64(si.indexInterval)

    if highIndex == 0 {
        // skip start entry
        si.NextTerm()
    } else if prefix.Field == si.lastTerm.Field && prefix.Text == si.lastTerm.Text {
        // We got exact match in the dictionary index

        if si.termsScanMode == SM_FULL_INFO || si.termsScanMode == SM_MERGE_INFO {
            si.lastTermPositions = make(map[int][]int)

            si.frqFile.Seek(si.lastTermInfo.FreqPointer+si.frqFileOffset, 0)
            freqs := make(map[int32]int32)
            var docId int32
            for count := 0; int64(count) < si.lastTermInfo.DocFreq; count++ {
                docDelta, _ := si.frqFile.ReadVInt()
                if docDelta%2 == 1 {
                    docId += (docDelta - 1) / 2
                    freqs[docId] = 1
                } else {
                    docId += docDelta / 2
                    t, _ := si.frqFile.ReadVInt()
                    freqs[docId] = t
                }
            }

            si.prxFile.Seek(si.lastTermInfo.ProxPointer+si.prxFileOffset, 0)
            for docId, freq := range freqs {
                var termPosition int32
                positions := make([]int, freq)

                for count := 0; int32(count) < freq; count++ {
                    t, _ := si.prxFile.ReadVInt()
                    termPosition += t
                    positions[count] = int(termPosition)
                }

                t := int(docId)
                if i, ok := si.docMap[t]; ok {
                    si.lastTermPositions[i] = positions
                }
            }
        }

        return
    }

    // Search term matching specified prefix
    for si.lastTerm != nil {
        if strings.Compare(si.lastTerm.Field, prefix.Field) > 0 || (prefix.Field == si.lastTerm.Field && strings.Compare(si.lastTerm.Text, prefix.Text) >= 0) {
            // Current term matches or greate than the pattern
            return
        }

        si.NextTerm()
    }
}

func (si *SegmentInfo) LoadDelFile() map[int]int {
    if si.delGen == -1 {
        // There is no delete file for this segment
        return nil
    } else {
        // It's 2.1+ format deleteions file
        return si.Load21DelFile()
    }
}

func (si *SegmentInfo) Load21DelFile() map[int]int {
    var buf [8]byte
    var delBytes []byte
    str, _ := BaseConvert(strconv.FormatInt(si.delGen, 10), 10, 36)
    delFile, _ := si.directory.OpenInput(si.name + "_" + str + ".del")

    format, _ := delFile.ReadInt()

    // format is actually byte count
    byteCount := math.Ceil(float64(format / 8))
    binary.BigEndian.PutUint64(buf[:], math.Float64bits(byteCount))
    bitCount, _ := delFile.ReadInt()

    if bitCount == 0 {
        delBytes = make([]byte, 0)
    } else {
        delBytes, _ = delFile.ReadBytes(buf[:])
    }

    deletions := make(map[int]int)
    for count := 0; count < int(byteCount); count++ {
        for bit := 0; bit < 8; bit++ {
            if delBytes[count]&1<<uint(bit) != 0 {
                deletions[count*8+bit] = 1
            }
        }
    }

    if len(deletions) > 0 {
        return nil
    } else {
        return deletions
    }

}

func (si *SegmentInfo) OpenCompoundFile(extension string) store.IndexInput {
    var fdtFile store.IndexInput
    if (extension == ".fdx" || extension == ".fdt") && si.usesSharedDocStore == true {

        fdxFName := si.sharedDocStoreOptions.segment + ".fdx"
        fdtFName := si.sharedDocStoreOptions.segment + ".fdt"

        fdxFile, _ := si.directory.OpenInput(fdxFName)
        fdxFile.Seek(int64(si.sharedDocStoreOptions.offset)*8, 1)

        if extension == ".fdx" {
            return fdxFile
        } else {
            fdtStartOffset, _ := fdxFile.ReadLong()
            fdtFile, _ = si.directory.OpenInput(fdtFName)
            fdtFile.Seek(fdtStartOffset, 1)
            return fdtFile
        }

        var ok_a, ok_b bool

        for _, f := range si.sharedDocStoreOptions.files {
            if f.filename == fdxFName {
                ok_a = true
            }

            if f.filename == fdtFName {
                ok_b = true
            }
        }

        if !ok_a {
            panic(fmt.Sprintf("Shared doc storage segment compound file doesn't contain %s file", fdxFName))
        }

        if !ok_b {
            panic(fmt.Sprintf("Shared doc storage segment compound file doesn't contain %s file", fdtFName))
        }

        // Open shared docstore segment file
        cfxFile, _ := si.directory.OpenInput(si.sharedDocStoreOptions.segment + ".cfx")
        // Seek to the start of '.fdx' file within compound file
        for _, sdsoFile := range si.sharedDocStoreOptions.files {
            if sdsoFile.filename == fdxFName {
                cfxFile.Seek(sdsoFile.offset, 0)
                break
            }
        }

        // Seek to the start of current segment documents section
        cfxFile.Seek(int64(si.sharedDocStoreOptions.offset)*8, 1)

        if extension == ".fdx" {
            return cfxFile
        } else {

            fdtStartOffset, _ := cfxFile.ReadLong()

            for _, sdsoFile := range si.sharedDocStoreOptions.files {
                if sdsoFile.filename == fdtFName {
                    cfxFile.Seek(sdsoFile.offset, 0)
                    break
                }
            }

            cfxFile.Seek(fdtStartOffset, 1)

            return fdtFile
        }
    }

    filename := si.name + extension
    if !si.isCompound {
        file, _ := si.directory.OpenInput(filename)
        return file
    }

    for _, segFile := range si.segFiles {
        if segFile.filename == filename {
            file, _ := si.directory.OpenInput(si.name + ".cfs")
            file.Seek(segFile.offset, 1)
            return file
        }
    }

    fmt.Errorf("Segment compound file doesn't contain %s file.", filename)
    return nil
}

func (si *SegmentInfo) compoundFileLength(extension string) int64 {
    if (extension == ".fdx" || extension == ".fdt") && si.usesSharedDocStore {
        filename := si.sharedDocStoreOptions.segment + extension

        if !si.sharedDocStoreOptions.isCompound {
            len, _ := si.directory.FileLength(filename)
            return len
        }

        if size, ok := si.sharedDocStoreOptions.fileSizes[filename]; ok {
            return size
        }

        panic(fmt.Sprintf("Shared doc store compound file doesn't contain %s file.", filename))

    }

    filename := si.name + extension

    // Try to get common file first
    if si.directory.FileExists(filename) {
        len, _ := si.directory.FileLength(filename)
        return len
    }

    if _, ok := si.segFileSizes[filename]; !ok {
        panic(fmt.Sprintf("Index compound file doesn't contain %s file.", filename))
    }

    return si.segFileSizes[filename]
}

func (si *SegmentInfo) DetectLatestDelGen() int64 {
    delFileList := make([]int64, 0)
    files, err := si.directory.ListAll()

    if err != nil {
        panic(err)
    }

    for _, file := range files {
        if file == si.name+".del" {
            // Matches <segment_name>.del file name
            delFileList = append(delFileList, 0)
        }
    }

    if len(delFileList) == 0 {
        return -1
    } else {
        max := int64(0)
        for _, v := range delFileList {
            if v >= max {
                max = v
            }
        }
        return max
    }
}

func (si *SegmentInfo) GetName() string {
    return si.name
}

func (si *SegmentInfo) Fields(indexed bool) []string {
    result := make([]string, 0)
    for _, field := range si.fields {
        if indexed == false || field.IsIndexed {
            result = append(result, field.Name)
        }
    }
    return result
}

func (si *SegmentInfo) GetFieldNum(fieldName string) int {
    for _, field := range si.fields {
        if field.Name == fieldName {
            return field.DocCount
        }
    }

    return -1
}

func (si *SegmentInfo) Count() int32 {
    return si.docCount
}

func (si *SegmentInfo) GetDelGen() int64 {
    return si.delGen
}

func (si *SegmentInfo) GetFieldInfos() map[int]*FieldInfo {
    return si.fields
}

func (si *SegmentInfo) CurrentTerm() *Term {
    return si.lastTerm
}

func (si *SegmentInfo) CurrentTermPositions() map[int][]int {
    return si.lastTermPositions
}

func (si *SegmentInfo) GetField(fieldNum int) *FieldInfo {
    return si.fields[fieldNum]
}

func (si *SegmentInfo) GetFieldPosition(fieldNum int) int {
    // Treat values which are not in a translation table as a 'direct value'
    if item, ok := si.fieldsDicPositions[fieldNum]; ok {
        return item
    }

    return fieldNum

}

func (si *SegmentInfo) HasDeletions() bool {
    return len(si.deleted) > 0
}

func (si *SegmentInfo) WriteChanges() {
    // Get new generation number
    latestDelGen := si.DetectLatestDelGen()

    if si.deleteddirty == false {
        // There was no deletions by current process

        if latestDelGen == si.delGen {
            // Delete file hasn't been updated by any concurrent process
            return
        } else if latestDelGen > si.delGen {
            // Delete file has been updated by some concurrent process
            // Reload deletions file
            si.delGen = latestDelGen
            si.deleted = si.LoadDelFile()

            return
        } else {
            panic("Delete file processing workflow is corrupted for the segment.")
            return
        }
    }

    if latestDelGen > si.delGen {
        // Merge current deletions with latest deletions file
        si.delGen = latestDelGen

        latestDelete := si.LoadDelFile()

        for k, v := range latestDelete {
            si.deleted[k] = v
        }
    }

    byteCount := int(math.Floor(float64(si.docCount/8)) + 1)
    delBytes := make([]byte, 8)
    for count := 0; count < byteCount; count++ {
        b := uint32(0)
        for bit := 0; bit < 8; bit++ {
            _, ok := si.deleted[count*8+bit]
            if ok {
                b |= (1 << uint(bit))
                binary.BigEndian.PutUint32(delBytes, b)
            }
        }
    }

    bitCount := len(si.deleted)

    if si.delGen == -1 {
        // Set delete file generation number to 1
        si.delGen = 1
    } else {
        // Increase delete file generation number by 1
        si.delGen++
    }
    str, _ := BaseConvert(strconv.FormatInt(si.delGen, 10), 10, 36)
    delFile, _ := si.directory.CreateOutput(si.name + "_" + str + ".del")
    delFile.WriteInt(si.docCount)
    delFile.WriteInt(int32(bitCount))
    delFile.WriteBytes(delBytes)

    si.deleteddirty = false
}

func (si *SegmentInfo) IsDeleted(id int) bool {
    if len(si.deleted) == 0 {
        return false
    }

    _, ok := si.deleted[id]
    return ok
}

func (si *SegmentInfo) loadDictionaryIndex() {
    var dictionaryLoader DictionaryLoader
    // Check, if index is already serialized
    // if (si.directory->fileExists($this->_name . '.sti')) {
    //     // Load serialized dictionary index data
    //     $stiFile = $this->_directory->getFileObject($this->_name . '.sti');
    //     $stiFileData = $stiFile->readBytes($this->_directory->fileLength($this->_name . '.sti'));

    //     // Load dictionary index data
    //     if (($unserializedData = @unserialize($stiFileData)) !== false) {
    //         list($this->_termDictionary, $this->_termDictionaryInfos) = $unserializedData;
    //         return;
    //     }
    // }

    // Load data from .tii file and generate .sti file

    // Prefetch dictionary index data
    tiiFile := si.OpenCompoundFile(".tii")
    buf := make([]byte, int(si.compoundFileLength(".tii")))
    tiiFileData, _ := tiiFile.ReadBytes(buf)

    // Load dictionary index data
    si.termDictionary, si.termDictionaryInfos = dictionaryLoader.load(tiiFileData)

    // stiFileData = serialize(array($this->_termDictionary, $this->_termDictionaryInfos));
    // stiFile = $this->_directory->createFile($this->_name . '.sti');
    // stiFile->writeBytes($stiFileData);
}

func (si *SegmentInfo) TermInfo(term *Term) *TermInfo {
    termKey := term.Key()

    var termInfo *TermInfo

    if ti, ok := _termInfoCache[termKey]; ok {
        // Move termInfo to the end of cache
        _termInfoCache[termKey] = ti

        return termInfo
    }

    if si.termDictionary == nil {
        si.loadDictionaryIndex()
    }

    searchField := si.GetFieldNum(term.Field)

    if searchField == -1 {
        return nil
    }

    searchDicField := si.GetFieldPosition(searchField)

    // search for appropriate value in dictionary
    lowIndex := 0
    highIndex := len(si.termDictionary) - 1
    for highIndex >= lowIndex {
        // $mid = ($highIndex - $lowIndex)/2;
        mid := (highIndex + lowIndex) >> 1
        midTerm := si.termDictionary[mid]

        fieldNum := si.GetFieldPosition(int(midTerm.fieldNum))
        delta := searchDicField - fieldNum
        if delta == 0 {
            delta = strings.Compare(term.Text, midTerm.term)
        }

        if delta < 0 {
            highIndex = mid - 1
        } else if delta > 0 {
            lowIndex = mid + 1
        } else {
            termInfo = si.termDictionaryInfos[mid].Clone()
            // Put loaded termInfo into cache
            _termInfoCache[termKey] = termInfo

            return termInfo
        }
    }

    if highIndex == -1 {
        // Term is out of the dictionary range
        return nil
    }

    prevPosition := highIndex
    prevTerm := si.termDictionary[prevPosition]
    prevTermInfo := si.termDictionaryInfos[prevPosition]

    tisFile := si.OpenCompoundFile(".tis")
    tiVersion, _ := tisFile.ReadInt()
    if tiVersion != FORMAT_SEGMENTS_GEN_SUB_VER {
        panic("Wrong TermInfoFile file format")
    }

    termCount, _ := tisFile.ReadLong()
    indexInterval, _ := tisFile.ReadInt()
    skipInterval, _ := tisFile.ReadInt()
    tisFile.ReadInt()

    tisFile.Seek(prevTermInfo.IndexPointer-24, 1)

    termValue := prevTerm.term              /* text */
    termFieldNum := prevTerm.fieldNum       /* field */
    freqPointer := prevTermInfo.FreqPointer /* freqPointer */
    proxPointer := prevTermInfo.ProxPointer /* proxPointer */

    var docFreq, skipOffset int32

    for count := prevPosition*int(indexInterval) + 1; int64(count) <= termCount && (si.GetFieldPosition(int(termFieldNum)) < searchDicField || (si.GetFieldPosition(int(termFieldNum)) == searchDicField && strings.Compare(termValue, term.Text) < 0)); count++ {
        termPrefixLength, _ := tisFile.ReadVInt()
        termSuffix, _ := tisFile.ReadString()
        termFieldNum, _ = tisFile.ReadVInt()
        termValue = GetPrefix(termValue, int(termPrefixLength)) + termSuffix

        docFreq, _ := tisFile.ReadVInt()
        fp, _ := tisFile.ReadVInt()
        freqPointer += int64(fp)
        pp, _ := tisFile.ReadVInt()
        proxPointer += int64(pp)

        if docFreq >= skipInterval {
            skipOffset, _ = tisFile.ReadVInt()
        }
    }

    if termFieldNum == int32(searchField) && termValue == term.Text {
        termInfo = NewTermInfo(int64(docFreq), freqPointer, proxPointer, int64(skipOffset), 0)
    } else {
        termInfo = nil
    }

    // Put loaded termInfo into cache
    _termInfoCache[termKey] = termInfo

    // if len(si.termInfoCache) == 1024 {
    //     si.cleanUpTermInfoCache()
    // }

    return termInfo
}

func (si *SegmentInfo) TermFreqs(term *Term, shift int32) map[int32]int32 {
    var docId int32
    termInfo := si.TermInfo(term)

    frqFile := si.OpenCompoundFile(".frq")
    frqFile.Seek(termInfo.FreqPointer, 1)
    result := make(map[int32]int32)

    for count := 0; int64(count) < termInfo.DocFreq; count++ {
        docDelta, _ := frqFile.ReadVInt()
        if docDelta%2 == 1 {
            docId += (docDelta - 1) / 2
            result[shift+docId] = 1
        } else {
            docId += docDelta / 2
            s, _ := frqFile.ReadVInt()
            result[shift+docId] = s
        }
    }

    return result
}

func (si *SegmentInfo) TermPositions(term *Term, shift int32) map[int32][]int32 {
    var docId int32
    termInfo := si.TermInfo(term)

    frqFile := si.OpenCompoundFile(".frq")
    frqFile.Seek(termInfo.FreqPointer, 1)

    freqs := make(map[int32]int32)

    for count := 0; int64(count) < termInfo.DocFreq; count++ {
        docDelta, _ := frqFile.ReadVInt()
        if docDelta%2 == 1 {
            docId += (docDelta - 1) / 2
            freqs[docId] = 1
        } else {
            docId += docDelta / 2
            s, _ := frqFile.ReadVInt()
            freqs[docId] = s
        }
    }

    result := make(map[int32][]int32)
    prxFile := si.OpenCompoundFile(".prx")
    prxFile.Seek(termInfo.ProxPointer, 1)
    for docId, freq := range freqs {
        var termPosition int32
        positions := make([]int32, freq)

        for count := 0; int32(count) < freq; count++ {
            v, _ := prxFile.ReadVInt()
            termPosition += v
            positions[count] = termPosition
        }

        result[shift+docId] = positions
    }

    return result
}

func (si *SegmentInfo) TermDocs(term *Term, shift int32) []int32 {
    var docId int32

    termInfo := si.TermInfo(term)

    frqFile := si.OpenCompoundFile(".frq")
    frqFile.Seek(termInfo.FreqPointer, 1)
    result := make([]int32, termInfo.DocFreq)

    for count := 0; int64(count) < termInfo.DocFreq; count++ {
        docDelta, _ := frqFile.ReadVInt()
        if docDelta%2 == 1 {
            docId += (docDelta - 1) / 2
        } else {
            docId += docDelta / 2
            // read freq
            frqFile.ReadVInt()
        }

        result[count] = shift + docId
    }

    return result
}
