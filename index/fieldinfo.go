package index

type FieldInfo struct {
    Name            string
    IsIndexed       bool
    DocCount        int
    StoreTermVector bool
    NormsOmitted    bool
    PayloadsStored  bool
}

func NewFieldInfo(name string, isIndexed bool, docCount int, storeTermVector, normsOmitted, payloadsStored bool) *FieldInfo {
    si := new(FieldInfo)
    si.Name = name
    si.IsIndexed = isIndexed
    si.DocCount = docCount
    si.StoreTermVector = storeTermVector
    si.NormsOmitted = normsOmitted
    si.PayloadsStored = payloadsStored
    return si
}
