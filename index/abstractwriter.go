package index

import (
    . "bitbucket.org/jtejido/inverted/document/field"
    "bitbucket.org/jtejido/inverted/store"
    . "bitbucket.org/jtejido/inverted/util"
    "bytes"

    "math"
    "sort"
    "strconv"
    "unicode/utf8"
)

const (
    SKIP_INTERVAL  = 2147483647
    INDEX_INTERVAL = 128
    MAX_SKIP_LEVEL = 0
)

type AbstractWriter struct {
    Directory         store.Directory
    Name              string
    DocCount          int
    Fields            map[string]*FieldInfo
    FdxFile           store.IndexOutput
    FdtFile           store.IndexOutput
    FrqFile           store.IndexOutput
    PrxFile           store.IndexOutput
    TiiFile           store.IndexOutput
    TisFile           store.IndexOutput
    Files             []string
    LastIndexPosition int64
    TermCount         int64
    PrevTerm          *Term
    PrevTermInfo      *TermInfo
    PrevIndexTerm     *Term
    PrevIndexTermInfo *TermInfo
}

func (aw *AbstractWriter) AddField(field *Field) int {

    _, ok := aw.Fields[field.Name]
    if !ok {
        fieldNumber := len(aw.Fields)
        aw.Fields[field.Name] = NewFieldInfo(field.Name, field.Indexed, fieldNumber, field.StoreTermVector, false, false)

        return fieldNumber
    } else {
        aw.Fields[field.Name].IsIndexed = field.Indexed
        aw.Fields[field.Name].StoreTermVector = field.StoreTermVector

        return aw.Fields[field.Name].DocCount
    }

}

func (aw *AbstractWriter) AddFieldInfo(fieldInfo *FieldInfo) int {

    _, ok := aw.Fields[fieldInfo.Name]
    if !ok {
        fieldNumber := len(aw.Fields)
        aw.Fields[fieldInfo.Name] = NewFieldInfo(fieldInfo.Name, fieldInfo.IsIndexed, fieldNumber, fieldInfo.StoreTermVector, false, false)
        return fieldNumber
    } else {
        aw.Fields[fieldInfo.Name].IsIndexed = fieldInfo.IsIndexed
        aw.Fields[fieldInfo.Name].StoreTermVector = fieldInfo.StoreTermVector
        return aw.Fields[fieldInfo.Name].DocCount
    }
}

func (aw *AbstractWriter) AddStoredFields(storedFields []*Field) {
    if aw.FdxFile == nil {

        aw.FdxFile, _ = aw.Directory.CreateOutput(aw.Name + ".fdx")
        aw.FdtFile, _ = aw.Directory.CreateOutput(aw.Name + ".fdt")

        aw.Files = append(aw.Files, aw.Name+".fdx")
        aw.Files = append(aw.Files, aw.Name+".fdt")
    }

    aw.FdxFile.WriteLong(aw.FdtFile.FilePointer())
    aw.FdtFile.WriteVInt(int32(len(storedFields)))
    for _, field := range storedFields {

        aw.FdtFile.WriteVInt(int32(aw.Fields[field.Name].DocCount))
        fieldBits := 0x01 |
            0x02 |
            0x00 /* 0x04 - third bit, compressed (ZLIB) */
        aw.FdtFile.WriteByte(byte(fieldBits))
        if field.Binary == true {
            aw.FdtFile.WriteVInt(int32(len(field.Value)))
            aw.FdtFile.WriteBytes([]byte(field.Value))
        } else {
            var buf bytes.Buffer
            for i, w := 0, 0; i < len(field.Value); i += w {
                r, width := utf8.DecodeRuneInString(field.Value[i:])
                buf.WriteRune(r)
                w = width
            }
            aw.FdtFile.WriteString(buf.String())
        }
    }

    aw.DocCount++

}

func (aw *AbstractWriter) Count() int {
    return aw.DocCount
}

func (aw *AbstractWriter) dumpFNM() {
    fnmFile, _ := aw.Directory.CreateOutput(aw.Name + ".fnm")
    fnmFile.WriteVInt(int32(len(aw.Fields)))

    // nrmFile, _ := aw.Directory.CreateOutput(aw.Name + ".nrm")
    // // Write header
    // nrmFile.WriteBytes([]byte("NRM"))
    // // Write format specifier
    // nrmFile.WriteByte(0xFF)

    var i, j byte

    var keys = make([]int, 0, len(aw.Fields))

    for _, f := range aw.Fields {
        keys = append(keys, f.DocCount)
    }

    sort.Ints(keys)

    for _, k := range keys {
        for _, field := range aw.Fields {
            if k == field.DocCount {

                fnmFile.WriteString(field.Name)

                if field.IsIndexed {
                    i = 0x01
                } else {
                    i = 0x00
                }

                if field.StoreTermVector {
                    j = 0x02
                } else {
                    j = 0x00
                }

                fnmFile.WriteByte(i | j)

                // if field.IsIndexed {
                //     nrmFile.WriteBytes(aw.norms[field.Name])
                // }

                break

            }

        }
    }

    aw.Files = append(aw.Files, aw.Name+".fnm")
    // aw.Files = append(aw.Files, aw.Name+".nrm")
}

func (aw *AbstractWriter) initializeDictionaryFiles() {
    aw.TisFile, _ = aw.Directory.CreateOutput(aw.Name + ".tis")
    aw.TisFile.WriteInt(FORMAT_SEGMENTS_GEN_SUB_VER)
    aw.TisFile.WriteLong(int64(0)) // dummy data for terms count
    aw.TisFile.WriteInt(int32(INDEX_INTERVAL))
    aw.TisFile.WriteInt(int32(SKIP_INTERVAL))
    aw.TisFile.WriteInt(int32(MAX_SKIP_LEVEL))
    aw.TiiFile, _ = aw.Directory.CreateOutput(aw.Name + ".tii")
    aw.TiiFile.WriteInt(FORMAT_SEGMENTS_GEN_SUB_VER)
    aw.TiiFile.WriteLong(int64(0)) // dummy data for terms count
    aw.TiiFile.WriteInt(int32(INDEX_INTERVAL))
    aw.TiiFile.WriteInt(int32(SKIP_INTERVAL))
    aw.TiiFile.WriteInt(int32(MAX_SKIP_LEVEL))

    /** Dump dictionary header */
    aw.TiiFile.WriteVInt(int32(0))                  // preffix length
    aw.TiiFile.WriteString("")                      // suffix
    aw.TiiFile.WriteInt(int32(FORMAT_SEGMENTS_SUB)) // field number
    aw.TiiFile.WriteByte(0x0F)
    aw.TiiFile.WriteVInt(int32(0))  // DocFreq
    aw.TiiFile.WriteVInt(int32(0))  // FreqDelta
    aw.TiiFile.WriteVInt(int32(0))  // ProxDelta
    aw.TiiFile.WriteVInt(int32(24)) // IndexDelta

    aw.FrqFile, _ = aw.Directory.CreateOutput(aw.Name + ".frq")
    aw.PrxFile, _ = aw.Directory.CreateOutput(aw.Name + ".prx")

    aw.Files = append(aw.Files, aw.Name+".tis")
    aw.Files = append(aw.Files, aw.Name+".tii")
    aw.Files = append(aw.Files, aw.Name+".frq")
    aw.Files = append(aw.Files, aw.Name+".prx")

    aw.PrevTerm = nil
    aw.PrevTermInfo = nil
    aw.PrevIndexTerm = nil
    aw.PrevIndexTermInfo = nil
    aw.LastIndexPosition = 24
    aw.TermCount = 0
}

func (aw *AbstractWriter) AddTerm(termEntry *Term, termDocs map[int][]int) {

    freqPointer := aw.FrqFile.FilePointer()
    proxPointer := aw.PrxFile.FilePointer()

    var prevDoc = 0

    for docId, termPositions := range termDocs {
        docDelta := (docId - prevDoc) * 2

        prevDoc = docId
        if len(termPositions) > 1 {

            aw.FrqFile.WriteVInt(int32(docDelta))

            // this is the tf portion
            aw.FrqFile.WriteVInt(int32(len(termPositions)))
        } else {

            aw.FrqFile.WriteVInt(int32(docDelta + 1))
        }

        var prevPosition = 0
        for _, position := range termPositions {
            aw.PrxFile.WriteVInt(int32(position - prevPosition))
            prevPosition = position
        }
    }

    var skipOffset int64

    if len(termDocs) >= SKIP_INTERVAL {
        /**
         * @todo Write Skip Data to a freq file.
         * It's not used now, but make index more optimal
         */
        skipOffset = aw.FrqFile.FilePointer() - freqPointer
    }

    term := NewTerm(termEntry.Text, strconv.Itoa(aw.Fields[termEntry.Field].DocCount))

    termInfo := NewTermInfo(int64(len(termDocs)), freqPointer, proxPointer, skipOffset, int64(0))

    aw.dumpTermDictEntry(aw.TisFile, &aw.PrevTerm, term, &aw.PrevTermInfo, termInfo)

    if (aw.TermCount+1)%INDEX_INTERVAL == 0 {
        aw.dumpTermDictEntry(aw.TiiFile, &aw.PrevIndexTerm, term, &aw.PrevIndexTermInfo, termInfo)

        indexPosition := aw.TisFile.FilePointer()
        aw.TiiFile.WriteVInt(int32(indexPosition - aw.LastIndexPosition))
        aw.LastIndexPosition = indexPosition

    }

    aw.TermCount++
}

/**
 * Close dictionary
 */
func (aw *AbstractWriter) dumpTermDictEntry(dicFile store.IndexOutput, prevTerm **Term, term *Term, prevTermInfo **TermInfo, termInfo *TermInfo) {

    if (*prevTerm) != nil && (*prevTerm).Field == term.Field {

        matchedBytes := 0
        maxBytes := math.Min(float64(len((*prevTerm).Text)), float64(len(term.Text)))
        for matchedBytes < int(maxBytes) &&
            (*prevTerm).Text[matchedBytes] == term.Text[matchedBytes] {
            matchedBytes++
        }

        // Calculate actual matched UTF-8 pattern
        prefixBytes := 0
        prefixChars := 0
        for prefixBytes < matchedBytes {
            charBytes := 1
            if (term.Text[prefixBytes] & 0xC0) == 0xC0 {
                charBytes++
                if ((term.Text[prefixBytes]) & 0x20) == 0x20 {
                    charBytes++
                    if ((term.Text[prefixBytes]) & 0x10) == 0x10 {
                        charBytes++
                    }
                }
            }

            if prefixBytes+charBytes > matchedBytes {
                // char crosses matched bytes boundary
                // skip char
                break
            }

            prefixChars++
            prefixBytes += charBytes
        }

        // Write preffix length

        dicFile.WriteVInt(int32(prefixChars))
        // Write suffix

        dicFile.WriteString(term.Text[prefixBytes:])

    } else {
        // Write preffix length
        dicFile.WriteVInt(int32(0))
        // Write suffix
        dicFile.WriteString(term.Text)
    }
    // Write field number
    conv, _ := strconv.Atoi(term.Field)
    dicFile.WriteVInt(int32(conv))
    // DocFreq (the count of documents which contain the term)

    dicFile.WriteVInt(int32(termInfo.DocFreq))

    (*prevTerm) = term

    if (*prevTermInfo) == nil {
        // Write FreqDelta
        dicFile.WriteVInt(int32(termInfo.FreqPointer))
        // Write ProxDelta
        dicFile.WriteVInt(int32(termInfo.ProxPointer))
    } else {
        // Write FreqDelta
        dicFile.WriteVInt(int32(termInfo.FreqPointer - (*prevTermInfo).FreqPointer))
        // Write ProxDelta
        dicFile.WriteVInt(int32(termInfo.ProxPointer - (*prevTermInfo).ProxPointer))
    }
    // Write SkipOffset - it's not 0 when termInfo.docFreq > skipInterval
    if termInfo.SkipOffset != 0 {
        dicFile.WriteVInt(int32(termInfo.SkipOffset))
    }

    (*prevTermInfo) = termInfo
}

func (aw *AbstractWriter) generateCFS() (err error) {
    var dataFile_read store.IndexInput

    cfsFile, _ := aw.Directory.CreateOutput(aw.Name + ".cfs")
    cfsFile.WriteVInt(int32(len(aw.Files)))

    dataOffsetPointers := make(map[string]int64)
    for _, fileName := range aw.Files {
        dataOffsetPointers[fileName] = cfsFile.FilePointer()
        cfsFile.WriteLong(0)
        cfsFile.WriteString(fileName)
    }

    for _, fileName := range aw.Files {

        dataOffset := cfsFile.FilePointer()

        // Write actual data offset value
        cfsFile.WriteLongAt(dataOffset, dataOffsetPointers[fileName])

        if dataFile_read, err = aw.Directory.OpenInput(fileName); err != nil {
            return
        }

        if err = cfsFile.CopyBytes(dataFile_read, dataFile_read.Length()); err != nil {
            return
        }

        aw.Directory.DeleteFile(fileName)
    }

    return
}

/**
 * Close dictionary
 */
func (aw *AbstractWriter) closeDictionaryFiles() {
    aw.TisFile.WriteLongAt(aw.TermCount, int64(4))
    // + 1 is used to count an additional special index entry (empty term at the start of the list)

    aw.TiiFile.WriteLongAt(int64((aw.TermCount-aw.TermCount%INDEX_INTERVAL)/INDEX_INTERVAL+1), int64(4))
}
