package index

type TermsStream interface {
	/**
	* Reset terms stream.
	 */
	ResetTermsStream()

	/**
	 * Skip terms stream up to specified term preffix.
	 *
	 * Prefix contains fully specified field info and portion of searched term
	 *
	 * @param \ZendSearch\Lucene\Index\Term $prefix
	 */
	SkipTo(prefix *Term)

	/**
	 * Scans terms dictionary and returns next term
	 *
	 * @return \ZendSearch\Lucene\Index\Term|null
	 */
	NextTerm() *Term

	/**
	 * Returns term in current position
	 *
	 * @return \ZendSearch\Lucene\Index\Term|null
	 */
	CurrentTerm() *Term

	/**
	 * Close terms stream
	 *
	 * Should be used for resources clean up if stream is not read up to the end
	 */
	CloseTermsStream()
}
