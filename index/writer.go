package index

import (
    . "bitbucket.org/jtejido/inverted/document"
    "bitbucket.org/jtejido/inverted/store"
    . "bitbucket.org/jtejido/inverted/util"
    "fmt"
    "sort"
    "strconv"
    "time"
)

type Writer struct {
    Directory        store.Directory
    SegmentInfos     *map[string]*SegmentInfo
    TargetVersion    int
    CurrentSegment   *DocumentWriter
    NewSegments      map[string]*SegmentInfo
    SegmentsToDelete map[string]string
    VersionUpdate    int64
}

func NewWriter(directory store.Directory, segmentInfos *map[string]*SegmentInfo, version int) *Writer {
    return &Writer{
        Directory:        directory,
        SegmentInfos:     segmentInfos,
        TargetVersion:    version,
        CurrentSegment:   nil,
        NewSegments:      make(map[string]*SegmentInfo),
        SegmentsToDelete: make(map[string]string),
        VersionUpdate:    0,
    }
}

func GetActualGeneration(directory store.Directory) int64 {

    for count := 0; count < GENERATION_RETRIEVE_COUNT; count++ {

        genFile, err := directory.OpenInput("segments.gen")

        if err != nil {
            break // it means no segments.gen file found
        }

        format, _ := genFile.ReadInt()

        if format != FORMAT_SEGMENTS_GEN_VER {
            panic("Wrong segments.gen file format")
        }

        gen1, _ := genFile.ReadLong()

        gen2, _ := genFile.ReadLong()

        if gen1 == gen2 {
            return gen1
        }

    }

    return int64(-1)
}

func CreateIndex(directory store.Directory, generation int64, nameCount int32) {
    if generation == 0 {
        files, err := directory.ListAll()

        if err != nil {
            panic(err)
        }

        for _, file := range files {
            if file == "deletable" || file == "segments" {
                directory.DeleteFile(file)
            }
        }

        segmentsFile, err := directory.CreateOutput("segments")
        if err != nil {
            panic(err)
        }

        segmentsFile.WriteInt(FORMAT_SEGMENTS_SUB)

        // write version (initialized by current time)
        segmentsFile.WriteLong(int64(time.Now().UnixNano()))
        // write name counter
        segmentsFile.WriteInt(nameCount)
        // write segment counter
        segmentsFile.WriteInt(0)

        deletableFile, err := directory.CreateOutput("deletable")
        if err != nil {
            panic(err)
        }
        // write counter
        deletableFile.WriteInt(0)
    } else {

        genFile, err := directory.CreateOutput("segments.gen")
        if err != nil {
            panic(err)
        }

        genFile.WriteInt(FORMAT_SEGMENTS_GEN_VER)

        // Write generation two times
        genFile.WriteLong(generation)

        genFile.WriteLong(generation)

        segmentsFile, err := directory.CreateOutput(GetSegmentFileName(generation))
        if err != nil {
            panic(err)
        }

        segmentsFile.WriteInt(FORMAT_SEGMENTS_GEN_SUB_VER)

        // write version (initialized by current time)
        segmentsFile.WriteLong(int64(time.Now().UnixNano()))

        // write name counter
        segmentsFile.WriteInt(nameCount)
        // write segment counter
        segmentsFile.WriteInt(0)
    }
}

func (writer *Writer) AddDocument(document *Document) {

    if writer.CurrentSegment == nil {
        writer.CurrentSegment = NewDocumentWriter(writer.Directory, writer.NewSegmentName())
    }

    writer.CurrentSegment.AddDocument(document)

    if writer.CurrentSegment.Count() >= MAX_BUFFERED_DOCS {
        writer.Commit()
    }

    writer.MaybeMergeSegments()

    writer.VersionUpdate++
}

func (writer *Writer) Commit() {
    if writer.CurrentSegment != nil {
        newSegment := writer.CurrentSegment.Close()
        if newSegment != nil {
            writer.NewSegments[newSegment.GetName()] = newSegment
        }
        writer.CurrentSegment = nil
    }

    writer.UpdateSegments()
}

func (writer *Writer) Optimize() {

    writer.UpdateSegments()

    writer.MergeSegments(*writer.SegmentInfos)
}

func (writer *Writer) NewSegmentName() string {
    generation := GetActualGeneration(writer.Directory)
    segFileRead, _ := writer.Directory.OpenInput(GetSegmentFileName(generation))

    segFileRead.Seek(12, 0) // 12 = 4 (int, file format marker) + 8 (long, index version)
    segmentNameCounter, err := segFileRead.ReadInt()

    if err != nil {
        fmt.Errorf("Error: ", err)
    }

    segFileWrite, _ := writer.Directory.OpenOutput(GetSegmentFileName(generation))
    // 12 = 4 (int, file format marker) + 8 (long, index version)

    segFileWrite.WriteIntAt(segmentNameCounter+1, 12)

    segFileRead.Seek(12, 0)

    str, err := BaseConvert(strconv.FormatInt(int64(segmentNameCounter), 10), 10, 36)

    if err != nil {
        fmt.Errorf("Error: ", err)
    }

    return "_" + str
}

func (writer *Writer) MaybeMergeSegments() {

    if writer.HasAnythingToMerge() == false {
        return
    }

    // Update segments list to be sure all segments are not merged yet by another process
    //
    // Segment merging functionality is concentrated in this class and surrounded
    // by optimization lock obtaining/releasing.
    // _updateSegments() refreshes segments list from the latest index generation.
    // So only new segments can be added to the index while we are merging some already existing
    // segments.
    // Newly added segments will be also included into the index by the _updateSegments() call
    // either by another process or by the current process with the commit() call at the end of _mergeSegments() method.
    // That's guaranteed by the serialisation of _updateSegments() execution using exclusive locks.
    writer.UpdateSegments()

    // Perform standard auto-optimization procedure
    var segmentSizes = make(map[string]int32)
    for segName, segmentInfo := range *writer.SegmentInfos {
        segmentSizes[segName] = segmentInfo.Count()
    }

    var mergePool = make(map[string]*SegmentInfo)
    var poolSize int32 = 0
    var sizeToMerge int32 = MAX_BUFFERED_DOCS
    segmentSizes = MapSort(segmentSizes)
    for segName, size := range segmentSizes {
        // Check, if segment comes into a new merging block
        for size >= sizeToMerge {
            // Merge previous block if it's large enough
            if poolSize >= sizeToMerge {
                writer.MergeSegments(mergePool)
            }
            mergePool = make(map[string]*SegmentInfo)
            poolSize = 0

            sizeToMerge *= MERGE_FACTOR

            if sizeToMerge > MAX_MERGE_DOCS {
                return
            }
        }

        mergePool[segName] = (*writer.SegmentInfos)[segName]
        poolSize += size
    }

    if poolSize >= sizeToMerge {
        writer.MergeSegments(mergePool)
    }
}

func (writer *Writer) HasAnythingToMerge() bool {
    var segmentSizes = make(map[string]int32)

    for segName, segmentInfo := range *writer.SegmentInfos {
        segmentSizes[segName] = segmentInfo.Count()

    }

    var mergePool = []*SegmentInfo{}
    var poolSize int32 = 0
    var sizeToMerge int32 = MAX_BUFFERED_DOCS
    segmentSizes = MapSort(segmentSizes)
    for segName, size := range segmentSizes {
        // Check, if segment comes into a new merging block
        for size >= sizeToMerge {
            // Merge previous block if it's large enough
            if poolSize >= sizeToMerge {
                return true
            }
            mergePool = []*SegmentInfo{}
            poolSize = 0

            sizeToMerge *= MERGE_FACTOR

            if sizeToMerge > MAX_MERGE_DOCS {
                return false
            }
        }

        mergePool = append(mergePool, (*writer.SegmentInfos)[segName])
        poolSize += size
    }

    if poolSize >= sizeToMerge {
        return true
    }

    return false
}

func (writer *Writer) MergeSegments(segments map[string]*SegmentInfo) {

    newName := writer.NewSegmentName()

    merger := NewSegmentMerger(writer.Directory, newName)

    var keys = make([]string, 0, len(segments))
    for k := range segments {
        keys = append(keys, k)
    }

    sort.Strings(keys)

    for _, k := range keys {
        merger.AddSource(segments[k])
        writer.SegmentsToDelete[segments[k].GetName()] = segments[k].GetName()
    }

    newSegment := merger.Merge()
    if newSegment != nil {
        writer.NewSegments[newSegment.GetName()] = newSegment
    }

    writer.Commit()
}

func (writer *Writer) UpdateSegments() {

    var generation int64

    // Write down changes for the segments
    for _, segInfo := range *writer.SegmentInfos {
        segInfo.WriteChanges()
    }

    generation = GetActualGeneration(writer.Directory)

    segmentsFile, _ := writer.Directory.OpenInput(GetSegmentFileName(generation))
    generation++
    newSegmentFile, err := writer.Directory.CreateOutput(GetSegmentFileName(generation))

    if err != nil {
        panic(err)
    }

    genFile, _ := writer.Directory.OpenOutput("segments.gen")

    genFile.WriteInt(FORMAT_SEGMENTS_GEN_VER)
    // Write generation (first copy)
    genFile.WriteLong(generation)

    // try block, if fails, do revert like the catch below

    // Write format marker
    newSegmentFile.WriteInt(FORMAT_SEGMENTS_GEN_SUB_VER)

    // Read src file format identifier
    format, _ := segmentsFile.ReadInt()

    if format != FORMAT_SEGMENTS_GEN_SUB_VER {
        panic("Unsupported segments file format.")
    }

    version, _ := segmentsFile.ReadLong()
    version += writer.VersionUpdate
    writer.VersionUpdate = 0
    newSegmentFile.WriteLong(version)

    // Write segment name counter
    sn, _ := segmentsFile.ReadInt()
    newSegmentFile.WriteInt(sn)

    // Get number of segments offset
    numOfSegmentsOffset := newSegmentFile.FilePointer()
    // Write dummy data (segment counter)
    newSegmentFile.WriteInt(int32(0))

    // Read number of segemnts
    var segmentsCount, _ = segmentsFile.ReadInt()

    var segments = make(map[string]int32)
    var docStoreSegment string
    var docStoreIsCompoundFile byte
    for count := 0; int32(count) < segmentsCount; count++ {
        segName, _ := segmentsFile.ReadString()
        segSize, _ := segmentsFile.ReadInt()

        var delGen, _ = segmentsFile.ReadLong()

        docStoreOffset, _ := segmentsFile.ReadInt()

        var docStoreOptions *DocStoreOptions

        if docStoreOffset != FORMAT_SEGMENTS_SUB {
            docStoreSegment, _ = segmentsFile.ReadString()
            docStoreIsCompoundFile, _ = segmentsFile.ReadByte()

            var isCompound bool

            if int(docStoreIsCompoundFile) == 1 {
                isCompound = true
            }

            docStoreOptions = &DocStoreOptions{offset: docStoreOffset, segment: docStoreSegment, isCompound: isCompound}
        } else {
            docStoreOptions = nil
        }

        var hasNorm bool

        hasSingleNormFile, _ := segmentsFile.ReadByte()

        if int(hasSingleNormFile) == 1 {
            hasNorm = true
        }
        numField, _ := segmentsFile.ReadInt()

        normGens := make([]int64, numField)
        if numField != FORMAT_SEGMENTS_SUB {
            for count1 := 0; count1 < int(numField); count1++ {
                normGens[count1], _ = segmentsFile.ReadLong()
            }
        }
        isCompoundByte, _ := segmentsFile.ReadByte()

        _, ok_del := writer.SegmentsToDelete[segName]
        if !ok_del {
            // Load segment if necessary
            _, ok_seg := (*writer.SegmentInfos)[segName]
            if !ok_seg {
                var isCompound bool

                if isCompoundByte == 0xFF {
                    // The segment is not a compound file
                    isCompound = false
                } else if isCompoundByte == 0x01 {
                    // The segment is a compound file
                    isCompound = true
                } else {
                    panic("status unknown")
                }

                (*writer.SegmentInfos)[segName] = NewSegmentInfo(writer.Directory, segName, segSize, delGen, docStoreOptions, hasNorm, isCompound)

            } else {
                // Retrieve actual deletions file generation number
                delGen = (*writer.SegmentInfos)[segName].GetDelGen()
            }

            newSegmentFile.WriteString(segName)
            newSegmentFile.WriteInt(segSize)
            newSegmentFile.WriteLong(delGen)
            if docStoreOptions != nil {
                newSegmentFile.WriteInt(docStoreOffset)
                newSegmentFile.WriteString(docStoreSegment)
                newSegmentFile.WriteByte(docStoreIsCompoundFile)
            } else {
                // Set DocStoreOffset to -1
                newSegmentFile.WriteInt(FORMAT_SEGMENTS_SUB)
            }

            newSegmentFile.WriteByte(hasSingleNormFile)
            newSegmentFile.WriteInt(numField)
            if numField != FORMAT_SEGMENTS_SUB {
                for _, normGen := range normGens {
                    newSegmentFile.WriteLong(normGen)
                }
            }
            newSegmentFile.WriteByte(isCompoundByte)

            segments[segName] = segSize
        }
    }
    segmentsFile.Close()

    segmentsCount = int32(len(segments) + len(writer.NewSegments))

    for segName, segmentInfo := range writer.NewSegments {

        newSegmentFile.WriteString(segName)
        newSegmentFile.WriteInt(segmentInfo.Count())

        // delete file generation: -1 (there is no delete file yet)
        newSegmentFile.WriteInt(FORMAT_SEGMENTS_SUB)
        newSegmentFile.WriteInt(FORMAT_SEGMENTS_SUB)
        // docStoreOffset: -1 (segment doesn't use shared doc store)
        newSegmentFile.WriteInt(FORMAT_SEGMENTS_SUB)
        // HasSingleNormFile
        if segmentInfo.hasSingleNormFile {
            newSegmentFile.WriteByte(0x01)
        } else {
            newSegmentFile.WriteByte(0xFF)
        }

        // NumField
        newSegmentFile.WriteInt(FORMAT_SEGMENTS_SUB)
        // IsCompoundFile
        if segmentInfo.isCompound {
            newSegmentFile.WriteByte(0x01)
        } else {
            newSegmentFile.WriteByte(0xFF)
        }

        segments[segmentInfo.GetName()] = segmentInfo.Count()
        (*writer.SegmentInfos)[segName] = segmentInfo

    }

    writer.NewSegments = make(map[string]*SegmentInfo)

    newSegmentFile.WriteIntAt(segmentsCount, numOfSegmentsOffset) // Update segments count
    newSegmentFile.Close()

    // catch (\Exception $e) {
    //     /** Restore previous index generation */
    //     $generation--;
    //     $genFile->seek(4, SEEK_SET);
    //     // Write generation number twice
    //     $genFile->writeLong($generation); $genFile->writeLong($generation);

    //     // Release index write lock
    //     Lucene\LockManager::releaseWriteLock($this->_directory);

    //     // Throw the exception
    //     throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
    // }

    // Write generation (second copy)
    genFile.WriteLong(generation)

    // Check if another update or read process is not running now
    // If yes, skip clean-up procedure
    // if (Lucene\LockManager::escalateReadLock($this->_directory)) {
    //     /**
    //      * Clean-up directory
    //      */
    //     filesToDelete = array();
    //     filesTypes    = array();
    //     filesNumbers  = array();

    //     // list of .del files of currently used segments
    //     // each segment can have several generations of .del files
    //     // only last should not be deleted
    //     $delFiles = array();

    //     foreach ($this->_directory->fileList() as $file) {
    //         if ($file == 'deletable') {
    //             // 'deletable' file
    //             $filesToDelete[] = $file;
    //             $filesTypes[]    = 0; // delete this file first, since it's not used starting from Lucene v2.1
    //             $filesNumbers[]  = 0;
    //         } elseif ($file == 'segments') {
    //             // 'segments' file
    //             $filesToDelete[] = $file;
    //             $filesTypes[]    = 1; // second file to be deleted "zero" version of segments file (Lucene pre-2.1)
    //             $filesNumbers[]  = 0;
    //         } elseif (preg_match('/^segments_[a-zA-Z0-9]+$/i', $file)) {
    //             // 'segments_xxx' file
    //             // Check if it's not a just created generation file
    //             if ($file != Lucene\Index::getSegmentFileName($generation)) {
    //                 $filesToDelete[] = $file;
    //                 $filesTypes[]    = 2; // first group of files for deletions
    //                 $filesNumbers[]  = (int)base_convert(substr($file, 9), 36, 10); // ordered by segment generation numbers
    //             }
    //         } elseif (preg_match('/(^_([a-zA-Z0-9]+))\.f\d+$/i', $file, $matches)) {
    //             // one of per segment files ('<segment_name>.f<decimal_number>')
    //             // Check if it's not one of the segments in the current segments set
    //             if (!isset($segments[$matches[1]])) {
    //                 $filesToDelete[] = $file;
    //                 $filesTypes[]    = 3; // second group of files for deletions
    //                 $filesNumbers[]  = (int)base_convert($matches[2], 36, 10); // order by segment number
    //             }
    //         } elseif (preg_match('/(^_([a-zA-Z0-9]+))(_([a-zA-Z0-9]+))\.del$/i', $file, $matches)) {
    //             // one of per segment files ('<segment_name>_<del_generation>.del' where <segment_name> is '_<segment_number>')
    //             // Check if it's not one of the segments in the current segments set
    //             if (!isset($segments[$matches[1]])) {
    //                 $filesToDelete[] = $file;
    //                 $filesTypes[]    = 3; // second group of files for deletions
    //                 $filesNumbers[]  = (int)base_convert($matches[2], 36, 10); // order by segment number
    //             } else {
    //                 $segmentNumber = (int)base_convert($matches[2], 36, 10);
    //                 $delGeneration = (int)base_convert($matches[4], 36, 10);
    //                 if (!isset($delFiles[$segmentNumber])) {
    //                     $delFiles[$segmentNumber] = array();
    //                 }
    //                 $delFiles[$segmentNumber][$delGeneration] = $file;
    //             }
    //         } elseif (isset(self::$_indexExtensions[substr($file, strlen($file)-4)])) {
    //             // one of per segment files ('<segment_name>.<ext>')
    //             $segmentName = substr($file, 0, strlen($file) - 4);
    //             // Check if it's not one of the segments in the current segments set
    //             if (!isset($segments[$segmentName])  &&
    //                 ($this->_currentSegment === null  ||  $this->_currentSegment->getName() != $segmentName)) {
    //                 $filesToDelete[] = $file;
    //                 $filesTypes[]    = 3; // second group of files for deletions
    //                 $filesNumbers[]  = (int)base_convert(substr($file, 1 /* skip '_' */, strlen($file)-5), 36, 10); // order by segment number
    //             }
    //         }
    //     }

    //     $maxGenNumber = 0;
    //     // process .del files of currently used segments
    //     foreach ($delFiles as $segmentNumber => $segmentDelFiles) {
    //         ksort($delFiles[$segmentNumber], SORT_NUMERIC);
    //         array_pop($delFiles[$segmentNumber]); // remove last delete file generation from candidates for deleting

    //         end($delFiles[$segmentNumber]);
    //         $lastGenNumber = key($delFiles[$segmentNumber]);
    //         if ($lastGenNumber > $maxGenNumber) {
    //             $maxGenNumber = $lastGenNumber;
    //         }
    //     }
    //     foreach ($delFiles as $segmentNumber => $segmentDelFiles) {
    //         foreach ($segmentDelFiles as $delGeneration => $file) {
    //                 $filesToDelete[] = $file;
    //                 $filesTypes[]    = 4; // third group of files for deletions
    //                 $filesNumbers[]  = $segmentNumber*$maxGenNumber + $delGeneration; // order by <segment_number>,<del_generation> pair
    //         }
    //     }

    //     // Reorder files for deleting
    //     array_multisort($filesTypes,    SORT_ASC, SORT_NUMERIC,
    //                     $filesNumbers,  SORT_ASC, SORT_NUMERIC,
    //                     $filesToDelete, SORT_ASC, SORT_STRING);

    //     foreach ($filesToDelete as $file) {
    //         try {
    //             /** Skip shared docstore segments deleting */
    //             /** @todo Process '.cfx' files to check if them are already unused */
    //             if (substr($file, strlen($file)-4) != '.cfx') {
    //                 $this->_directory->deleteFile($file);
    //             }
    //         } catch (ExceptionInterface $e) {
    //             if (strpos($e->getMessage(), 'Can\'t delete file') === false) {
    //                 // That's not "file is under processing or already deleted" exception
    //                 // Pass it through
    //                 throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
    //             }
    //         }
    //     }

    //     // Return read lock into the previous state
    //     Lucene\LockManager::deEscalateReadLock($this->_directory);
    // } else {
    //     // Only release resources if another index reader is running now
    //     foreach ($this->_segmentsToDelete as $segName) {
    //         foreach (self::$_indexExtensions as $ext) {
    //             $this->_directory->purgeFile($segName . $ext);
    //         }
    //     }
    // }

    // Clean-up _segmentsToDelete container
    writer.SegmentsToDelete = make(map[string]string)

    // Remove unused segments from segments list
    for segName, _ := range *writer.SegmentInfos {
        _, ok := segments[segName]
        if !ok {

            delete((*writer.SegmentInfos), segName)
        }
    }
}
