package index

import (
	"bitbucket.org/jtejido/inverted/util"
)

type DictionaryEntry struct {
	fieldNum int32
	term     string
}

type DictionaryLoader struct{}

func (dl *DictionaryLoader) load(data []byte) ([]*DictionaryEntry, []*TermInfo) {

	var pos int
	// $tiVersion = $tiiFile->readInt();
	tiVersion := int32(data[0])<<24 | int32(data[1])<<16 | int32(data[2])<<8 | int32(data[3])
	pos += 4
	if tiVersion != util.FORMAT_SEGMENTS_GEN_SUB_VER {
		panic("Wrong TermInfoIndexFile file format")
	}

	// $indexTermCount = $tiiFile->readLong();

	indexTermCount := int32(data[pos])<<56 |
		int32(data[pos+1])<<48 |
		int32(data[pos+2])<<40 |
		int32(data[pos+3])<<32 |
		int32(data[pos+4])<<24 |
		int32(data[pos+5])<<16 |
		int32(data[pos+6])<<8 |
		int32(data[pos+7])

	pos += 8

	//                  $tiiFile->readInt();  // IndexInterval
	pos += 4

	// $skipInterval   = $tiiFile->readInt();
	skipInterval := int32(data[pos])<<24 | int32(data[pos+1])<<16 | int32(data[pos+2])<<8 | int32(data[pos+3])
	pos += 4
	if indexTermCount < 1 {
		panic("Wrong number of terms in a term dictionary index")
	}

	if tiVersion == util.FORMAT_SEGMENTS_GEN_SUB_VER {
		/* Skip MaxSkipLevels value */
		pos += 4
	}

	var prevTerm string
	var freqPointer, proxPointer, indexPointer, skipDelta int32
	termDictionary := make([]*DictionaryEntry, indexTermCount)
	termInfos := make([]*TermInfo, indexTermCount)

	for count := 0; count < int(indexTermCount); count++ {
		//$termPrefixLength = $tiiFile->readVInt();
		nbyte := int32(data[pos])
		pos++
		termPrefixLength := int32(nbyte & 0x7F)
		for shift := 7; int32(nbyte&0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			termPrefixLength |= (nbyte & 0x7F) << uint32(shift)
		}

		// $termSuffix       = $tiiFile->readString();
		nbyte = int32(data[pos])
		pos++
		leng := int32(nbyte & 0x7F)
		for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			leng |= (nbyte & 0x7F) << uint32(shift)
		}
		var termSuffix string
		if leng == 0 {
			termSuffix = ""
		} else {
			termSuffix = string(data[pos:int(leng)])
			pos += int(leng)
			for count1 := 0; count1 < int(leng); count1++ {
				if (int32(termSuffix[count1]) & 0xC0) == 0xC0 {
					addBytes := 1
					if (termSuffix[count1])&0x20 == 1 {
						addBytes++

						if (termSuffix[count1])&0x10 == 1 {
							addBytes++
						}
					}
					termSuffix += string(data[pos:addBytes])
					pos += addBytes
					leng += int32(addBytes)

					if int32(termSuffix[count1]) == 0xC0 && int32(termSuffix[count1+1]) == 0x80 {
						termSuffix = string(termSuffix[:count1]) + string(termSuffix[count1+2:])
					}
					count1 += addBytes
				}
			}
		}

		var pb, pc int
		for pb < len(prevTerm) && int32(pc) < termPrefixLength {
			charBytes := 1
			if (prevTerm[pb] & 0xC0) == 0xC0 {
				charBytes++
				if prevTerm[pb]&0x20 == 1 {
					charBytes++
					if prevTerm[pb]&0x10 == 1 {
						charBytes++
					}
				}
			}

			if pb+charBytes > len(data) {
				// wrong character
				break
			}

			pc++
			pb += charBytes
		}
		termValue := string(prevTerm[0:pb]) + termSuffix

		// $termFieldNum     = $tiiFile->readVInt();
		nbyte = int32(data[pos])
		pos++
		termFieldNum := int32(nbyte & 0x7F)
		for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			termFieldNum |= (nbyte & 0x7F) << uint(shift)
		}

		// $docFreq          = $tiiFile->readVInt();
		nbyte = int32(data[pos])
		pos++
		docFreq := int32(nbyte & 0x7F)
		for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			docFreq |= (nbyte & 0x7F) << uint(shift)
		}

		// $freqPointer     += $tiiFile->readVInt();
		nbyte = int32(data[pos])
		pos++
		vint := int32(nbyte & 0x7F)
		for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			vint |= (nbyte & 0x7F) << uint(shift)
		}
		freqPointer += vint

		// $proxPointer     += $tiiFile->readVInt();
		nbyte = int32(data[pos])
		pos++
		vint = int32(nbyte & 0x7F)
		for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			vint |= (nbyte & 0x7F) << uint(shift)
		}
		proxPointer += vint

		if docFreq >= skipInterval {
			// $skipDelta = $tiiFile->readVInt();
			nbyte = int32(data[pos])
			pos++
			vint = int32(nbyte & 0x7F)
			for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
				nbyte = int32(data[pos])
				pos++
				vint |= (nbyte & 0x7F) << uint(shift)
			}
			skipDelta = vint
		} else {
			skipDelta = 0
		}

		// $indexPointer += $tiiFile->readVInt();
		nbyte = int32(data[pos])
		pos++
		vint = int32(nbyte & 0x7F)
		for shift := 7; (nbyte & 0x80) != 0; shift += 7 {
			nbyte = int32(data[pos])
			pos++
			vint |= (nbyte & 0x7F) << uint(shift)
		}

		indexPointer += vint

		termDictionary[count] = &DictionaryEntry{termFieldNum, termValue}

		termInfos[count] = &TermInfo{int64(docFreq), int64(freqPointer), int64(proxPointer), int64(skipDelta), int64(indexPointer)}

		prevTerm = termValue
	}

	termDictionary[0].fieldNum = -1

	return termDictionary, termInfos
}
