package index

import (
    . "bitbucket.org/jtejido/inverted/analysis"
    . "bitbucket.org/jtejido/inverted/analysis/analyzer/common/text/lang"
    . "bitbucket.org/jtejido/inverted/analysis/analyzer/tokenizer"
    . "bitbucket.org/jtejido/inverted/document"
    . "bitbucket.org/jtejido/inverted/document/field"
    "bitbucket.org/jtejido/inverted/store"
    "sort"
    "unicode"
    "unicode/utf8"
    // "fmt"
)

type DocumentWriter struct {
    AbstractWriter
    TermDocs       map[string]map[int][]int
    TermDictionary map[string]*Term
    Transformation *TransformationSet
}

func NewDocumentWriter(directory store.Directory, name string) *DocumentWriter {
    english := NewEnglish()
    transformers := []TransformationInterface{english}

    return &DocumentWriter{
        AbstractWriter: AbstractWriter{
            Directory: directory,
            Name:      name,
            DocCount:  0,
            Fields:    make(map[string]*FieldInfo),
            FdxFile:   nil,
            FdtFile:   nil,
            FrqFile:   nil,
            PrxFile:   nil,
            TiiFile:   nil,
            TisFile:   nil,
            Files:     make([]string, 0),
        },
        TermDocs:       make(map[string]map[int][]int),
        TermDictionary: make(map[string]*Term),
        Transformation: NewTransformationSet(transformers),
    }
}

func (dw *DocumentWriter) AddDocument(document *Document) {

    storedFields := make([]*Field, 0)

    var r rune
    for _, fieldName := range document.GetFieldNames() {

        field := document.GetField(fieldName)

        if field.StoreTermVector == true {
            /**
             * @todo term vector storing support
             */
        }

        if field.Indexed == true {
            if field.Tokenized == true {
                tokenizer := NewWhitespaceTokenizer()
                tokens := tokenizer.Tokenize(field.Value)
                var position = 0
                var tokenCounter = 0

                for _, v := range tokens {
                    text := v.GetTermText()

                    token := dw.Transformation.Transform([]byte(text))

                    r, _ = utf8.DecodeRune(token)

                    if len(token) > 0 && !unicode.IsSpace(r) {
                        tokenCounter++

                        term := NewTerm(text, field.Name)
                        termKey := term.Key()

                        _, ok := dw.TermDictionary[termKey]
                        _, ok_td := dw.TermDocs[termKey][dw.DocCount]
                        if !ok {
                            // New term
                            dw.TermDictionary[termKey] = term
                            dw.TermDocs[termKey] = make(map[int][]int)

                            dw.TermDocs[termKey][dw.DocCount] = make([]int, 0)
                        } else if !ok_td {
                            // Existing term, but new term entry
                            dw.TermDocs[termKey][dw.DocCount] = make([]int, 0)
                        }

                        position += v.GetPositionIncrement()
                        dw.TermDocs[termKey][dw.DocCount] = append(dw.TermDocs[termKey][dw.DocCount], position)
                    }
                }

                if tokenCounter == 0 {
                    // Field contains empty value. Treat it as non-indexed and non-tokenized
                    field.Indexed = false
                    field.Tokenized = false
                }

            }
        }

        if field.Stored == true {
            storedFields = append(storedFields, field)
        }

        dw.AddField(field)
    }

    dw.AddStoredFields(storedFields)
}

func (dw *DocumentWriter) Close() *SegmentInfo {
    if dw.DocCount == 0 {
        return nil
    }

    dw.dumpFNM()
    dw.dumpDictionary()
    dw.generateCFS()

    return NewSegmentInfo(
        dw.Directory,
        dw.Name,
        int32(dw.DocCount),
        -1,
        nil,
        false,
        true,
    )
}

func (dw *DocumentWriter) dumpDictionary() {

    var keys = make([]string, 0, len(dw.TermDictionary))
    for k := range dw.TermDictionary {
        keys = append(keys, k)
    }
    sort.Strings(keys)

    dw.initializeDictionaryFiles()

    for _, k := range keys {
        dw.AddTerm(dw.TermDictionary[k], dw.TermDocs[k])
    }

    dw.closeDictionaryFiles()
}
